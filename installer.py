from argparse import ArgumentParser
from collections import namedtuple
from datetime import datetime
from shutil import copy as filecopy, rmtree
from sys import stdout
from os import listdir, makedirs, name as osname, path as ospath, unlink

try:
    from PyQt4.QtCore import QSettings
    qt_present = True
except ImportError:
    qt_present = False

from PECAS_MapIt_Importer.mapit_plugin_version import _VERSION as plugin_version


if osname == 'nt':
    from msvcrt import getch

elif osname == 'posix':
    from sys import stdin
    from termios import tcgetattr, tcsetattr, TCSADRAIN
    from tty import setraw
    from os import symlink

    def getch():
        fd = stdin.fileno()
        oldattr = tcgetattr(fd)
        try:
            setraw(fd)
            ch = stdin.read(1)
        finally:
            tcsetattr(fd, TCSADRAIN, oldattr)
        return ch

def enum(name, items):
    items = dict(zip(items, items))
    return namedtuple(name, items.keys())(**items)

InstallType = enum('InstallType', ('link', 'copy'))

def argParser():
    parser = ArgumentParser(description='Install HBA Specto PECAS Mapit Importer Plugin for QGIS')
    parser.add_argument('-v', '--verbose',   action='store_true',
                                             help="verbose mode: show detailed installation actions")
    parser.add_argument('-u', '--uninstall', action='store_true',
                                             help="uninstall plugin")
    return parser

def beep():
    stdout.write('\a')
    stdout.flush()

def readstr(prompt):
    if not prompt.endswith(' '):
        prompt += ' '

    try:
        return raw_input(prompt)
    except EOFError:
        print " \n"
        raise KeyboardInterrupt

def readch(prompt, accepts, default=None):
    accept_str = accepts
    if default:
        if default in accepts:
            accept_str = accept_str.replace(default, default.upper())

    print '{} [{}]'.format(prompt, accept_str)
    while True:
        resp = getch().lower()
        if resp in accepts:
            return resp
        elif (resp == '\r') and default:
            return default
        elif (resp == '\x03'):
            raise KeyboardInterrupt
        beep()

def readbool(prompt, default=None):
    return (readch(prompt, 'yn', default) == 'y')

def pause(prompt=None):
    print (prompt or "Press any key to continue . . . ")
    getch()

def getPluginFolder(checkfor, action, foldername):
    confirmprompt = "{} folder '{{}}'?".format(action)
    notfoundprompt = "{} folder '{{}}' not found.".format(foldername)
    
    homedir = ospath.expanduser('~')
    path = ospath.join(homedir, '.qgis2', 'python', 'plugins')
    if readbool(confirmprompt.format(path)):
        if path and ospath.exists(checkfor(path)):
            return path
        print notfoundprompt.format(checkfor(path))

    while True:
        path = readstr("Please enter the path to your QGIS Plugins folder: ")
        if path and ospath.exists(checkfor(path)):
            return path
        print notfoundprompt.format(checkfor(path))

    return None

def printHeader(header=None):
    print "=" * 40
    if header:
        print header
        print "=" * 40

def replaceInFile(filename, replacements):
    with open(filename, 'r') as file:
        lines = file.readlines()
    
    with open(filename, 'w') as file:
        for line in lines:
            for rep in replacements:
                if rep.check(line):
                    line = rep.subst(line)
            file.write(line)

def removeQSettings(verbose=False):
    if not qt_present:
        print "Unable to load Qt: not removing saved settings."
        return

    if verbose:
        printHeader("REMOVING SAVED SETTINGS:")

    namespace = 'HBA_Mapit_'
    prefix = len(namespace)
    settings = QSettings('hbaspecto', 'PECAS Plugin')
    if verbose:
        for key in settings.allKeys():
            keyname = str(key)
            if keyname.startswith(namespace):
                print "    {}".format(keyname[prefix:])
    settings.remove(namespace)

def removeFiles(targetdir, verbose=False):
    target = ospath.join(targetdir, plugin_dirname)
    if verbose:
        printHeader("REMOVING FILES:")
        for filename in listdir(target):
            print "    {}".format(filename)

    if osname == 'posix' and ospath.islink(target):
        unlink(target)
        dirtype = 'link'
    else:
        rmtree(target)
        dirtype = 'directory'

    if verbose:
        print "Removing {}: {}".format(dirtype, target)

def getHTTPSSettings():
    settings_ok = False
    https_settings = {}
    while not settings_ok:
        https_settings['host'] = readstr("HTTPS host    : ")
        https_settings['user'] = readstr("HTTPS username: ")
        https_settings['pass'] = readstr("HTTPS password: ")
        settings_ok = readbool("Are the above values correct?")
    
    return https_settings

def saveHTTPSSettings(plugindir, authdata, verbose):
    filename = ospath.join(plugindir, 'mapit_https.py')

    if not authdata and ospath.exists(filename):
        # Let's not overwrite an existing file with blank data, kplsthx
        return
    
    with open(filename, 'w') as httpfile:
        httpfile.write(
            (
                "# -*- coding: utf-8 -*-\n"
                "\n"
                "# Generated by Pecas Plugin installer script\n"
                "#  on {timestamp}\n"
                "\n"
                "HTTPS_HOST = {authdata[host]!r}\n"
                "HTTPS_USER = {authdata[user]!r}\n"
                "HTTPS_PASSWORD = {authdata[pass]!r}\n"
            ).format(
                timestamp = datetime.now().strftime('%Y-%m-%d at %H:%M:%S'),
                authdata  = authdata or dict(zip(['host', 'user', 'pass'], [""] * 3))
            )
        )
    
def assignProject(plugindir, projecturl, verbose):
    if verbose:
        print "\nAssigning pecas project URL..."
    
    filename = ospath.join(plugindir, 'mapit_config.py')
    projectstring = "        MAPIT_URL_HOST = {!r}\n".format(projecturl)
    replacements = [Replacement(
        check = (lambda line : '{defaulthost}' in line),
        subst = (lambda _    : projectstring)
    )]
    
    if verbose:
        print "    Modifying '{}'.".format(filename)
    replaceInFile(filename, replacements)

def insertVersion(plugindir, verbose):
    if verbose:
        print "\nUpdating version info..."
    
    filename = ospath.join(plugindir, 'metadata.txt')
    versionstring = "version={0[major]}.{0[minor]}.{0[build]}\n".format(plugin_version)
    replacements = [Replacement(
        check = (lambda line : line.startswith('version')),
        subst = (lambda _    : versionstring)
    )]
    
    if verbose:
        print "    Modifying '{}'.".format(filename)
    replaceInFile(filename, replacements)

def copyFiles(targetdir, https=None, installtype=InstallType.copy, verbose=False):
    print "Installing PECAS plugin to '{}' folder...\n".format(targetdir)
    if verbose:
        printHeader("{}ING FILES:".format(installtype.upper()))

    sourcedir = ospath.join(ospath.dirname(ospath.realpath(__file__)), plugin_dirname)
    targetdir = ospath.join(targetdir, plugin_dirname)

    def sieve(fn):
        return (fn.startswith('.') or fn.endswith('.pyc'))
        
    if installtype == InstallType.copy:
        if not ospath.exists(targetdir):
            if verbose:
                print "Creating directory '{}'".format(targetdir)
            makedirs(targetdir)

        for filename in listdir(sourcedir):
            source = ospath.join(sourcedir, filename)

            if ospath.isfile(source) and not sieve(filename):
                target = ospath.join(targetdir, filename)
                if verbose:
                    print "    {} -> {}".format(filename, target)
                filecopy(source, target)
    elif installtype == InstallType.link:
        if verbose:
            print "Linking directory {} -> '{}'".format(sourcedir, targetdir)
        symlink(sourcedir, targetdir)

    saveHTTPSSettings(targetdir, https, verbose)

###############################################################################

def install(verbose):
    print "INSTALLING HBA PECAS MAPIT IMPORTER PLUGIN v{0[major]}.{0[minor]}.{0[build]} FOR QGIS\n".format(plugin_version)

    # Get ready to rumble
    checkfor = lambda path: path
    path = getPluginFolder(checkfor, "Install to", "Destination")
    
    use_authmgr = True
    https_settings = None
    if not readbool("Use QGIS Authentication Manager to manage HTTPS connections?"):
        use_authmgr = False
        if readbool("Setup HTTPS parameters now?"):
            https_settings = getHTTPSSettings()

    installtype = InstallType.copy
    if osname == 'posix':
        prompt = (
            "Install plugin by [L]inking current directory, or statically [C]opying files?\n"
            " (If unsure, copying is always safer.)"
        )
        if readch(prompt, 'lc', default='c') == 'l':
            installtype = InstallType.link
    
    # Do the things
    copyFiles(path, https_settings, installtype, verbose)
    insertVersion(ospath.join(path, plugin_dirname), verbose)
    
    if verbose:
        printHeader()
        
    print (
        "Installation complete.\n"
        " -> To use, open QGIS then select Plugins > Manage and Install Plugins...\n"
        "    In the search box that appears, look for the plugin titled 'PECAS' and click its checkbox.\n"
        "    (You may have to first enable experimental plugins, on the Settings tab.)"
    )
    if use_authmgr:
        print (
            " -> If you need to setup an HTTPS connection, use the QGIS Authentication Manager, found on\n"
            "    the Authentication tab under Settings > Options..."
        )
    pause()
    
def uninstall(verbose):
    print "UNINSTALLING HBA PECAS MAPIT IMPORTER PLUGIN FOR QGIS\n"

    # Get ready to rumble
    checkfor = lambda path : ospath.join(path, 'PECAS_MapIt_Importer')
    path = getPluginFolder(checkfor, "Uninstall from", "Plugin installation")
    
    # Do the things
    removeFiles(path, verbose)
    removeQSettings(verbose)
    
    if verbose:
        printHeader()
    
    print "Uninstallation complete."
    pause()

def main():
    args = argParser().parse_args()

    if args.uninstall:
        run = uninstall
    else:
        run = install

    try:
        run(args.verbose)
    except KeyboardInterrupt:
        print "{} cancelled.".format(run.__name__.title())
    
###############################################################################

Replacement = namedtuple('Replacement', ['check', 'subst'])
plugin_dirname = 'PECAS_MapIt_Importer'

if __name__ == '__main__':
    main()
