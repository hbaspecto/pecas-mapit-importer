# Installing the PECAS MapIt Importer Plugin

## Installing QGIS

The PECAS plugin requires QGIS 2.16 or greater to function. However it currently only works on QGIS 2; QGIS 3 support is planned for a later version of the plugin, send us an email at <contact@hbaspecto.com> if you would like to help fund the conversion. 

If you do not have a version of QGIS >= 2.16 and < 3.00, you will need to obtain one from the [QGIS Download Site](https://qgis.org/en/site/forusers/download.html) or their [archive site](https://qgis.org/downloads/).

## Setting up your authentication system in QGIS

QGIS uses an authentication manager to store passwords.  To set one up, in QGIS go to `Settings -> Options -> Authentication` (Or `QGIS -> Preferences -> Authentication` on MacOS systems) and then click the plus sign.  QGIS will ask you to set a master password, (or will ask you to provide the master password that you've set previously if you've already used authentication manager.)  This is simply so that other users of your computer can't see your web mapping passwords. (Unless you are sharing a public computer or deal with extremely sensitive mapping data we recommend using `p3cas` or something equally simple as your QGIS master password.)

### Setting password for installing Plugin [Plugin Installation Password]

Add an authentication setting for installing the plugin. Call it `hba-plugin` and setup Basic Authentication with username `qgis-user` and use the password `ToffeeBlizzard`.


![Adding the authorization password for plugin installation][auth-install]

### Setting password for MapIt access

The PECAS MapIt Importer Plugin connects to a MapIt server, which generates maps by connecting to a MapIt database.  This MapIt server may be protected by its own password. Add an authentication setting for MapIt to your authentication manager.  Again, press the "plus" sign, and this time, enter the URL for the mapit server, and use Basic Authentication to enter the username and password.  (If your MapIt server is internal only to your organization, it probably will not have a username and password, and you can skip this step)

![Adding the authorization password for your MapIt server][mapit-auth]


[auth-install]: plugin_auth.png "Authorizing the plugin" width=500px

[mapit-auth]: mapit-auth.png "Setting up MapIt server password" width=500px

## Installing the Plugin

To obtain and install the plugin, in QGIS go to the menu item `Plugins -> Manage and Install Plugins`.  Click on the `Settings` and enable `Show also experimental plugins`. Then, click `Add...`.  Type `HBA Plugins` for the Name, and enter `http://qgis-plugins.hbaspecto.com/plugins/plugins.xml` as the URL. For Authentication, choose your `hba-plugin` authentication you set up above in [Plugin Installation Password][].  (In some cases you may have to enter the username `qgis-user` and password `ToffeeBlizzard` manually at this step, instead of relying on the entry you previously made in the Authentication Manager.) The resulting setup should look similar to the figure below.

![View of the plugin installation settings][plugin-install-settings]

To install the plugin from the repository you just setup, select `All` in the left side, and then search for PECAS.  The `PECAS MapIt Importer Plugin` should show up, and you can install it by pressing the `Install plugin` button.

![Installing the plugin][install-plugin]

[plugin-install-settings]: plugin-install-settings.png "Plugin installation settings" width=700px

[install-plugin]: install-plugin.png "Installing the plugin" width=900px

## Testing the plugin

To test the plugin, click on the Household Activity button ![household activity](household-activity-button.png)  in the plugin's toolbar.  You should be able
to select a scenario, a year, and review the type of households by zone (`Entire Group: Households`) as shown in the figure below.  Select both WMS and WFS options, as shown, to 
test importing both Web graphics from MapIt, and GIS data from MapIt.

### Password prompt

QGIS is supposed to use the authentication manager password for MapIt that you setup in the [Setting password for MapIt access][], so it shouldn't prompt for a password.  However
the authentication manager sometimes is bypassed, and you'll have to type your MapIt username and password if you are prompted. 

![Testing reviewing household locations][testing-household-activity]

The resulting map should look about like the figure below, with separate layers for MapIt generated 
pie charts, the data itself, and the context.  An important feature of the plugin is to
properly import the metadata and the URLs for each map, describing the content and how and when it was generated, and pointing back to the MapIt database. This can be reviewed by 
hovering over the layer's entry in the layer panel, as shown in the figure below. 

![Household quantity map][household-quantity-map]


[testing-household-activity]: viewing-household-activity.png "Testing Household Activity"

[household-quantity-map]: household-quantity-map.png "Reviewing Household Quantity Map"