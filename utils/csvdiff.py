# -*- coding: utf-8 -*-
"""
/***************************************************************************
 csvdiff
    provides

        __main__

 Compare the contents of two CSV files on a per-cell basis
                             -------------------
        begin                : 2017-12-08
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Sean Nichols, HBA Specto
        email                : seanni@trichotomy.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type

import os
import sys
import argparse
from math import log10, floor
from pprint import pprint
from collections import namedtuple

CH_ESC = '\x1b'
CH_SPC = '\xe0'
ORD_CTRLC = 3
ORD_SPECIAL = 224

try:
    import msvcrt
    PLATFORM = 'windows'
except ImportError:
    import tty, termios
    PLATFORM = 'posix'

Stats = namedtuple('Stats', ['rows', 'cols', 'diffs', 'rdelta', 'cdelta'])
Diff = namedtuple('Diff', ['rlabel', 'clabel', 'coord1', 'coord2', 'val1', 'val2'])
Coord = namedtuple('Coord', ['row', 'col'])
ModChar = namedtuple('ModChar', ['value', 'modifier'])

def forceustr(s):
    if sys.version_info.major >= 3 and isinstance(s, bytes):
        s = str(s, 'utf-8')
    elif not isinstance(s, str):
        s = str(s)
        
    return s
    
def getch():
    # Just gets a single keypress, doesn't wait for \n or anything.
    #  Since the os functions will return a bytestring, these probably
    #  should be coerced into the system native string, depending on
    #  python version (bytestr for python2, unicode for python3).

    specialchar = False
    if PLATFORM == 'windows':
        ch = msvcrt.getch()

        if ord(ch) == ORD_CTRLC:
            raise KeyboardInterrupt
        elif ord(ch) == ORD_SPECIAL:
            specialchar = True
            ch = msvcrt.getch()
    else:
        fd = sys.stdin.fileno()
        tcset = termios.tcgetattr(fd)

        try:
            tty.setcbreak(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, tcset)

    ch = forceustr(ch)

    if specialchar:
        return ModChar(ch, CH_SPC)

    return ch

def escSeq(ch):
    # Call this if you read in an escape. Will determine (and return)
    #  the following (possibly escaped) sequence.

    if ch != CH_ESC:
        return ch

    ch = getch()
    if ch != '[':
        return ch

    return ModChar('[' + getch(), CH_ESC)

def celladdr(coord):
    c = coord.col
    addr = ""
    while c > 0:
        mod = (c - 1) % 26
        addr = chr(mod + 65) + addr
        c = (c - mod) // 26
        
    return addr + str(coord.row)

def val(s):
    # A very (very) naive string-to-number parser.
    if '.' in s:
        try:
            return float(s)
        except ValueError as e:
            if str(e).startswith('could not convert'):
                return 0
            else:
                raise
    else:
        try:
            return int(s)
        except ValueError as e:
            if str(e).startswith('invalid literal'):
                return 0
            else:
                raise

def n_sig_figs(x, n=1):
    if x == 0:
        return 0

    rv = round(x, (-int(floor(log10(abs(x))))) - 1 + n)

    if floor(log10(abs(rv)) + 1) >= n:
        return int(rv)

    return rv

def formattuple(t):
    return '({})'.format(','.join([str(x) for x in t]))

def die(errmessages=None):
    if type(errmessages) in (list, tuple):
        errmessages = "\n".join([" - " + m for m in errmessages])
    elif type(errmessages) is str and errmessages:
        errmessages = " - " + errmessages

    verb = "update" if args.updatefile else "compare"
    if not errmessages: 
        msg = "Cannot {} CSV files.".format(verb)
    else:
        msg = "Cannot {} CSV files because:\n".format(verb) + errmessages

    if args.updatefile:
        msg += "\nOutput to {.name!r} NOT complete.".format(args.updatefile)
        
    print(msg)

    args.file1.close()
    args.file2.close()
    exit()

def choosediff(diff, accum=None, formatlen=None):
    print("{}".format(formatdiff(diff, formatlen)), end=" ...?  ")
    sys.stdout.flush()

    isLeftArr = lambda ch: ch in (ModChar('K', CH_SPC), ModChar('[D', CH_ESC)) # Windows/Posix
    isRightArr = lambda ch: ch in (ModChar('M', CH_SPC), ModChar('[C', CH_ESC)) # Windows/Posix

    newval = False

    # Don't optimize this condition.
    #  newval could end up being assigned 0 or '' or something else "falsy":
    while newval is False:
        ch = getch()
        # The condition order in these 'if' statements is significant, as isLeftArr()
        #  isRightArr() will call getch() again (and return its result) if there's already
        #  an escape in the buffer... we still want to test the return value of that
        #  call against '1' and '2'.
        if isLeftArr(ch) or ch == '1':
            newval = diff.val1
            print('<-')
            if accum is not None:
                accum[0] += 1
        elif isRightArr(ch) or ch == '2':
            newval = diff.val2
            print('->')
            if accum is not None:
                accum[1] += 1

    return newval

def choosediffs(diffs, accum=None, printheader=False):
    if printheader or len(diffs) > 1:
        formatlen = {
            'row' : max([len(d.rlabel) for d in diffs]),
            'col' : max([len(d.clabel) for d in diffs])
        }
        choose = lambda d: choosediff(d, accum, formatlen)
    else:
        choose = lambda d: choosediff(d, accum)

    if printheader:
        printheadlines(formatlen)

    return [choose(d) for d in diffs]

def printheadlines(formatlen, linenums=True):
    indent = lambda ch: ch * (formatlen['row'] + formatlen['col'] + 4)
    prefix = ("   #  ", "----- ") if linenums else ("", "")
    headlines = prefix[0] + indent(" ") + " FILE1             [ cell ] FILE2             [ cell ]\n" + \
                prefix[1] + indent("-") + " -------------------------- --------------------------"
    print(headlines)

def formatdiff(diff, formatlen=None):
    if formatlen is None:
        formatlen = {
            'row' : len(diff.rlabel),
            'col' : len(diff.clabel)
        }

    valformat = lambda x, lbl: (" {0." + lbl + ":<16}") if float(x) >= 0 else ("{0." + lbl + ":<17}")
    formatstr = "{0.rlabel:<" + str(formatlen['row']) + "} x {0.clabel:<" + str(formatlen['col']) + "}  " + \
                valformat(diff.val1, 'val1') + " [{1:^6}] " + valformat(diff.val2, 'val2') + " [{1:^6}]"
    return formatstr.format(diff, celladdr(diff.coord1), celladdr(diff.coord2))

def printdiffs(diffs):
    if diffs:
        formatlen = {
            'row' : max([len(d.rlabel) for d in diffs]),
            'col' : max([len(d.clabel) for d in diffs])
        }

        printheadlines(formatlen)

        i = 1
        for diff in diffs:
            print("{:>4}. {}".format(i, formatdiff(diff, formatlen)))
            i += 1

def readfileheaders(pfx, file1, file2):
    head = file1.readline().strip()
    head2 = file2.readline().strip()
    cols = head.split(",")
    cols2 = head2.split(",")

    nc = len(cols)
    nc2 = len(cols2)

    errmsg = []
    if nc != nc2:
        errmsg.append("Different number of columns in {.name!r} ({}) and {.name!r} ({})".format(file1, nc, file2, nc2))
    if nc < pfx:
        errmsg.append("Number of columns in {.name!r} ({}) is less than number of columns to use as the row ID ({}).".format(file1, nc, pfx))
    if errmsg:
        die(errmsg)

    colindices = range(pfx, nc)
    optioncols = cols[pfx:]

    xlt = [0, 0]
    for c in cols2[pfx:]:
        xlt.append(optioncols.index(c) + 1)

    return (cols, cols2, colindices, optioncols, xlt)
            
def readfile1(pfx, file1):
    fdata = {}

    n = 1
    for line in file1:
        n += 1
        rowdata = line.strip().split(",")
        rowprefix = tuple(rowdata[:pfx])
        if rowprefix in fdata:
            die("Duplicated row {} in file {.name!r} found at line {}.".format(rowprefix, file1, n))
        fdata[rowprefix] = tuple([n] + rowdata[pfx:])

    file1.close()

    return fdata

def finddiffs(pfx, file1, file2, prec, outfile=None):
    def writeout(str):
        if outfile:
            outfile.write(str)
            
            
    (cols, cols2, colindices, optioncols, xlt) = readfileheaders(pfx, file1, file2)
    fdata1 = readfile1(pfx, file1)
    
    if outfile:
        writeout(",".join(cols2) + "\n")
        accum = [0, 0]
        formatlen = {
            'row' : max([len(str(r)) for r in fdata1.keys()]) - 5,
            'col' : max([len(c) for c in optioncols])
        }
        printheadlines(formatlen, False)

    swappedrows = {}

    rows2 = []
    diffs = []

    n = 1
    for line in file2:
        n += 1
        rowdata = line.strip().split(",")
        rowprefix = tuple(rowdata[:pfx])
        if rowprefix in rows2:
            printdiffs(diffs)
            die("Duplicated row {} in file {.name!r} found at line {}.".format(rowprefix, file2, n))

        rows2.append(rowprefix)
        swappedrows[n] = (n != fdata1[rowprefix][0])
        
        writeout(",".join(rowprefix))

        for j in colindices:
            if not prec:
                changed = (rowdata[j] != fdata1[rowprefix][xlt[j]])
            else:
                changed = (n_sig_figs(float(rowdata[j]), prec) != n_sig_figs(float(fdata1[rowprefix][xlt[j]]), prec))

            if changed:
                d = Diff(rlabel=formattuple(rowprefix), clabel=cols2[j],
                         coord1=Coord(row=fdata1[rowprefix][0], col=xlt[j] + pfx),
                         coord2=Coord(row=n, col=j + pfx),
                         val1=val(fdata1[rowprefix][xlt[j]]), val2=val(rowdata[j]))
                if outfile:
                    writeout(",{}".format(choosediff(d, accum, formatlen)))
                else:
                    diffs.append(d)
            else:
                writeout("," + rowdata[j])
                
        writeout("\n")
            
    file2.close()
    if outfile:
        outfile.close()

    cdelta = len([0 for (r2, r1) in zip(xlt[1:], range(len(xlt))) if r1 != r2])
    numdiffs = len(diffs) if not outfile else accum

    stats = Stats(rows=n-1, cols=len(optioncols), diffs=numdiffs, rdelta=len([r for r in swappedrows.values() if r]), cdelta=cdelta)

    return (stats, diffs)

def runMain():
    errmsg = []
    if not args.file1.name.endswith("csv"):
        errmsg.append("{.name!r} does not appear to be a CSV file?".format(args.file1))
    if not args.file2.name.endswith("csv"):
        errmsg.append("{.name!r} does not appear to be a CSV file?".format(args.file2))
    if args.file1.name == args.file2.name:
        errmsg.append("{.name!r} and {.name!r} appear to be the same file?".format(args.file1, args.file2))

    if errmsg:
        die(errmsg)

    if args.updatefile:
        print("For each change, enter either '1' or '2' to indicate which value to keep:\n")
        actionline = "Num cell values updated from file1: {0.diffs[0]}\n" + \
                     "Num cell values updated from file2: {0.diffs[1]}\n"
        (stats, _) = finddiffs(args.n, args.file1, args.file2, max(args.p, 0), args.updatefile)
        n = sum(stats.diffs)
        if not n:
            print("No differences found. Output file written with no changes from file2.")
        else:
            statline = "1 difference" if n == 1 else "{} differences".format(n)
            print("{} found. Output file written with changes applied to file2.".format(statline))
    else:
        actionline = "Num cell value changes found: {0.diffs}\n"
        (stats, diffs) = finddiffs(args.n, args.file1, args.file2, max(args.p, 0))
        if not diffs:
            print("No differences found.")
        else:
            printdiffs(diffs)

    if args.v:
        print(("Num rows checked: {0.rows}\n" +
               "Num cols checked: {0.cols}\n" +
               actionline +
               "Num rows swapped position: {0.rdelta}\n" +
               "Num cols swapped position: {0.cdelta}\n" +
               "Sig. Fig. precision: {1}").format(stats, args.p if args.p > 0 else "ALL"))
               
        if args.updatefile:
            print("New CSV file written to: {.name!r}".format(args.updatefile))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Compare the contents of two CSV files on a per-cell basis")
    parser.add_argument('-n', type=int, metavar="NUMCOLS", default=2,
                              help="Number of columns to consider as row identifiers (default=%(default)s).")
    parser.add_argument('-p', type=int, metavar="PREC", default=0,
                              help="Number of sig.figs. of precision to check for differences (default=ALL).")
    parser.add_argument('-v', action='store_true',
                              help="Verbose mode: show summary info at end.")
    parser.add_argument('-u', type=argparse.FileType('w', 1), metavar="OUTPUTFILE", dest="updatefile",
                              help="Update: at each difference, choose which variant to keep and write the updated " + 
                                   "CSV to the specified file. (Note row/column order will be the same as input file2.)")
    parser.add_argument('file1', type=argparse.FileType('r', 1), help="First CSV file to compare.")
    parser.add_argument('file2', type=argparse.FileType('r', 1), help="Second CSV file to compare.")

    args = parser.parse_args()

    runMain()
