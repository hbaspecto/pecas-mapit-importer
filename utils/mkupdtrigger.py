# -*- coding: utf-8 -*-
"""
/***************************************************************************
 mkupdtrigger
    provides

        __main__

 Generate the SQL to create a postgresql trigger to update the underlying
  table(s) when the results of a view have been edited.
  
                             -------------------
        begin                : 2017-12-19
        git sha              : $Format:%H$
        copyright            : (C) 2017 by Sean Nichols, HBA Specto
        email                : seanni@trichotomy.ca
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import, division, print_function, unicode_literals
__metaclass__ = type

from argparse import ArgumentParser, RawTextHelpFormatter
from collections import namedtuple
from pprint import pprint

Tableset = namedtuple('Tableset', ['schema', 'tables', 'tkeys', 'tfields', 'vkeys', 'vfields'])
InsertParams = namedtuple('InsertParams', ['table', 'tfields', 'vfields'])
UpdateParams = namedtuple('UpdateParams', ['table', 'fieldmaps', 'keymaps'])
DeleteParams = namedtuple('DeleteParams', ['table', 'keymaps'])
TriggerParams = namedtuple('TriggerParams', ['funcname', 'doactions', 'triggername', 'actiontypes', 'viewname'])

def die(errmessages=None):
    if type(errmessages) in (list, tuple):
        errmessages = "\n".join([" - " + m for m in errmessages])
    elif type(errmessages) is str and errmessages:
        errmessages = " - " + errmessages

    if not errmessages: 
        msg = "error: cannot generate trigger"
    else:
        msg = "error: generate trigger because:\n" + errmessages

    print(msg)
    exit()

def getTableName(table, schema):
    return table if "." in table else ".".join([schema, table])

def breakStringAt(s, chlimit, delim=[","], toinsert="\n"):
    new_s = []
    while s != "":
        breakpt = max([(s.rfind(d, 0, chlimit), len(d)) for d in delim])
        if breakpt[0] == -1:
            try:
                breakpt = min([b for b in [(s.find(d), len(d)) for d in delim] if b[0] != -1])
            except ValueError:
                breakpt = (len(s), 0)

        new_s.append(s[:breakpt[0]])
        s = s[sum(breakpt):]
        
    return toinsert.join(new_s)
    
def mkInsert(tableset):
    re_line = lambda flist: breakStringAt(flist, 66, [", "], ",\n            ")
    
    sql = "IF TG_OP = 'INSERT' THEN\n"
    for i in range(len(tableset.tables)):
        iparams = InsertParams(
            table   = getTableName(tableset.tables[i], tableset.schema),
            tfields = re_line(", ".join(tableset.tfields[i])),
            vfields = re_line(", ".join(["NEW.{}".format(v) for v in tableset.vfields[i]]))
        )
        
        sql += "\n".join([
            "        INSERT INTO {0.table} (",
            "            {0.tfields}",
            "        ) VALUES (",
            "            {0.vfields}",
            "        );\n"
        ]).format(iparams)
    sql += "        RETURN NEW;"
    
    return sql

def mkUpdate(tableset):
    sql = "{}IF TG_OP = 'UPDATE' THEN\n".format("    ELS" if args.insert else "")
    for i in range(len(tableset.tables)):
        fieldpairs = zip(setminus(tableset.tfields[i], tableset.tkeys[i]),
                         setminus(tableset.vfields[i], tableset.vkeys[i]))
        keypairs   = zip(tableset.tkeys[i], tableset.vkeys[i])
        
        uparams = UpdateParams(
            table     = getTableName(tableset.tables[i], tableset.schema),
            fieldmaps = ",\n            ".join(["{} = NEW.{}".format(*f) for f in fieldpairs]),
            keymaps   = ",\n            ".join(["{} = OLD.{}".format(*k) for k in keypairs])
        )
        
        sql += "\n".join([
            "        UPDATE {0.table} SET",
            "            {0.fieldmaps}",
            "        WHERE",
            "            {0.keymaps};\n"
        ]).format(uparams)
    sql += "        RETURN NEW;"
        
    return sql
    
def mkDelete(tableset):
    sql = "{}IF TG_OP = 'DELETE' THEN\n".format("    ELS" if (args.insert or args.update) else "")
    for i in range(len(tableset.tables)):
        keypairs = zip(tableset.tkeys[i], tableset.vkeys[i])
        dparams = DeleteParams(
            table   = getTableName(tableset.tables[i], tableset.schema),
            keymaps = ",\n            ".join(["{} = OLD.{}".format(*k) for k in keypairs])
        )
        
        sql += "\n".join([
            "        DELETE * FROM {0.table}",
            "        WHERE",
            "            {0.keymaps};\n"
        ]).format(dparams)
    sql += "        RETURN NULL;"
    
    return sql
    
def mkAction(action, tableset):
    if not hasattr(mkAction, 'fs'):
        mkAction.fs = {
            'insert' : mkInsert,
            'update' : mkUpdate,
            'delete' : mkDelete
        }
        
    return mkAction.fs[action](tableset)

def partition(source, sizes):
    dest = []
    for sz in sizes:
        dest.append(source[:sz])
        source = source[sz:]
    return dest

def setminus(source, toberemoved):
    return [x for x in source if x not in toberemoved]
    
def main():
    errmsg = []
    if not args.tables:
        errmsg.append("Must include at least one target table.")
    if len(args.fields) < len(args.tables):
        errmsg.append("Must include at least one field for evey target table.")
    if (args.update or args.delete) and len(args.keys) < len(args.tables):
        errmsg.append("Must include at least one primary key for every target table when " +
                      "trigger actions include 'UPDATE' or 'DELETE'.")
    if not (args.insert or args.update or args.delete):
        errmsg.append("Trigger actions must include at least one of 'INSERT', 'UPDATE', 'DELETE'.")
    
    if errmsg:
        die(errmsg)
        
    tfields = [fs.split(",") for fs in args.fields]
    tkeys   = [ks.split(",") for ks in args.keys]
    if args.vfields:
        vfields = partition([fs.split(",") for fs in args.vfields], sizes=[len(tf) for tf in tfields])
        vkeys   = [[vfields[tbl][tfields[tbl].index(k)] for k in tkeys[tbl]] for tbl in range(len(args.tables))]
    else:
        vfields = tfields
        vkeys = tkeys

    tableset = Tableset(
        schema  = args.schema,
        tables  = args.tables,
        tkeys   = tkeys,
        tfields = tfields,
        vkeys   = vkeys,
        vfields = vfields
    )
    
    actions = [a for a in ["insert", "update", "delete"] if vars(args)[a]]

    triggerparams = TriggerParams(
        funcname    = ".".join([args.schema, "_".join(["update", args.tables[0], "table_from_view()"])]),
        doactions   = "\n".join([mkAction(a, tableset) for a in actions]),
        triggername = "_".join(["update", args.schema, args.tables[0], "from", args.view, "trigger"]),
        actiontypes = " OR ".join([a.upper() for a in actions]),
        viewname    = ".".join([args.schema, args.view])
    )
        
    trigger = "\n".join([
        "CREATE OR REPLACE FUNCTION {0.funcname}",
        "    RETURNS TRIGGER",
        "    LANGUAGE plpgsql",
        "    AS $function$",
        "BEGIN",
        "    {0.doactions}",
        "    END IF;",
        "    RETURN NEW;",
        "END;",
        "$function$;",
        "",
        "CREATE TRIGGER {0.triggername}",
        "    INSTEAD OF {0.actiontypes} ON {0.viewname}",
        "    FOR EACH ROW EXECUTE PROCEDURE {0.funcname};\n"
    ]).format(triggerparams)
    
    print(trigger)
    
if __name__ == '__main__':
    parser = ArgumentParser(description="Generate the SQL to create a postgresql trigger to update the " +
                                        "underlying table(s) when the results of a view have been edited.",
                            prefix_chars='-+', formatter_class=RawTextHelpFormatter,
                            epilog="example: If you have a view called public.listpeople that looks like:\n \n" +
                                   "    SELECT id, name, age FROM public.people;\n \n" +
                                   "Then you may wish to create I/U/D triggers as:\n \n" +
                                   "    mkupdtrigger.py -t people -k id -f id,name,age public listpeople\n \n" +
                                   "example: If you have a view called internal.people_jobs that looks like:\n \n" +
                                   "    SELECT people.id, name, age, title\n" +
                                   "      FROM public.people\n" +
                                   " LEFT JOIN internal.jobs ON people.job = jobs.id;\n \n" +
                                   "Then you may wish to create I/U triggers as:\n \n" +
                                   "    mkupdtrigger.py -t public.people -k id -f id,name,age -t jobs -k id -f id,title -d internal people_jobs")
                            
    parser.add_argument('-t',      metavar="TABLENAME", dest="tables", action='append', required=True,
                                   help="Name of the table whose contents are to be updated. If the " +
                                        "table is in a different schema from the view, include the " +
                                        "relevant schema with a dot as -t TableSchema.TableName. If " +
                                        "multiple tables are to be updated, then include multiple " +
                                        "-t options (with relevant -k and -f options). At least one " +
                                        "table is required." )
    parser.add_argument('-k',      metavar="PRIMARYKEY(S)", dest="keys", action='append',
                                   help="Include at least one primary key for each table (specified in " +
                                        "the same order). [EXCEPTION: If you are ONLY trying to generate " +
                                        "an 'INSERT' trigger, then the primary key(s) may be skipped. " +
                                        "However a trigger that includes 'UPDATE' or 'DELETE' actions " +
                                        "requires a primary key for every table. If a table has a " +
                                        "multiple-column primary key then specify all field names, " +
                                        "separated by commas. e.g.: -kidfield1,idfield2") 
    parser.add_argument('-f',      metavar="FIELDNAME(S)", dest="fields", action="append", required=True,
                                   help="Include at least one set of fields for each table (specified in " +
                                        "the same order). Separate multiple fields within a table by " +
                                        "commas, e.g.: -fid,name,age")
    parser.add_argument('--viewf', metavar="VIEWFIELDNAME(S)", dest="vfields",
                                   help="Field names as exported by the view, if different from those in " +
                                        "the tables. If present, there must be the same number of view " +
                                        "fields as there are in all tables combined, and they must be " +
                                        "presented in the same order.")
    parser.add_argument('schema',  help="Name of the schema where the view and tables are found. " +
                                        "(If any tables are in a different schema, include the " +
                                        "relevant schema with the table name as -t TableSchema.TableName).")
    parser.add_argument('view',    help="Name of the view whose underlying table(s) should be updated.")

    insertopt = parser.add_mutually_exclusive_group()
    insertopt.add_argument('+i',
                           '--insert',    dest='insert', action='store_true',
                                          help="Include an 'INSERT' action in the generated trigger. (Default)")
    insertopt.add_argument('-i',
                           '--no-insert', dest='insert', action='store_false',
                                          help="Do NOT include an 'INSERT' action in the generated trigger.")
    updateopt = parser.add_mutually_exclusive_group()
    updateopt.add_argument('+u',
                           '--update',    dest='update', action='store_true',
                                          help="Include an 'UPDATE' action in the generated trigger. (Default)")
    updateopt.add_argument('-u',
                           '--no-update', dest='update', action='store_false',
                                          help="Do NOT include an 'UPDATE' action in the generated trigger.")
    deleteopt = parser.add_mutually_exclusive_group()
    deleteopt.add_argument('+d',
                           '--delete',    dest='delete', action='store_true',
                                          help="Include a 'DELETE' action in the generated trigger. (Default)")
    deleteopt.add_argument('-d',
                           '--no-delete', dest='delete', action='store_false',
                                          help="Do NOT include a 'DELETE' action in the generated trigger.")
    
    parser.set_defaults(insert=True, update=True, delete=True)
    args = parser.parse_args()
    main()