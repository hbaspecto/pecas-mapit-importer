﻿-- Development events
drop view if exists p141.development_events_history_geom; 
create view p141.development_events_history_geom 
as SELECT deh.year_run, deh.event_type, deh.parcel_id, deh.original_pecas_parcel_num, 
deh.new_pecas_parcel_num, deh.available_services, deh.old_space_type_id, deh.new_space_type_id, 
deh.old_space_quantity, deh.new_space_quantity, deh.old_year_built, new_year_built, deh.land_area, 
old_is_derelict, new_is_derelict, old_is_brownfield, new_is_brownfield, deh.zoning_rules_code, deh.taz,
case when event_type in ('C', 'CS') then deh.new_space_quantity
     when event_type in ('A', 'AS') then deh.new_space_quantity - deh.old_space_quantity
	 else 0 end as space_added,
case when event_type in ('C', 'CS', 'D','DS') then deh.old_space_quantity else 0 end as space_demolished,
pbg.geom
	FROM p141.development_events_history deh
	join p141.parcels_backup_with_geom pbg on deh.original_pecas_parcel_num = pbg.pecas_parcel_num
	where event_type != 'US';
	

-- Parcel snapshots
drop view if exists p141.parcels_2020_geom;
create view p141.parcels_2020_geom as
-- select * from p141.parcels_backup_with_geom limit 0;
select p.*, g.geom
from p141.parcels_2020 p
join p141.parcels_backup_with_geom g
on g.pecas_parcel_num = p.pecas_parcel_num;
	
drop view if exists p141.parcels_2030_geom;
create view p141.parcels_2030_geom as
-- select * from p141.parcels_backup_with_geom limit 0;
select p.*, g.geom
from p141.parcels_2030 p
join p141.parcels_backup_with_geom g
on g.pecas_parcel_num = p.pecas_parcel_num;
	
drop view if exists p141.parcels_2040_geom;
create view p141.parcels_2040_geom as
-- select * from p141.parcels_backup_with_geom limit 0;
select p.*, g.geom
from p141.parcels_2040 p
join p141.parcels_backup_with_geom g
on g.pecas_parcel_num = p.pecas_parcel_num;

drop view if exists p141.parcels_2050_geom;
create view p141.parcels_2050_geom as
-- select * from p141.parcels_backup_with_geom limit 0;
select p.*, g.geom
from p141.parcels_2050 p
join p141.parcels_backup_with_geom g
on g.pecas_parcel_num = p.pecas_parcel_num;

-- Zone geometry
-- select addgeometrycolumn('p141','luzs','geom',3401, 'MULTIPOLYGON', 2);
-- update p141.luzs sdl set geom = basel.geom
-- from taz_and_luz_development.luz_v4 basel where 
-- basel.luzv4 = sdl.luz_number;
-- 
-- select addgeometrycolumn('p141','tazs','geom',3401, 'MULTIPOLYGON', 2);
-- update p141.tazs sdl set geom = basel.geom
-- from taz_and_luz_development.taz5k_v4 basel where 
-- basel.taz5kv4 = sdl.taz_number;
-- 
-- create index on p141.tazs using gist(geom);
-- create index on p141.luzs using gist(geom);

-- SWITCH TO MAPIT DATABASE and see add_gale_shapely_to_mapit.sql in the web_output svn repository
