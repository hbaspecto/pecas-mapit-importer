#
# pecas-visualizations/Makefile.inc ---
#

# Usage: include ./Makefile.inc

ifeq (${PMI_DIR},)
  $(error source ./hba-setup.env)
endif

__include_default:
	@echo "Makefile.inc: The first target should be before 'Makefile.inc'."
	exit 1

SHELL:=/bin/bash
.SUFFIXES:

#####

sys_python3_exe=$(shell PATH=/usr/local/bin:/usr/bin:${PATH} type -p python3)
pip3_cmd:=${PMI_VE_DIR}/bin/python3 -m pip

#####

isort_cmd:=isort \
	  --multi-line 3 \
	  --trailing-comma \
	  --line-width 1

autopep8_cmd:=autopep8 \
	  --aggressive \
	  --in-place \
	  --max-line-length 120
