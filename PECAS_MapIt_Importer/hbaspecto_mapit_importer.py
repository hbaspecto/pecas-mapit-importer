# -*- coding: utf-8 -*-

# General Library Imports
# from collections import namedtuple
from os import \
    path as \
    ospath
from pprint import (
    pformat,
)
from time import (
    sleep,
)

# Qt Imports
from PyQt5.QtCore import (
    QCoreApplication,
    QSettings,
    QTranslator,
    qVersion,
)
from PyQt5.QtGui import (
    QIcon,
)
from PyQt5.QtWidgets import (
    QAction,
    QInputDialog,
    QMessageBox,
)
# QGIS Imports
from qgis.core import (
    QgsAuthManager,
    QgsProject,
    QgsVectorLayer,
)

from .hbaspecto_mapit_importer_dialog import (
    MapitImportPluginDialog,
)
from .mapit_config import (
    MapitConfig,
    showConsole,
    showConsoleF,
)
from .mapit_import_parameters import (
    MapitImportParameters,
)
from .mapit_layer_spec import (
    MapitLayerGroup,
    MapitLayerSpec,
    MapitWFSLayerSpec,
    MapitWMSLayerSpec,
)
from .mapit_lookup_if import (
    MapitAPILookup,
)
from .mapit_plugin_progressbar import (
    MapitImportProgress,
    MapitProgressDialog,
)
from .mapit_styler import (
    MapitStyler,
)
from .mapit_utils import (
    MapitPluginAbout,
    MapitPredefinedMap,
    MapitURLLoader,
)
from .resources import *


class MapitImportPlugin:
    pluginmenu = None

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.plugin_dir = ospath.dirname(__file__)

        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = ospath.join(self.plugin_dir, 'i18n', 'PECAS_MapIt_Importer_{}.qm'.format(locale))

        MapitConfig.setAuthManager(QgsAuthManager())

        if ospath.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        self.actions = []
        self.menuname = self.tr('&PECAS MapIt Importer')

        self.toolbar = self.iface.addToolBar("PECAS MapIt Importer")
        self.toolbar.setObjectName('PECAS_MapIt_Importer')

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """

        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        # return QCoreApplication.translate('PECAS MapIt Importer', message, None, QCoreApplication.CodecForTr)
        return QCoreApplication.translate('PECAS MapIt Importer', message, None)

    def get_pluginmenu(self):
        if self.pluginmenu is not None:
            return self.pluginmenu

        for menuact in self.iface.pluginMenu().actions():
            if menuact.text() == self.menuname:
                self.pluginmenu = menuact.menu()
                return self.pluginmenu

        raise EnvironmentError

    def add_menu_action(self, icon_path, text, callback, enabled_flag=True, add_to_menu=True,
                        add_to_toolbar=True, status_tip=None, whats_this=None, parent=None):
        """Add a toolbar icon to the toolbar."""

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            self.toolbar.addAction(action)

        if add_to_menu:
            self.iface.addPluginToMenu(self.menuname, action)

        self.actions.append(action)

        return action

    def add_menu_separator(self, add_to_menu=True, add_to_toolbar=True):
        try:
            if add_to_menu:
                m = self.get_pluginmenu()
                self.actions.append(m.addSeparator())
            if add_to_toolbar:
                self.toolbar.addSeparator()
        except EnvironmentError:
            showConsole("PECAS MapIt Importer plugin menu not yet created:")
            # TODO: error is coming from here?
            showConsole([ch.text() for ch in self.iface.pluginMenu().actions()])

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        self.dlg = MapitImportPluginDialog()

        icon_path = ':/plugins/MapitImportPlugin/'
        parentwin = self.iface.mainWindow()

        def menuAction(actiondef):
            menutext = '    ' + self.tr(actiondef.menu)
            icon = actiondef.icon
            isvisible = actiondef.visible
            isenabled = actiondef.visible
            callback = self.make_run_closure(actiondef)
            return (icon_path + icon, menutext, callback, isenabled,
                    isvisible, isvisible, None, None, parentwin)

        def addMenuItems(predicate, sectiontitle, addsep=True):
            items_to_add = [item for (_, item) in allmenuitems if predicate(item) and item.visible]
            if items_to_add:
                if addsep:
                    self.add_menu_separator()
                self.add_menu_action(None, self.tr(sectiontitle), self.NOP, True, True, False)
                for item in items_to_add:
                    self.add_menu_action(*menuAction(item))

        def addMenuItemsByMode(modefilter, sectiontitle, addsep=True):
            addMenuItems((lambda item: item.mode == modefilter), sectiontitle, addsep)

        def addMenuItemsByType(itemtype, sectiontitle, addsep=True):
            addMenuItems((lambda item: isinstance(item, itemtype)), sectiontitle, addsep)

        allmenuitems = sorted(MapitConfig.schematypes.items())

        addMenuItemsByMode(MapitConfig.SCM_MODE_ONEYEAR, "One Year", addsep=False)
        addMenuItemsByMode(MapitConfig.SCM_MODE_COMPARESCENARIO, "Compare Scenarios")
        addMenuItemsByMode(MapitConfig.SCM_MODE_TIMESERIES, "Time Series")
        addMenuItemsByType(MapitConfig.Predef, "Predefined Maps")
        addMenuItemsByType(MapitConfig.Util, "Utilities")
        addMenuItemsByType(MapitConfig.Test, "Testing", addsep=False)

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(self.tr('&PECAS MapIt Importer'), action)
            self.iface.removeToolBarIcon(action)

        del self.toolbar

    def makeLayerSpecs(self, importparams):
        showConsole(
            (
                "Making Layer Specs with\n"
                "    Layer name: {}\n"
                "    ViewSpecs: {}"
            ).format(
                importparams.layername,
                [v.viewspec for v in importparams.views]
            )
        )
        if importparams.projecturl.startswith('https'):
            MapitLayerSpec.setQAuthIDForHost(importparams.projecturl)

        has_wms = (MapitConfig.OPTION_ST_WMS in importparams.servicetypes)
        has_wfs = (MapitConfig.OPTION_ST_WFS in importparams.servicetypes)

        wmslayers = importparams.wmslayers
        featureclasses = importparams.featureclasses

        def mkLSpecList(importview):
            lst = []
            if has_wms:
                viewlayers = wmslayers or [importview.wlayername]
                lst.append(MapitWMSLayerSpec(importparams, importview, viewlayers))
            if has_wfs:
                lst.extend([MapitWFSLayerSpec(importparams, importview, fc) for fc in featureclasses])
            return lst

        def mkViewGroup(importview):
            return MapitLayerGroup(importview.viewspec, mkLSpecList(importview))

        if len(importparams.views) == 1:
            lspecs = mkLSpecList(importparams.views[0])
        else:
            lspecs = [mkViewGroup(ipv) for ipv in importparams.views]

        return MapitLayerGroup(importparams.layername, lspecs)

    def addLayersToQGIS(self, lspec, charttype, progress=None, parentgroup=None):
        if parentgroup is None:
            useprovidedname = False
            parentgroup = QgsProject.instance().layerTreeRoot()
        else:
            useprovidedname = True

        if isinstance(lspec, MapitLayerGroup):
            showConsole("adding layer group: {!r}".format(lspec.getName()))
            grp = parentgroup.addGroup(lspec.getName())
            for ch in lspec.getChildren():
                if not self.addLayersToQGIS(ch, charttype, progress, grp):
                    return False
        else:
            qlayer = lspec.getQLayer(True, useprovidedname)
            if qlayer.isValid():
                showConsole("qlayer valid: {}".format(lspec))
                if isinstance(qlayer, QgsVectorLayer):
                    if qlayer.featureCount() == 0:
                        showConsole("no features in layer. skipping...")
                        self.msgBox(
                            "The requested data set is not present in any LUZ/TAZ zones "
                            "in this scenario and year. Skipping map import."
                        )
                        return False
                    if lspec.isZoneLayer():
                        showConsole("applying zone layer style")
                        MapitStyler(self.iface, charttype, qlayer).apply()
                        if progress is not None:
                            if not progress.makeProgress():
                                return True
                    elif lspec.isRoadLayer():
                        showConsole("applying road layer style")
                        MapitStyler.applyRoadStyle(qlayer)

                showConsole("drawing layer on screen")
                QgsProject.instance().addMapLayer(qlayer, False)

                parentgroup.addLayer(qlayer)
                if progress is not None:
                    if not progress.makeProgress():
                        return True
            else:
                showConsole("Invalid {} layer specification importing {}".format(lspec.servicetype, lspec))
                return False

        return True

    def createQLayerGroup(self, groupname, parent=None):
        if parent is None:
            parent = QgsProject.instance().layerTreeRoot()

        return parent.addGroup(groupname)

    def NOP(self):
        # A do-nothing function. Mostly used as a stub for QAction callbacks (e.g.: menu items that don't do anything)
        pass

    def make_run_closure(self, val):
        # Closure to enforce create-time binding, rather than call-time binding
        return lambda: self.run(val)

    def _qParent(self, parentwindow=None):
        if parentwindow is None:
            try:
                return self.dlg if self.dlg.isVisible() else self.iface.mainWindow()
            except AttributeError:
                return self.iface.mainWindow()

        return parentwindow

    def _fallback(self, operation, params, default, exceptions=None):
        exceptions = exceptions or Exception

        try:
            if isinstance(params, (list, tuple)):
                return operation(*params)
            else:
                return operation(params)
        except Exception as e:
            if isinstance(e, exceptions):
                return default
            raise

    def _compose(self, f, g):
        return (lambda *args, **kwargs: f(g(*args, **kwargs)))

    def inputBox(self, msg, default=None, coerce=None, title=None, parentwindow=None, addtolist=False):
        if title:
            mtitle = "PECAS MapIt Importer Plugin: {}".format(title)
        else:
            mtitle = "PECAS MapIt Importer Plugin"

        mparent = self._qParent(parentwindow)

        if coerce == int:
            showInputBox = QInputDialog.getInt
            extras = {'value': self._fallback(int, default, 0)}
        elif coerce == float:
            showInputBox = QInputDialog.getDouble
            extras = {'value': self._fallback(float, default, 0.0)}
        elif isinstance(coerce, (list, tuple)) and coerce:
            extras = {
                'list': coerce,
                'current': self._fallback(coerce.index, default, 0),
                'editable': addtolist
            }
            showInputBox = QInputDialog.getItem
        else:
            coerce = coerce or str
            extras = {'text': str(default or '')}
            showInputBox = self._compose(lambda r: (coerce(r[0]), r[1]), QInputDialog.getText)

        (rv, ok) = showInputBox(mparent, self.tr(mtitle), self.tr(msg + "\n"), **extras)
        return rv if ok else None

    def msgBox(self, msg, mbtype=QMessageBox.Information, btns=None, default=None, title=None, parentwindow=None):
        if btns is None:
            mbtns = QMessageBox.Ok
            mdefault = default or QMessageBox.Ok
        else:
            mbtns = btns
            mdefault = default or (QMessageBox.Ok & btns) or btns

        if mbtype == QMessageBox.Warning:
            showMsgBox = QMessageBox.warning
        elif mbtype == QMessageBox.Information:
            showMsgBox = QMessageBox.information
        elif mbtype == QMessageBox.Critical:
            showMsgBox = QMessageBox.critical
        else:
            showMsgBox = QMessageBox.question
            if btns is None:
                mbtns |= QMessageBox.Cancel
                mdefault = QMessageBox.Cancel

        mparent = self._qParent(parentwindow)

        if title:
            mtitle = "PECAS MapIt Importer Plugin: {}".format(title)
        else:
            mtitle = "PECAS MapIt Importer Plugin"

        return showMsgBox(mparent, self.tr(mtitle), self.tr(msg), mbtns, mdefault)

    def determineImportSteps(self, importparams, pbardlg=None):
        progress = MapitImportProgress(pbardlg)

        if importparams.generate:
            progress.addSteps(MapitImportProgress.STEP_GENERATE)

        for v in range(importparams.numviews):
            if MapitConfig.OPTION_ST_WMS in importparams.servicetypes:
                progress.addSteps(MapitImportProgress.STEP_WMSIMPORT)
            if MapitConfig.OPTION_ST_WFS in importparams.servicetypes:
                progress.addSteps(MapitImportProgress.STEP_WFSIMPORT)
                progress.addSteps(MapitImportProgress.STEP_WFSSTYLE)
                nfclasses = len(importparams.featureclasses)
                if nfclasses > 1:
                    progress.addSteps(MapitImportProgress.STEP_WFSIMPORT, nfclasses - 1)

        progress.addSteps(MapitImportProgress.STEP_DISPLAY)

        progress.ready()

        return progress

    def getUserInfo(self, schema):
        # This is where we actually show the dialog box and wait for the user
        #  to fiddle around with the settings, etc.

        lookup = MapitAPILookup(self.dlg.getProjectUrl())
        try:
            self.dlg.resetUI(schema, lookup, MapitConfig.MAPIT_URL_HOST, plugin=self)
        except EnvironmentError as e:
            self.msgBox("Unable to connect to {} because {}.".format(
                lookup.getLookupSourceString(), e), QMessageBox.Warning)
            raise

        done = False
        self.dlg.show(bringtofront=True)

        while not done:
            self.running = True
            result = self.dlg.exec_()
            self.running = False
            if result == self.dlg.Rejected:
                return False

            errors = self.dlg.validate()
            if errors:
                errmsg = "Cannot import from Mapit because:\n" + "\n".join([" - {}".format(err) for err in errors])
                self.dlg.show(bringtofront=True)
                self.msgBox(errmsg)
            else:
                done = True

        return True

    def runTest(self, test_inits):
        # Stuff that I may want to test within the QGIS plugin environment.

        d = MapitProgressDialog("Blah.", tickmarks=True)
        d.show()
        for i in range(10):
            try:
                d.setProgress(i * 10, "Step {}/10 underway.".format(i))
            except StopIteration:
                self.msgBox("A")
                del d
                return

            for j in range(10):
                try:
                    d.setProgress(i * 10 + j)
                except StopIteration:
                    self.msgBox("B")
                    del d
                    return
                sleep(0.2)
        del d
        return

    def run(self, action):
        if isinstance(action, MapitConfig.Schema):
            self.runSchema(action)
        elif isinstance(action, MapitConfig.Predef):
            self.runPredef(action)
        elif isinstance(action, MapitConfig.Util):
            self.runUtil(action)
        elif isinstance(action, MapitConfig.Test):
            self.runTest(action)

    def runPredef(self, predef):
        self.runUtil(predef)

    def runUtil(self, util):
        run = util.run
        if isinstance(run, str):
            run = eval(run)

        params = {
            'plugin': self,
            'schema': util,
            'lookup': MapitAPILookup(self.dlg.getProjectUrl())
        }
        print("Running utility with params", params)

        try:
            run(**params)
        except NotImplementedError as e:
            typestr = str(type(util)).split("'")[1].split('.')[-1].replace("_", " ")
            msg = "{} '{}' not implemented".format(typestr, util.menu)
            if e.message:
                msg += ":\n{}".format(e.message)
            self.msgBox(msg)

    def runSchema(self, schema):
        if hasattr(self, 'running') and self.running:
            self.dlg.show(bringtofront=True)
            return

        if not self.getUserInfo(schema):
            return

        # This is provisional, for the purposes of setting up the # of steps in the progress bar
        ip = MapitImportParameters.fromImportDialog([], self.dlg)

        progbar = MapitProgressDialog("PECAS MapIt Importer Plugin: Importing Layers")
        progress = self.determineImportSteps(ip, progbar)

        if self.dlg.getViewTab() == self.dlg.OPTION_TAB_NEWVIEW:
            try:
                resp = MapitAPILookup(self.dlg.getProjectUrl()).createViewFromDialog(self.dlg)
            except Exception:
                progress.done()
                raise

            if not progress.makeProgress():
                return

            nv = MapitConfig.getNumViews(self.dlg.schema)
            vspecs = [vs['viewname'] for vs in resp][:nv]
            if nv == 1:
                self.dlg.setLayerName(vspecs[0])
                self.dlg.setViewSpec(vspecs[0])

            first_mapurl = MapitConfig.getMapPath(resp[0]['mapfilename'], self.dlg.getProjectUrl())
            self.dlg.setFeatureClasses(first_mapurl)
        else:
            url = self.dlg.ledViewURL.text()
            if self.dlg.viewurl_ok:
                vspecs = [self.dlg.lookup.getMapViewFromURL(url)[0]]
            else:
                self.msgBox("Cannot read map from URL: {!r}".format(url))
                return

        # Now run this again so we can actually get the full import parameters, with the revised views
        showConsole("Making Import Params with vspecs = {}".format(pformat(vspecs)))
        ip = MapitImportParameters.fromImportDialog(vspecs, self.dlg)
        self.runImportView(ip, progress)

    def runImportView(self, importparams, progress):
        try:
            lspecs = self.makeLayerSpecs(importparams)
            self.addLayersToQGIS(lspecs, importparams.charttype, progress)
        except TypeError as e:
            progress.done()
            showConsole("Unable to make Layer Specs because {}.".format(e))
            raise
        except Exception:
            progress.done()
            raise

        progress.done()
