# -*- coding: utf-8 -*-

from base64 import (
    encodestring,
)
from collections import (
    namedtuple,
)
from pprint import (
    pformat,
)

from qgis.core import (
    QgsMessageLog,
)

from .mapit_plugin_version import (
    _VERSION,
)

try:

    from .mapit_https import (
        HTTPS_HOST,
        HTTPS_PASSWORD,
        HTTPS_USER,
    )

except ImportError:

    HTTPS_HOST = None

    HTTPS_USER = None

    HTTPS_PASSWORD = None


def enum(name, items, skipzero=True):

    rng = list(range(1, len(items) + 1)) if skipzero else list(range(len(items)))

    items = dict(list(zip(items, rng)))

    return namedtuple(name, list(items.keys()))(**items)


class MapitConfig():

    MAJOR_VERSION = _VERSION['major']

    MINOR_VERSION = _VERSION['minor']

    BUILD_VERSION = _VERSION['build']

    ############################################################################

    # Eventually, we will switch over to reading all this stuff from the API

    #  call which gets it from Mapit's own config, dependent on the server. But

    #  in the meantime...

    _ALBERTA = True

    _ATLANTA = False

    _DEV = False

    _HTTPS = True

    _EXTRASCHEMAS = False

    if _ALBERTA:

        MAPIT_URL_HOST = 'https://mapit-aset-1-s.hbaspecto.com/'

        MAP_URL_PATH = 'cgi-bin/mapserv?map=/home/vagrant/web_output/mapfiles/AT/'

        MAP_LAYERS_CHART = ['activity', 'commodity']

        MAP_LAYERS_ZONES = ['luz3', 'taz3', 'luz2', 'taz2', 'luz_v4', 'taz5k_v4']

        MAP_LAYERS_CONTEXT = ['ab_highways', 'ab_towns']

        DB_HOSTNAME = 'whitemud.office.hbaspecto.com'

        DB_DATABASE = 'run_log'

        DB_USERNAME = 'postgres'

        DB_PASSWORD = 'postgres'

        DB_PORT = 5009

    elif _ATLANTA:

        MAPIT_URL_HOST = 'http://arcpecas.atlantaregion.com/'

        MAP_URL_PATH = 'cgi-bin/mapserv.exe?map=C:/djangoProjects/web_output/mapfiles/ATLANTA/'

        MAP_LAYERS_CHART = ['activity', 'commodity']

        MAP_LAYERS_ZONES = ['luzs', 'tazs']

        MAP_LAYERS_CONTEXT = ['Transit', 'MajorRoads']

        DB_HOSTNAME = 'arcpecas.atlantaregion.com'

        DB_DATABASE = 'atlanta_db'

        DB_USERNAME = 'usrPostgres'

        DB_PASSWORD = 'usrPostgres'

        DB_PORT = 5432

    elif _DEV:

        MAPIT_URL_HOST = 'http://minburn.office.hbaspecto.com:5013/'  # Port to be changed to 5031

        MAP_URL_PATH = 'cgi-bin/mapserv?map=/home/vagrant/web_output/mapfiles/AT/'

        MAP_LAYERS_CHART = ['activity', 'commodity']

        MAP_LAYERS_ZONES = ['luz2', 'taz2']

        MAP_LAYERS_CONTEXT = ['ab_highways', 'ab_towns']

        DB_HOSTNAME = 'minburn.office.hbaspecto.com'

        DB_DATABASE = 'run_log'

        DB_USERNAME = 'postgres'

        DB_PASSWORD = 'postgres'

        DB_PORT = 5014  # Port to be changed to 5032

    if _HTTPS and MAPIT_URL_HOST:

        MAPIT_URL_HOST = HTTPS_HOST

        MAPIT_API_USER = HTTPS_USER

        MAPIT_API_PASS = HTTPS_PASSWORD

    APP_ORG = 'hbaspecto'

    APP_NAME = 'PECAS MapIt Importer Plugin'

    # keep this as a float to force f-p division when we do scaling

    UI_NATIVE_DPI = 96.0

    ############################################################################

    # Careful here!

    # When creating a QgsLayer, the service type is case sensitive!

    #  * 'wms' must be lowercase

    #  * 'WFS' must be uppercase

    OPTION_ST_WMS = 'wms'

    OPTION_ST_WFS = 'WFS'

    DEFAULT_WMS_VERSION = '1.3.0'

    DEFAULT_WFS_VERSION = '2.0.0'

    MAPIT_URL_PREFIX = 'mapit/'

    # Schema Definitions. These are the different options that will

    #  appear in the Plugin menu

    schemas = enum('Schemas_', [

        'SCM_HOUSEHOLDACTIVITY', 'SCM_NONHOUSEHOLDACTIVITY',

        'SCM_PRICES',

        'SCM_PUTPRODUCTION', 'SCM_PUTCONSUMPTION',

        'SCM_PRODUCTIONACCESSIBILITY', 'SCM_CONSUMPTIONACCESSIBILITY',

        'SCM_SPACEQUANTITIES',

        'SCM_HOUSEHOLDACT_COMP', 'SCM_NONHOUSEHOLDACT_COMP',

        'SCM_HOUSEHOLDACT_TIMESER', 'SCM_NONHOUSEHOLDACT_TIMESER',

        'SCM_PRICES_TIMESER',

        'SCM_PUTPRODUCTION_TIMESER', 'SCM_PUTCONSUMPTION_TIMESER',



        'PDM_A', 'PDM_B', 'PDM_C', 'PDM_D', 'PDM_E',



        'UTL_IMPORTFROMURL', 'UTL_EXPERTMODE', 'UTL_ABOUT',



        'SCM_SHENASZONECONSTANTS',



        'TST_TEST'

    ])

    SCM_FILTER_INCLUDE = 0

    SCM_FILTER_EXCLUDE = 1

    SCM_MODE_NONSCHEMA = -1

    SCM_MODE_TIMESERIES = 0

    SCM_MODE_ONEYEAR = 1

    SCM_MODE_COMPARESCENARIO = 2

    _actionfields = ['id', 'mode', 'menu', 'icon', 'visible']

    Schema = namedtuple('Plugin_Schema', _actionfields + ['file', 'atype', 'attrib', 'aggfunc', 'filter'])

    Predef = namedtuple('Predefined_Map', _actionfields + ['run', 'mapid'])

    Util = namedtuple('Plugin_Tool', _actionfields + ['run', 'title'])

    Test = namedtuple('Test', _actionfields + ['settings'])

    Filter = namedtuple('Filter', ['type', 'groups'])

    schematypes = {

        schemas.SCM_HOUSEHOLDACTIVITY: Schema(

            id='HouseholdActivity',

            mode=SCM_MODE_ONEYEAR,

            menu="Household Activity",

            file='ActivityLocations',

            icon='icon_hh.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=True,

            filter=Filter(type=SCM_FILTER_INCLUDE, groups=['Households'])

        ),

        schemas.SCM_NONHOUSEHOLDACTIVITY: Schema(

            id='NonHouseholdActivity',

            mode=SCM_MODE_ONEYEAR,

            menu="Non-Household Activity",

            file='ActivityLocations',

            icon='icon_non-hh.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=True,

            filter=Filter(type=SCM_FILTER_EXCLUDE, groups=['Households'])

        ),

        schemas.SCM_PRICES: Schema(

            id='Prices',

            mode=SCM_MODE_ONEYEAR,

            menu="Prices",

            file='ExchangeResults',

            icon='icon_prices.png',

            atype='commodity',

            attrib='price',

            aggfunc='avg',

            visible=True,

            filter=None

        ),

        schemas.SCM_PUTPRODUCTION: Schema(

            id='PutProduction',

            mode=SCM_MODE_ONEYEAR,

            menu="Put Production",

            file='CommodityZUtilities',

            icon='icon_put-prod.png',

            atype='commodity',

            attrib='quantity',

            aggfunc='sum',

            visible=True,

            filter=None

        ),

        schemas.SCM_PUTCONSUMPTION: Schema(

            id='PutConsumption',

            mode=SCM_MODE_ONEYEAR,

            menu="Put Consumption",

            file='CommodityZUtilities',

            icon='icon_put-cons.png',

            atype='commodity',

            attrib='quantity',

            aggfunc='sum',

            visible=True,

            filter=None

        ),

        schemas.SCM_PRODUCTIONACCESSIBILITY: Schema(

            id='ProductionAccessibility',

            mode=SCM_MODE_ONEYEAR,

            menu="Production Accessibility",

            file='CommodityZUtilities',

            icon='icon_basic.png',

            atype='commodity',

            attrib='zutility',

            aggfunc='avg',

            visible=_DEV,

            filter=False

        ),

        schemas.SCM_CONSUMPTIONACCESSIBILITY: Schema(

            id='ConsumptionAccessibility',

            mode=SCM_MODE_ONEYEAR,

            menu="Consumption Accessibility",

            file='CommodityZUtilities',

            icon='icon_basic.png',

            atype='commodity',

            attrib='zutility',

            aggfunc='avg',

            visible=_DEV,

            filter=False

        ),

        schemas.SCM_SPACEQUANTITIES: Schema(

            id='SpaceQuantities',

            mode=SCM_MODE_ONEYEAR,

            menu="Space Quantities",

            file='FloorSpaceI',

            icon='icon_basic.png',

            atype='commodity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=False

        ),

        schemas.SCM_HOUSEHOLDACT_COMP: Schema(

            id='HouseholdActivitySC',

            mode=SCM_MODE_COMPARESCENARIO,

            menu="Compare Household Activity",

            file='ActivityLocations',

            icon='icon_hh.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=Filter(type=SCM_FILTER_INCLUDE, groups=['Households'])

        ),

        schemas.SCM_NONHOUSEHOLDACT_COMP: Schema(

            id='NonHouseholdActivitySC',

            mode=SCM_MODE_COMPARESCENARIO,

            menu="Compare Non-Household Activity",

            file='ActivityLocations',

            icon='icon_non-hh.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=Filter(type=SCM_FILTER_EXCLUDE, groups=['Households'])

        ),

        schemas.SCM_HOUSEHOLDACT_TIMESER: Schema(

            id='HouseholdActivityTimeSer',

            mode=SCM_MODE_TIMESERIES,

            menu="Compare Household Activity Time Series",

            file='ActivityLocations',

            icon='icon_hh_ts.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=Filter(type=SCM_FILTER_INCLUDE, groups=['Households'])

        ),

        schemas.SCM_NONHOUSEHOLDACT_TIMESER: Schema(

            id='NonHouseholdActivityTimeSer',

            mode=SCM_MODE_TIMESERIES,

            menu="Compare Non-Household Activity Time Series",

            file='ActivityLocations',

            icon='icon_non-hh_ts.png',

            atype='activity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=Filter(type=SCM_FILTER_EXCLUDE, groups=['Households'])

        ),

        schemas.SCM_PRICES_TIMESER: Schema(

            id='PricesTimeSer',

            mode=SCM_MODE_TIMESERIES,

            menu="Prices Time Series",

            file='ExchangeResults',

            icon='icon_prices.png',

            atype='commodity',

            attrib='price',

            aggfunc='avg',

            visible=_DEV,

            filter=None

        ),

        schemas.SCM_PUTPRODUCTION_TIMESER: Schema(

            id='PutProductionTimeSer',

            mode=SCM_MODE_TIMESERIES,

            menu="Put Production Time Series",

            file='CommodityZUtilities',

            icon='icon_put-prod_ts.png',

            atype='commodity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=None

        ),

        schemas.SCM_PUTCONSUMPTION_TIMESER: Schema(

            id='PutConsumptionTimeSer',

            mode=SCM_MODE_TIMESERIES,

            menu="Put Consumption Time Series",

            file='CommodityZUtilities',

            icon='icon_put-cons_ts.png',

            atype='commodity',

            attrib='quantity',

            aggfunc='sum',

            visible=False,

            filter=None

        ),

        schemas.PDM_A: Predef(

            id='PredefinedMapA',

            mode=SCM_MODE_NONSCHEMA,

            menu="A",

            icon='icon_predef_A.png',

            run='MapitPredefinedMap',

            mapid='A',

            visible=False

        ),

        schemas.PDM_B: Predef(

            id='PredefinedMapB',

            mode=SCM_MODE_NONSCHEMA,

            menu="B",

            icon='icon_predef_B.png',

            run='MapitPredefinedMap',

            mapid='B',

            visible=False

        ),

        schemas.PDM_C: Predef(

            id='PredefinedMapC',

            mode=SCM_MODE_NONSCHEMA,

            menu="C",

            icon='icon_predef_C.png',

            run='MapitPredefinedMap',

            mapid='C',

            visible=False

        ),

        schemas.PDM_D: Predef(

            id='PredefinedMapD',

            mode=SCM_MODE_NONSCHEMA,

            menu="D",

            icon='icon_predef_D.png',

            run='MapitPredefinedMap',

            mapid='D',

            visible=False

        ),

        schemas.PDM_E: Predef(

            id='PredefinedMapE',

            mode=SCM_MODE_NONSCHEMA,

            menu="E",

            icon='icon_predef_E.png',

            run='MapitPredefinedMap',

            mapid='E',

            visible=False

        ),

        schemas.UTL_IMPORTFROMURL: Util(

            id='ImportFromURL',

            mode=SCM_MODE_NONSCHEMA,

            menu="Import Pre-defined View from URL",

            icon='icon_url.png',

            run='MapitURLLoader',

            visible=True,

            title="Enter View URL"

        ),

        schemas.UTL_EXPERTMODE: Util(

            id='MapitExpertMode',

            mode=SCM_MODE_NONSCHEMA,

            menu="Define View via Mapit ('Expert Mode')",

            icon='icon_util.png',

            run=(lambda x: x),    # TODO: At some point, this will need to actually do something...

            visible=False,

            title="Define View / Expert Mode"

        ),

        schemas.UTL_ABOUT: Util(

            id='MapitAboutPlugin',

            mode=SCM_MODE_NONSCHEMA,

            menu="About PECAS MapIt Importer Plugin",

            icon='icon_info.png',

            run='MapitPluginAbout',

            visible=True,

            title="About PECAS MapIt Importer Plugin"

        ),

        schemas.SCM_SHENASZONECONSTANTS: Schema(

            id='HouseholdActivityWithZoneConstants',

            mode=SCM_MODE_ONEYEAR,

            menu="Shena's Household w/ Zone Constants",

            file='ActivityLocations',

            icon='icon_extra_hh-zoneconst.png',

            atype='activity',

            attrib='zoneconstant',

            aggfunc='sum',

            visible=_EXTRASCHEMAS,

            filter=Filter(type=SCM_FILTER_INCLUDE, groups=['Households'])

        ),

        schemas.TST_TEST: Test(

            id='TestMode',

            mode=SCM_MODE_NONSCHEMA,

            menu="Test Widget",

            icon='icon_extra_util.png',

            visible=_EXTRASCHEMAS,

            settings=None

        )

    }

    @staticmethod
    def getUrlHost(projecturl):

        return projecturl

    @staticmethod
    def getBaseUrl(viewspec, projecturl, mapitcat='Act', mapfile=None):

        baseurl = [MapitConfig.getUrlHost(projecturl), MapitConfig.MAPIT_URL_PREFIX]

        if mapitcat == 'Act':

            baseurl.extend(['mapAct/', viewspec])

        elif mapitcat == 'Yr':

            if mapfile.endswith('.map'):

                mapfile = mapfile[:-4]

            mapfile = mapfile[mapfile.rfind('/') + 1:]

            baseurl.extend(['mapYr/', mapfile, '?viewName=', viewspec])

        showConsole("Base URL is: {}".format(baseurl))

        return "".join(baseurl)

    @staticmethod
    def getNumViews(schema):

        try:

            return {

                MapitConfig.SCM_MODE_NONSCHEMA: 1,

                MapitConfig.SCM_MODE_TIMESERIES: 3,

                MapitConfig.SCM_MODE_ONEYEAR: 1,

                MapitConfig.SCM_MODE_COMPARESCENARIO: 3

            }[schema.mode]

        except KeyError:

            return 1

    @staticmethod
    def determineViewSpecType(viewspec, scen=1):

        # FIXME: this part is super sketchy - need to confirm better method

        #  (actually, there is a function added to the Mapit API that will give

        #   us this info; we just need to switch over all calls so they use it,

        #   then we can delete this garbage)

        if 'comparison' in viewspec:

            return 'comp'

        elif 'rulehalf' in viewspec:

            return 'roh'

        else:

            return 'scen{}'.format(scen)

    @staticmethod
    def getReadableATypes(atype):

        return ("activity", "activities") if atype == 'activity' else ("put", "puts")

    @staticmethod
    def getMapUrl(viewspec, projecturl, scen=1):

        vsparts = viewspec.split('_')

        vstype = MapitConfig.determineViewSpecType(viewspec, scen)

        vstimestamp = '_'.join(vsparts[-4:-2])

        mapfilespec = ('_'.join(['luz_mapfile', vstype, 'act', vstimestamp])) + '.map'

        return MapitConfig.getMapPath(mapfilespec, projecturl)

    @staticmethod
    def getMapPath(filename, projecturl):

        mapurlhost = MapitConfig.getUrlHost(projecturl)

        # print "filename: {}\nprojecturl: {}\nmapurlhost: {}".format(filename, projecturl, mapurlhost)

        if not filename.endswith('.map'):

            filename += '.map'

        return mapurlhost + MapitConfig.MAP_URL_PATH + filename

    @staticmethod
    def getHTTPSAuthHeader():

        if not MapitConfig._HTTPS:

            return None

        authstr = '{}:{}'.format(MapitConfig.MAPIT_API_USER, MapitConfig.MAPIT_API_PASS)

        return 'Basic {}'.format(encodestring(authstr.encode('ascii'))[:-1].decode("utf-8"))

    @staticmethod
    def getProjectUrl(url):

        protocol = 'http:'

        parts = url.lower().split('/')

        try:

            if parts[0].startswith('http') or parts[0].startswith('ftp'):

                protocol = parts[0]

                parts = parts[1:]

            while not parts[0]:

                parts = parts[1:]

            return '/'.join([protocol, '', parts[0], ''])

        except IndexError:

            return None

    @classmethod
    def setAuthManager(cls, authman):

        cls._authman = authman

    @classmethod
    def getAuthManager(cls):
        return cls._authman


def showConsole(msg, continueLine=False):
    """Show the message on the console."""
    # cast to text...
    msg = "{}".format(msg)
    # ...then turn into bytes; so the string doenst cause encoding errors.
    # the QGIS console will print the escaped bytes.
    msg = msg.encode('utf-8')

    try:
        if continueLine:
            print(msg, end=' ')
        else:
            print(msg)
    except IOError:
        QgsMessageLog.logMessage("[PECAS MapIt Importer Plugin]: {}".format(str(msg)))


def showConsoleF(msg, indent=1):

    showConsole(pformat(msg, indent))
