# -*- coding: utf-8 -*-

# QGIS Imports

from qgis.core import (
    QgsAuthMethodConfig,
    QgsRasterLayer,
    QgsVectorLayer,
)

from .mapit_config import (
    MapitConfig,
    showConsole,
)


def getPreferredElement(list, prefs, fallback=None):

    if len(list) == 0:

        raise LookupError

    try:

        return [pref for pref in prefs if pref in list][0]

    except IndexError:

        if fallback is None:

            return list[0]

        else:

            return fallback


class MapitLayerSpec(object):

    caps = None

    qauthid = None

    @classmethod
    def setQAuthIDForHost(cls, host):

        if not host.endswith('/'):

            host += '/'

        def hostMatches(url):

            if not url.endswith('/'):

                url += '/'

            return (url == host)

        authmanager = MapitConfig.getAuthManager()

        authconfig = QgsAuthMethodConfig()

        for qid in authmanager.configIds():

            authmanager.loadAuthenticationConfig(qid, authconfig)

            if hostMatches(str(authconfig.uri())):

                cls.setQAuthID(qid)

                return qid

        errmsg = "No config found in Auth Manager corresponding to host {!r}".format(host)

        raise LookupError(errmsg)

    @classmethod
    def setQAuthID(cls, qauthid):

        cls.qauthid = qauthid

    def __init__(self, importparams=None, importview=None, components=None, servicetype=None):

        servicetype = servicetype or self._defaultServiceType()

        if importparams:

            self.setSpecsFromImportParams(importparams, importview, servicetype, components)

    def __str__(self):

        return (

            "[Mapit Plugin Layer: "

            "layername={0.layername!r}, "

            "viewspec={0.viewspec!r} "

            "on service {0.servicetype}]"

        ).format(self)

    def _defaultServiceType(self):

        return None

    def setSpecsFromImportParams(self, ip, ipv, servicetype=None, components=None):

        if servicetype is None:

            reqstypes = ip.servicetypes

            if len(reqstypes) == 1:

                servicetype = reqstypes[0]

            else:

                # If multiple services have been requested, we really need to specify which

                #  one we're trying to generate a spec for. (If only one service has been,

                #  then we can of course infer from that.)

                errmsg = "Ambiguous service type requested for layer import spec. Must specify WMS or WFS."

                raise TypeError(errmsg)

        specs = (ipv.viewspec, ip.projecturl, ipv.mapurl, ip.layername, ip.mapitcat, servicetype, components)

        showConsole("Layer Specs: {}".format(specs))

        self.setSpec(*specs)

    def setSpec(self, viewspec, projecturl, mapurl, layername, mapitcat, servicetype=None, components=None):

        self.viewspec = viewspec

        self.projecturl = projecturl

        self.mapurl = mapurl

        self.layername = layername

        self.mapitcat = mapitcat

        self.servicetype = servicetype

        self.featureclass = components

        self.wlayers = components

        self.capabilities = None

        self.qlayer = None

        self.baseurl = MapitConfig.getBaseUrl(viewspec, projecturl, mapitcat, mapurl)

    def mkParamUrl(self):

        raise NotImplementedError

    def getLayerParams(self, useprovidedname=False):

        if useprovidedname and (self.capabilities is not None):

            layername = self.capabilities.getLayerName(self.featureclass)

        else:

            layername = self.layername

        return (self.mkParamUrl(), layername, self.servicetype)

    def getServiceType(self):

        return self.servicetype

    def getLayerName(self):

        return self.layername

    def mkQLayer(self, useprovidedname=False):

        raise NotImplementedError

    def getQLayer(self, createifnoexist=True, useprovidedname=False):

        if self.qlayer is None:

            if not createifnoexist:

                return None

            self.mkQLayer(useprovidedname)

        return self.qlayer

    def setQLayerMetadata(self, createifnoqlayer=True):

        qlayer = self.getQLayer(createifnoqlayer)

        if (qlayer is None) and not createifnoqlayer:

            return

        qlayer.setShortName(self.viewspec)

        title = self.capabilities.getTitle(self.featureclass)

        abstract = self.capabilities.getAbstract(self.featureclass)

        if title is not None:

            qlayer.setTitle(title)

        if abstract is not None:

            qlayer.setAbstract(abstract)

        qlayer.setDataUrl(self.baseurl)

        qlayer.setDataUrlFormat('text/html')

    def makesValidLayer(self):

        return self.getQLayer().isValid()

    def isZoneLayer(self):

        title = self.capabilities.getTitle(self.featureclass)

        zlayers = [zlyr + '_data' for zlyr in MapitConfig.MAP_LAYERS_ZONES]

        showConsole("Checking if {} is a zone layer:\n  (Known zone layers are {})".format(title, zlayers))

        return (title in zlayers)

    def isRoadLayer(self):

        # This is very kludge-y but if you have a better way of doing it (that will work

        #  in any context, in any set of layer names in any jurisdiction), feel free to

        #  substitute the code here.

        # Having said that, false negatives (the most likely types of error here) aren't

        #  the end of the world. The only thing this is being used for (for now) is to

        #  determine which layers should get "road type" styles (as opposed to getting

        #  whatever line styles QGIS chooses to assign). Which means that if we miss one,

        #  then oops: the roads may look a little funny - but no big deal.

        # False positives are a little worse, as it may try to change line styles for a

        #  feature set that doesn't have any linear features. But that's less likely to

        #  happen with the below heuristic.

        # In the meantime, if you are getting false negatives because your roading layer

        #  has a name that isn't accounted for, feel free to add it to the keyword list.

        road_keywords = ['road', 'highway', 'street']

        title = self.capabilities.getTitle(self.featureclass).lower()

        return any([(word in title) for word in road_keywords])


class MapitWMSLayerSpec(MapitLayerSpec):

    def __init__(self, importparams=None, importview=None, wlayers=None):

        super(MapitWMSLayerSpec, self).__init__(importparams, importview, wlayers)

        self.capabilities = importview.wms_caps

    def _defaultServiceType(self):

        return MapitConfig.OPTION_ST_WMS

    def mkParamUrl(self):

        def _cleanLayerName(layer):

            if layer.endswith('_with_geom'):

                return layer[:-10]

            return layer

        proj = self.capabilities.getAvailableProjections()[0]

        allformats = self.capabilities.getAvailableFormats()

        imgformat = getPreferredElement(allformats, ['image/svg+xml', 'image/png'])

        # A little bit ugly, but shrug. In theory, you can supply a comma-separated

        #  list of layers, but it doesn't seem to want to work that way; instead

        #  requires a separate layers=... param for each one.

        layerlist = '&layers='.join([_cleanLayerName(l) for l in self.wlayers])

        # And there needs to be a separate styles entry for each layer, even if they are blank...

        styleslist = '&styles=' * (len(self.wlayers) - 1)

        wmsdata = {

            'contextualWMSLegend': 0,

            'crs': proj,

            'format': imgformat,

            'layers': layerlist,

            'styles': styleslist,

            'dpiMode': 7,

            'featureCount': 10,

            'url': self.mapurl.replace('=', '%3D')

        }

        if self.qauthid:

            wmsdata['authcfg'] = self.qauthid

        return '&'.join(['{}={}'.format(k, v) for (k, v) in list(wmsdata.items())])

    def mkQLayer(self, useprovidedname=False):

        layerparams = self.getLayerParams(useprovidedname)

        self.qlayer = QgsRasterLayer(*layerparams)

        if not self.qlayer.isValid():

            showConsole("Invalid qlayer: {!r}".format(layerparams[0]))

        self.setQLayerMetadata(False)


class MapitWFSLayerSpec(MapitLayerSpec):

    def __init__(self, importparams=None, importview=None, featureclass=None):

        super(MapitWFSLayerSpec, self).__init__(importparams, importview, featureclass)

        self.capabilities = importview.wfs_caps

    def _defaultServiceType(self):

        return MapitConfig.OPTION_ST_WFS

    def mkParamUrl(self):

        proj = self.capabilities.getDefaultProjection(self.featureclass)

        wfsdata = {

            'service': 'WFS',

            'restrictToRequestBBOX': '1',

            'srsname': proj,

            'typename': self.featureclass

        }

        if self.qauthid:

            wfsdata['authcfg'] = self.qauthid

        return self.mapurl + '&' + '&'.join(['{}={}'.format(k, v) for (k, v) in list(wfsdata.items())])

    def mkQLayer(self, useprovidedname=False):

        layerparams = self.getLayerParams(useprovidedname)

        self.qlayer = QgsVectorLayer(*layerparams)

        self.setQLayerMetadata(False)


class MapitLayerGroup():

    def __init__(self, name, children=[]):

        self.name = name

        self.children = children

    def addChild(self, child):

        self.children.append(child)

    def getChildren(self):

        return self.children

    def getName(self):

        return self.name
