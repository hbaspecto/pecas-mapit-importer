# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mapit_plugin_urlimport_dialog_base.ui'
#
# Created: Fri Nov 01 16:03:25 2019
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import (
    QtCore,
    QtGui,
)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MapitPluginURLImportDialogBase(object):
    def setupUi(self, MapitPluginURLImportDialogBase):
        MapitPluginURLImportDialogBase.setObjectName(_fromUtf8("MapitPluginURLImportDialogBase"))
        MapitPluginURLImportDialogBase.resize(822, 570)
        MapitPluginURLImportDialogBase.setMinimumSize(QtCore.QSize(800, 500))
        MapitPluginURLImportDialogBase.setSizeGripEnabled(True)
        self.vltDialog = QtGui.QVBoxLayout(MapitPluginURLImportDialogBase)
        self.vltDialog.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.vltDialog.setObjectName(_fromUtf8("vltDialog"))
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.vltDialog.addLayout(self.formLayout)
        self.bbxDialog = QtGui.QDialogButtonBox(MapitPluginURLImportDialogBase)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.bbxDialog.setFont(font)
        self.bbxDialog.setOrientation(QtCore.Qt.Horizontal)
        self.bbxDialog.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.bbxDialog.setObjectName(_fromUtf8("bbxDialog"))
        self.vltDialog.addWidget(self.bbxDialog)

        self.retranslateUi(MapitPluginURLImportDialogBase)
        QtCore.QObject.connect(self.bbxDialog,
                               QtCore.SIGNAL(_fromUtf8("accepted()")),
                               MapitPluginURLImportDialogBase.accept)
        QtCore.QObject.connect(self.bbxDialog,
                               QtCore.SIGNAL(_fromUtf8("rejected()")),
                               MapitPluginURLImportDialogBase.reject)
        QtCore.QMetaObject.connectSlotsByName(MapitPluginURLImportDialogBase)

    def retranslateUi(self, MapitPluginURLImportDialogBase):
        MapitPluginURLImportDialogBase.setWindowTitle(
            _translate(
                "MapitPluginURLImportDialogBase",
                "PECAS - Import View from URL",
                None))
