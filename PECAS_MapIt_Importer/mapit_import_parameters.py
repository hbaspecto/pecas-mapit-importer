# -*- coding: utf-8 -*-

from pprint import (
    pformat,
)

from .mapit_capabilities_parsers import (
    MapitWFSCapabilitiesParser,
    MapitWMSCapabilitiesParser,
)
from .mapit_config import (
    MapitConfig,
    showConsole,
)
from .mapit_styler import (
    MapitStyler,
)


class MapitImportParameters(object):

    layername = None

    projecturl = None

    numviews = None

    servicetypes = None

    featureclasses = None

    wmslayers = None

    charttype = None

    generate = False

    views = None

    @classmethod
    def fromImportDialog(cls, viewspecs, dlg):

        mapitcat = None

        if viewspecs:

            if isinstance(viewspecs, str):

                viewspecs = [viewspecs]

            mapurls = [dlg.getMapUrl(vs) for vs in viewspecs]

            mapitcat = dlg.lookup.getViewCategoryFromViewSpec(viewspecs[0])

        else:

            mapurls = [dlg.getMapUrl()]

            mapitcat = dlg.lookup.getViewCategoryFromMapFile(mapurls[0])

        return cls(

            viewspecs=viewspecs,

            layername=dlg.getLayerName(),

            mapurls=mapurls,

            numviews=MapitConfig.getNumViews(dlg.schema),

            mapitcat=mapitcat,

            servicetypes=dlg.getServiceTypes(),

            featureclasses=dlg.getFeatureClassNames(),

            wmslayers=dlg.getWMSLayers(),

            chartattrib=dlg.schema.attrib,

            istimeseries=(dlg.schema.mode == MapitConfig.SCM_MODE_TIMESERIES),

            generate=(dlg.getViewTab() == dlg.OPTION_TAB_NEWVIEW)

        )

    def __str__(self):

        return pformat(dict([(f, str(getattr(self, f))) for f in dir(self) if not f.startswith('_')]))

    def getNumViews(self):

        return len(self.views) or self.numviews

    def __init__(self, viewspecs, layername, mapurls, numviews, mapitcat, servicetypes,

                 featureclasses, wmslayers, chartattrib, istimeseries, generate):

        self.numviews = numviews

        self.layername = layername

        self.servicetypes = servicetypes

        self.featureclasses = featureclasses

        self.wmslayers = wmslayers

        self.generate = generate

        viewparams = list(zip(viewspecs, mapurls, [servicetypes] * numviews))

        self.views = [MapitImportView(*vp) for vp in viewparams]

        if self.views:

            mapurl0 = self.views[0].mapurl

            self.projecturl = MapitConfig.getProjectUrl(mapurl0)

            if not mapitcat:

                # Probably means it doesn't exist in the view list (and so wasn't returned

                #  by the API call). Try to figure it out from the filename.

                if '_yr_' in mapurl0:

                    mapitcat = 'Yr'

                elif '_act_' in mapurl0:

                    mapitcat = 'Act'

            showConsole('MapitCat: {}'.format(mapitcat))

            self.mapitcat = mapitcat

            capabilities = self.views[0].wfs_caps

            print("Capabilities: ", capabilities)

            if MapitConfig.OPTION_ST_WMS in servicetypes:

                capabilities = self.views[0].wms_caps

            if MapitConfig.OPTION_ST_WFS in servicetypes:

                capabilities = self.views[0].wfs_caps

            print("Capabilities: ", capabilities)

            abstract = capabilities.getAbstract()

            if chartattrib is None:

                chartattrib = abstract.split()[2]

            if istimeseries is None:

                istimeseries = 'between' in abstract.split()

            if chartattrib in ['quantity'] and not istimeseries:

                self.charttype = MapitStyler.CHART_TYPE_PIE

            else:

                self.charttype = MapitStyler.CHART_TYPE_BAR

        showConsole("Created Import Params with:\n    {}".format(self))


class MapitImportView(object):

    viewspec = None

    mapurl = None

    wms_caps = None

    wfs_caps = None

    def __str__(self):

        return pformat(dict([(f, str(getattr(self, f))) for f in dir(self) if not f.startswith('_')]))

    def __init__(self, viewspec, mapurl, servicetypes):

        print("Creating view for ", viewspec, mapurl, servicetypes)

        self.viewspec = viewspec

        self.mapurl = mapurl

        if MapitConfig.OPTION_ST_WMS in servicetypes:

            self.wms_caps = MapitWMSCapabilitiesParser(mapurl, MapitConfig.DEFAULT_WMS_VERSION)

        if MapitConfig.OPTION_ST_WFS in servicetypes:

            self.wfs_caps = MapitWFSCapabilitiesParser(mapurl, MapitConfig.DEFAULT_WFS_VERSION)

    @property
    def wlayername(self):

        if not self.wms_caps:

            return None

        return self.wms_caps.getLayerName()
