# -*- coding: utf-8 -*-

# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load MapitImportPlugin class from file MapitImportPlugin.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    from .hbaspecto_mapit_importer import (
        MapitImportPlugin,
    )
    return MapitImportPlugin(iface)
