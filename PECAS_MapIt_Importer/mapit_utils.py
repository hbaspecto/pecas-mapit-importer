# -*- coding: utf-8 -*-

from json import \
    dumps as \
    dumpjson

from PyQt5.QtCore import (
    QSettings,
)

from .mapit_capabilities_parsers import (
    MapitWFSCapabilitiesParser,
)
from .mapit_config import (
    MapitConfig,
    showConsole,
)
from .mapit_import_parameters import (
    MapitImportParameters,
)
from .mapit_lookup_if import (
    MapitAPILookup,
)
from .mapit_plugin_predef_map_dialog import (
    MapitPredefMapDialog,
)
from .mapit_plugin_progressbar import (
    MapitProgressDialog,
)


class MapitPluginUtil(object):

    def run(self):

        raise NotImplementedError

    def __init__(self, plugin, schema, lookup):

        self.plugin = plugin

        self.schema = schema

        self.lookup = lookup

        self.settings = QSettings(MapitConfig.APP_ORG, MapitConfig.APP_NAME)

        self.run()


class MapitURLLoader(MapitPluginUtil):

    defaultsettings = {

        'importwms': True,

        'importwfs': True,

        'projecturl': MapitConfig.MAPIT_URL_HOST,

        'wfsfeatures': 'onlyzonefeatures'

    }

    def determineLayerTypes(self, mapfile):

        print("************** Default settings is ", list(self.defaultsettings.items()))

        savedsettings = dict([

            (key, self.settings.value(('HBA_Mapit_' + key), default))

            for (key, default) in list(self.defaultsettings.items())

        ])

        print ("savedsettings is ", savedsettings)

        services = [

            (MapitConfig.OPTION_ST_WMS, 'importwms'),

            (MapitConfig.OPTION_ST_WFS, 'importwfs')

        ]

        print("services is ", services)

        importservices = [service for (service, key) in services if savedsettings[key]]

        print ("importservices is ", importservices)

        if MapitConfig.OPTION_ST_WFS in importservices:

            mapurl = MapitConfig.getMapPath(mapfile, self.projecturl)

            wfscapabil = MapitWFSCapabilitiesParser(mapurl, MapitConfig.DEFAULT_WFS_VERSION)

            classnames = wfscapabil.getFeatureClassNames()

            if savedsettings['wfsfeatures'] == 'onlyzonefeatures':

                featureclasses = classnames[-1:]

            else:

                featureclasses = classnames[:-2] + classnames[:-3:-1]

        else:

            featureclasses = []

        showConsole((importservices, featureclasses))

        return (importservices, featureclasses)

    def run(self):

        # TODO: For now just throw up an inputbox or two. Later we'll onboard a full

        #  dialog box to do get the url/layer name, configure it properly, etc...

        title = "Enter View URL from Mapit."

        error_msg = ""

        label = (

            "{}"

            "Enter a Mapit URL here to import the view map into QGIS.\n"

            "This works best if you copy/paste the 'WFS/WMS' link (will have a filename that ends in '.map') "

            "although links to the 'Interactive View' pages will also work in some cases:"

        )

        url_ok = False

        while not url_ok:

            url = self.plugin.inputBox(label.format(error_msg), coerce=str.strip, title=title)

            if url is None:

                # User canceled

                return None

            elif not url:

                error_msg = "No URL entered. Please try again:\n\n"

            else:

                error_msg = "No map verified at specified URL. Please check URL and try again:\n\n"

                try:

                    self.projecturl = MapitConfig.getProjectUrl(url)

                    self.lookup = MapitAPILookup(self.projecturl)

                    (viewspec, mapfile) = self.lookup.getMapViewFromURL(url)

                    if viewspec:

                        confirm_msg = (

                            "URL Entered:\n    {}\n\n"

                            "The following view was found at the specified URL and will be used "

                            "as the QGIS layer name.\n"

                            "Please change and/or confirm layer name:"

                        ).format(url)

                        layername = self.plugin.inputBox(confirm_msg, viewspec, coerce=str.strip,

                                                         title="Confirm view/layer name")

                        if layername is None:

                            # User canceled

                            return None

                        else:

                            url_ok = True

                except TypeError:

                    pass

        mapitcat = self.lookup.getViewCategoryFromViewSpec(viewspec)

        mapurl = MapitConfig.getMapPath(mapfile, MapitConfig.getProjectUrl(url))

        if mapfile and (not mapfile.endswith('.map')):

            mapfile += '.map'

        (servicetypes, featureclasses) = self.determineLayerTypes(mapfile)

        showConsole(mapurl)

        ip = MapitImportParameters(

            viewspecs=[viewspec],

            layername=layername,

            mapurls=[mapurl],

            numviews=1,

            mapitcat=mapitcat,

            servicetypes=servicetypes,

            featureclasses=featureclasses,

            wmslayers=[viewspec],

            chartattrib=None,          # forces importparam.__init__() to infer it from getCapabilities

            istimeseries=None,  # ditto

            generate=False

        )

        progbar = MapitProgressDialog("PECAS MapIt Importer Plugin: Importing Layers")

        progress = self.plugin.determineImportSteps(ip, progbar)

        self.plugin.runImportView(ip, progress)

    def __init__(self, plugin, schema, lookup):

        self.projecturl = self.defaultsettings['projecturl']

        super(MapitURLLoader, self).__init__(plugin, schema, lookup)


class MapitPredefinedMap(MapitPluginUtil):

    pecas_mapit_module = 'year_range'

    pecas_output_module = 'onescen'

    output_csv_file = None

    selyear = False

    numscen = 1

    scen1 = ''

    scen2 = None

    attrib = 'quantity'

    aggfunc = 'sum'

    startYear = 2011

    endYear = None

    qty_roh = 'quantity'

    operator = '-'

    output_csv_file = 'ActivityLocations.csv'

    attrtype = 'group'

    attrlist = ['Households']

    mkuseorbuysell = ''

    viewspecs = []

    mapurls = []

    featureclasses = [''] * 3

    wmslayers = ['']

    class MapitUserInputError(Exception):

        pass

    @property
    def layername_scen(self):

        if self.numscen == 1:

            return self.scen1

        else:

            return "{} and {}".format(self.scen1, self.scen2)

    @property
    def mapit_vtype(self):

        return {'year_range': 'Yr', 'single_year': 'Act'}[self.pecas_mapit_module]

    @property
    def to_import(self):

        return {

            'viewspecs': self.viewspecs,

            'layername': "Predefined map {} on {}".format(self.mapid, self.layername_scen),

            'mapurls': self.mapurls,

            'numviews': 1,

            'mapitcat': self.mapit_vtype,

            'servicetypes': self.svctypes,

            'featureclasses': self.featureclasses,

            'wmslayers': self.wmslayers,

            'chartattrib': self.attrib,

            'istimeseries': (self.pecas_mapit_module == 'year_range'),

            'generate': True

        }

    @property
    def creation_params(self):

        cparams = {

            'aggFunc': self.aggfunc,

            'attribute': self.attrib,

            'selected_file': self.lookup.getModelFileID(self.output_csv_file),

            'mkUseOrBuySell': self.mkuseorbuysell,

            'num_scen': ['', 'one', 'two'][self.numscen],

            'pltActOrCom': 'act',

            'scenario1': self.scen1,

            'sltMethod': 'sltList',

            'activityCondition': self.attrlist,

            'actList1': dumpjson(self.attrlist),

            'actType': self.attrtype,

            'startyear': self.startYear,

            'endyear': self.endYear

        }

        if self.numscen > 1:

            cparams.update({

                'scenario2': self.scen2,

                'quantityRofH': self.qty_roh,

                'operator': self.operator

            })

        return cparams

    def getUserInput(self, caption=None):

        caption = caption or ("Enter parameters for predefined map {}:".format(self.mapid))

        params = MapitPredefMapDialog.InputParams(

            caption=caption,

            numscen=self.numscen,

            selyear=self.selyear,

            altname="policy"

        )

        dlg = MapitPredefMapDialog(lookup=self.lookup, input_params=params)

        dlg.show()

        if dlg.exec_() == dlg.Rejected:

            raise self.MapitUserInputError

        self.scen1 = dlg.base_scenario

        if self.numscen == 2:

            self.scen2 = dlg.alt_scenario

        if self.selyear:

            self.endYear = dlg.end_year

        self.svctypes = dlg.service_types

    def createView(self):

        return self.lookup.createView(self.mapit_vtype, self.creation_params)

    def setupMapParameters(self):

        go = False

        if self.mapid == 'A':

            go = True

        elif self.mapid == 'B':

            self.numscen = 2

            self.selyear = True

        elif self.mapid == 'C':

            self.numscen = 2

            self.selyear = True

        elif self.mapid == 'D':

            self.numscen = 2

        elif self.mapid == 'E':

            self.numscen = 2

            self.qty_roh = 'locationutility'

        self.getUserInput()

        if not self.selyear:

            endyear = int(self.lookup.getYearList(self.scen1)[-1])

            if self.numscen == 2:

                endyear2 = int(self.lookup.getYearList(self.scen2)[-1])

                endyear = max(endyear, endyear2)

            self.endYear = endyear

        return go

    def run(self):

        try:

            if not self.setupMapParameters():

                raise NotImplementedError("stub/placeholder")

        except self.MapitUserInputError:

            return

        # Preliminary input specs, just for the purposes of setting up the (right number of steps

        #  in the) progress bar. Once we've created the view in mapit, we'll be able to actually

        #  determine the URL and read the WMS/WFS Capabilities to find out the specific import

        #  parameters, and will then re-create this.

        ip = MapitImportParameters(**self.to_import)

        progbar = MapitProgressDialog("PECAS MapIt Importer Plugin: Generating Predefined Map")

        progress = self.plugin.determineImportSteps(ip, progbar)

        # Ok, time to create the view

        try:

            resp = self.createView()

        except Exception:

            progress.done()

            raise

        # Now that the view exists, we can fill in the missing import parameters and re-create

        #  the Import Params object

        for view in resp:

            if 'compare' in view['viewname']:

                self.viewspecs = [view['viewname']]

                self.mapurls = []

                self.featureclasses = []

                self.wmslayers = []

                break

        ip = MapitImportParameters(**self.to_import)

        # Finally, import the scenario map

        self.plugin.runImportView(ip, progress)

    def __init__(self, plugin, schema, lookup):

        self.mapid = schema.mapid

        super(MapitPredefinedMap, self).__init__(plugin, schema, lookup)


class MapitPluginAbout(MapitPluginUtil):

    def run(self):

        msg = (

            "HBA PECAS Mapit Importer Plugin\n"

            "      for QGIS\n\n"

            "Copyright (c) 2017-2018 HBA Specto Inc.\n\n"

            "Version {0.MAJOR_VERSION}.{0.MINOR_VERSION}.{0.BUILD_VERSION}"

        ).format(MapitConfig)

        self.plugin.msgBox(msg)
