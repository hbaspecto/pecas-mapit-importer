# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mapit_plugin_predef_map_dialog_base.ui'
#
# Created: Fri Nov 01 16:03:25 2019
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import (
    QtCore,
    QtGui,
)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MapitPluginURLImportDialogBase(object):
    def setupUi(self, MapitPluginURLImportDialogBase):
        MapitPluginURLImportDialogBase.setObjectName(_fromUtf8("MapitPluginURLImportDialogBase"))
        MapitPluginURLImportDialogBase.setWindowModality(QtCore.Qt.WindowModal)
        MapitPluginURLImportDialogBase.resize(1000, 300)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MapitPluginURLImportDialogBase.sizePolicy().hasHeightForWidth())
        MapitPluginURLImportDialogBase.setSizePolicy(sizePolicy)
        MapitPluginURLImportDialogBase.setMinimumSize(QtCore.QSize(1000, 300))
        MapitPluginURLImportDialogBase.setSizeGripEnabled(True)
        self.vltDialog = QtGui.QVBoxLayout(MapitPluginURLImportDialogBase)
        self.vltDialog.setSizeConstraint(QtGui.QLayout.SetMaximumSize)
        self.vltDialog.setObjectName(_fromUtf8("vltDialog"))
        self.lblFormCaption = QtGui.QLabel(MapitPluginURLImportDialogBase)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lblFormCaption.sizePolicy().hasHeightForWidth())
        self.lblFormCaption.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(11)
        self.lblFormCaption.setFont(font)
        self.lblFormCaption.setObjectName(_fromUtf8("lblFormCaption"))
        self.vltDialog.addWidget(self.lblFormCaption)
        self.fltInput = QtGui.QFormLayout()
        self.fltInput.setContentsMargins(20, -1, -1, -1)
        self.fltInput.setObjectName(_fromUtf8("fltInput"))
        self.lblBaseScen = QtGui.QLabel(MapitPluginURLImportDialogBase)
        self.lblBaseScen.setObjectName(_fromUtf8("lblBaseScen"))
        self.fltInput.setWidget(0, QtGui.QFormLayout.LabelRole, self.lblBaseScen)
        self.hltBaseScen = QtGui.QHBoxLayout()
        self.hltBaseScen.setObjectName(_fromUtf8("hltBaseScen"))
        self.cboBaseScen = QtGui.QComboBox(MapitPluginURLImportDialogBase)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboBaseScen.sizePolicy().hasHeightForWidth())
        self.cboBaseScen.setSizePolicy(sizePolicy)
        self.cboBaseScen.setObjectName(_fromUtf8("cboBaseScen"))
        self.hltBaseScen.addWidget(self.cboBaseScen)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hltBaseScen.addItem(spacerItem)
        self.fltInput.setLayout(0, QtGui.QFormLayout.FieldRole, self.hltBaseScen)
        self.lblAltScen = QtGui.QLabel(MapitPluginURLImportDialogBase)
        self.lblAltScen.setObjectName(_fromUtf8("lblAltScen"))
        self.fltInput.setWidget(1, QtGui.QFormLayout.LabelRole, self.lblAltScen)
        self.hltAltScen = QtGui.QHBoxLayout()
        self.hltAltScen.setObjectName(_fromUtf8("hltAltScen"))
        self.cboAltScen = QtGui.QComboBox(MapitPluginURLImportDialogBase)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboAltScen.sizePolicy().hasHeightForWidth())
        self.cboAltScen.setSizePolicy(sizePolicy)
        self.cboAltScen.setObjectName(_fromUtf8("cboAltScen"))
        self.hltAltScen.addWidget(self.cboAltScen)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hltAltScen.addItem(spacerItem1)
        self.fltInput.setLayout(1, QtGui.QFormLayout.FieldRole, self.hltAltScen)
        self.lblEndYear = QtGui.QLabel(MapitPluginURLImportDialogBase)
        self.lblEndYear.setObjectName(_fromUtf8("lblEndYear"))
        self.fltInput.setWidget(2, QtGui.QFormLayout.LabelRole, self.lblEndYear)
        self.hltEndYear = QtGui.QHBoxLayout()
        self.hltEndYear.setObjectName(_fromUtf8("hltEndYear"))
        self.cboEndYear = QtGui.QComboBox(MapitPluginURLImportDialogBase)
        self.cboEndYear.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cboEndYear.sizePolicy().hasHeightForWidth())
        self.cboEndYear.setSizePolicy(sizePolicy)
        self.cboEndYear.setObjectName(_fromUtf8("cboEndYear"))
        self.hltEndYear.addWidget(self.cboEndYear)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hltEndYear.addItem(spacerItem2)
        self.fltInput.setLayout(2, QtGui.QFormLayout.FieldRole, self.hltEndYear)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.fltInput.setItem(3, QtGui.QFormLayout.FieldRole, spacerItem3)
        self.lblImportType = QtGui.QLabel(MapitPluginURLImportDialogBase)
        self.lblImportType.setObjectName(_fromUtf8("lblImportType"))
        self.fltInput.setWidget(4, QtGui.QFormLayout.LabelRole, self.lblImportType)
        self.hltImportType = QtGui.QHBoxLayout()
        self.hltImportType.setObjectName(_fromUtf8("hltImportType"))
        self.ckbImportWMS = QtGui.QCheckBox(MapitPluginURLImportDialogBase)
        self.ckbImportWMS.setObjectName(_fromUtf8("ckbImportWMS"))
        self.hltImportType.addWidget(self.ckbImportWMS)
        self.ckbImportWFS = QtGui.QCheckBox(MapitPluginURLImportDialogBase)
        self.ckbImportWFS.setChecked(False)
        self.ckbImportWFS.setObjectName(_fromUtf8("ckbImportWFS"))
        self.hltImportType.addWidget(self.ckbImportWFS)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hltImportType.addItem(spacerItem4)
        self.fltInput.setLayout(4, QtGui.QFormLayout.FieldRole, self.hltImportType)
        self.vltDialog.addLayout(self.fltInput)
        self.bbxDialog = QtGui.QDialogButtonBox(MapitPluginURLImportDialogBase)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.bbxDialog.setFont(font)
        self.bbxDialog.setOrientation(QtCore.Qt.Horizontal)
        self.bbxDialog.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.bbxDialog.setObjectName(_fromUtf8("bbxDialog"))
        self.vltDialog.addWidget(self.bbxDialog)

        self.retranslateUi(MapitPluginURLImportDialogBase)
        QtCore.QObject.connect(self.bbxDialog,
                               QtCore.SIGNAL(_fromUtf8("accepted()")),
                               MapitPluginURLImportDialogBase.accept)
        QtCore.QObject.connect(self.bbxDialog,
                               QtCore.SIGNAL(_fromUtf8("rejected()")),
                               MapitPluginURLImportDialogBase.reject)
        QtCore.QMetaObject.connectSlotsByName(MapitPluginURLImportDialogBase)

    def retranslateUi(self, MapitPluginURLImportDialogBase):
        MapitPluginURLImportDialogBase.setWindowTitle(
            _translate(
                "MapitPluginURLImportDialogBase",
                "PECAS - Enter Parameters for Predefined Map",
                None))
        self.lblFormCaption.setText(
            _translate(
                "MapitPluginURLImportDialogBase",
                "Predefined Map <insert caption here>",
                None))
        self.lblBaseScen.setText(_translate("MapitPluginURLImportDialogBase", "Base Scenario:", None))
        self.lblAltScen.setText(_translate("MapitPluginURLImportDialogBase", "Alternate Scenario:", None))
        self.lblEndYear.setText(_translate("MapitPluginURLImportDialogBase", "Run comparison up until:", None))
        self.lblImportType.setText(_translate("MapitPluginURLImportDialogBase", "Import map layer as:", None))
        self.ckbImportWMS.setText(_translate("MapitPluginURLImportDialogBase", "Raster Images (WMS)", None))
        self.ckbImportWFS.setText(_translate("MapitPluginURLImportDialogBase", "Feature Layers (WFS)", None))
