# -*- coding: utf-8 -*-

from urllib.error import (
    HTTPError,
)
from urllib.request import (
    Request,
    urlopen,
)
from xml.etree import (
    ElementTree,
)

from .mapit_config import (
    MapitConfig,
)


class MapitServiceCapabilitiesParser(object):

    DEFAULT_SERVICE_VERSION = ''

    DEFAULT_SERVICE_NAME = ''

    DEFAULT_NAMESPACE = ''

    namesp = {}

    def __init__(self, url=None, serviceversion=None, servicename=None):

        if url is not None:

            self.parseFile(url, serviceversion, servicename)

    def getAbstract(self, layer=None):

        raise NotImplementedError

    def getTitle(self, layer=None):

        raise NotImplementedError

    def getLayerName(self, layer=None):

        raise NotImplementedError

    def openFile(self, url):

        retry = False

        ok = False

        req = Request(url)

        while not ok:

            try:

                fh = urlopen(req)

                ok = True

            except HTTPError as e:

                if (getattr(e, 'code', None) == 401) and not retry:

                    authline = e.headers['www-authenticate']

                    if ':' in authline:

                        authline = authline.split(':')[1]

                    scheme = authline.split()[0]

                    if scheme.lower() != 'basic':

                        raise

                    req.add_header("Authorization", MapitConfig.getHTTPSAuthHeader())

                    retry = True

                else:

                    raise

        return fh

    def parseFile(self, url, serviceversion=None, servicename=None):

        srvcap_params = []

        if 'request=' not in url:

            srvcap_params.append('request=GetCapabilities')

        if 'service=' not in url:

            if servicename is None:

                servicename = self.DEFAULT_SERVICE_NAME

            srvcap_params.append('service=' + servicename)

        if 'version=' not in url:

            if serviceversion is None:

                serviceversion = self.DEFAULT_SERVICE_VERSION

            srvcap_params.append('version=' + serviceversion)

        if len(srvcap_params) > 0:

            if '?' in url:

                url += '&' + '&'.join(srvcap_params)

            else:

                url += '?' + '&'.join(srvcap_params)

        f = self.openFile(url)

        self.root = ElementTree.fromstring(f.read())

        if self.root.tag == 'HTML':

            # Uh-oh. This isn't a valid GetCapabilities document

            raise TypeError('GetCapabilities returned HTML')

    def walkToNode(self, nodepath, ns=None):

        if ns is not None:

            if not ns.endswith(':'):

                ns += ':'

        curnode = self.root

        for n in nodepath:

            if ns is None:

                curnode = curnode.find(n)

            else:

                curnode = curnode.find(ns + n, self.namesp)

        return curnode

    def walkToNodes(self, nodepath, ns=None):

        parent = self.walkToNode(nodepath[:-1], ns)

        if ns is None:

            return parent.findall(nodepath[-1])

        else:

            if not ns.endswith(':'):

                ns += ':'

            return parent.findall(ns + nodepath[-1], self.namesp)


class MapitWMSCapabilitiesParser(MapitServiceCapabilitiesParser):

    DEFAULT_SERVICE_NAME = 'wms'

    DEFAULT_NAMESPACE = 'wms'

    namesp = {

        'wms': 'http://www.opengis.net/wms',

        'xld': 'http://www.opengis.net/sld',

        'xsi': 'http://www.w3.org/2001/XMLSchema-instance',

        'ms': 'http://mapserver.gis.umn.edu/mapserver'

    }

    def __init__(self, url=None, serviceversion=None, servicename='wms'):

        self.DEFAULT_SERVICE_VERSION = MapitConfig.DEFAULT_WMS_VERSION

        super(MapitWMSCapabilitiesParser, self).__init__(url, serviceversion, servicename)

    def walkToNode(self, nodepath, ns='wms'):

        return super(MapitWMSCapabilitiesParser, self).walkToNode(nodepath, ns)

    def walkToNodes(self, nodepath, ns='wms'):

        return super(MapitWMSCapabilitiesParser, self).walkToNodes(nodepath, ns)

    def getAbstract(self, layer=None):

        # layer ignored - for interface consistency with WFS parser

        return self.walkToNode(['Service', 'Abstract']).text

    def getTitle(self, layer=None):

        if layer is None:

            return self.walkToNode(['Service', 'Title']).text

        else:

            return self.getFieldFromNamedLayer('Title', layer)

    def getLayerName(self, layer=None):

        if isinstance(layer, str):

            return layer

        else:

            return self.walkToNode(['Capability', 'Layer', 'Name']).text

    def getAvailableFormats(self, filters=None):

        fmts = [fmt.text for fmt in self.walkToNodes(['Capability', 'Request', 'GetMap', 'Format'])]

        if filters is not None:

            fmts = [f for f in fmts if f.split('/')[0] in filters]

        return fmts

    def getAvailableProjections(self):

        return [crs.text for crs in self.walkToNodes(['Capability', 'Layer', 'CRS'])]

    def getFieldFromNamedLayer(self, fieldname, layername):

        for layer in self.walkToNodes(['Capability', 'Layer', 'Layer']):

            if layer.find('wms:Name', self.namesp).text == layername:

                try:

                    return layer.find('wms:' + fieldname, self.namesp).text

                except AttributeError:

                    return None

    def getFieldFromLayers(self, fieldname):

        return [layer.find('wms:' + fieldname,
                           self.namesp).text for layer in self.walkToNodes(['Capability', 'Layer', 'Layer'])]

    def getLayerNames(self):

        return self.getFieldFromLayers('Name')


class MapitWFSCapabilitiesParser(MapitServiceCapabilitiesParser):

    DEFAULT_SERVICE_NAME = 'wfs'

    DEFAULT_NAMESPACE = 'wfs'

    namesp = {

        'wfs': 'http://www.opengis.net/wfs/2.0',

        'gml': 'http://www.opengis.net/gml/3.2',

        'ows': 'http://www.opengis.net/ows/1.1',

        'xlink': 'http://www.w3.org/1999/xlink',

        'xsi': 'http://www.w3.org/2001/XMLSchema-instance',

        'fes': 'http://www.opengis.net/fes/2.0',

        'ms': 'http://mapserver.gis.umn.edu/mapserver'

    }

    def __init__(self, url=None, serviceversion=None, servicename='wfs'):

        self.DEFAULT_SERVICE_VERSION = MapitConfig.DEFAULT_WFS_VERSION

        super(MapitWFSCapabilitiesParser, self).__init__(url, serviceversion, servicename)

    def walkToNode(self, nodepath, ns='wfs'):

        return super(MapitWFSCapabilitiesParser, self).walkToNode(nodepath, ns)

    def walkToNodes(self, nodepath, ns='wfs'):

        return super(MapitWFSCapabilitiesParser, self).walkToNodes(nodepath, ns)

    def getAbstract(self, featureclass=None):

        if featureclass is None:

            return self.walkToNode(['ServiceIdentification', 'Abstract'], 'ows').text

        else:

            return self.getFieldFromNamedFeatureClass('Abstract', featureclass)

    def getTitle(self, featureclass=None):

        if featureclass is None:

            return self.walkToNode(['ServiceIdentification', 'Title'], 'ows').text

        else:

            return self.getFieldFromNamedFeatureClass('Title', featureclass)

    def getLayerName(self, featureclass=None):

        # This is a bit dumb - mostly for interface compatibility with WMS parser

        if featureclass is None:

            raise AttributeError

        else:

            return featureclass

    def getDefaultProjection(self, featureclass):

        proj = self.getFieldFromNamedFeatureClass('DefaultCRS', featureclass)

        # Not sure why it's a double colon, but QGIS don't like it
        return proj[(proj.find('crs:') + 4):].replace('::', ':')

    def getFieldFromNamedFeatureClass(self, fieldname, featureclass):

        for feature in self.walkToNodes(['FeatureTypeList', 'FeatureType']):

            if feature.find('wfs:Name', self.namesp).text == featureclass:

                try:

                    return feature.find('wfs:' + fieldname, self.namesp).text

                except AttributeError:

                    return None

    def getFieldFromFeatureClasses(self, fieldname):

        return [feature.find('wfs:' + fieldname,
                             self.namesp).text for feature in self.walkToNodes(['FeatureTypeList', 'FeatureType'])]

    def getFeatureClassNames(self):

        return self.getFieldFromFeatureClasses('Name')

    def getFeatureClassTitles(self):

        return self.getFieldFromFeatureClasses('Title')
