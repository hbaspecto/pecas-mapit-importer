# -*- coding: utf-8 -*-

from os import \
    path as \
    ospath

from PyQt5 import (
    QtCore,
    uic,
)
from PyQt5.QtCore import \
    Qt as \
    QtConstants
from PyQt5.QtGui import (
    QColor,
    QPainter,
)
from PyQt5.QtWidgets import (
    QDialog,
    QProgressBar,
    qApp,
)

(FORM_CLASS, _) = uic.loadUiType(ospath.join(ospath.dirname(__file__), 'mapit_plugin_progressbar_base.ui'))


class MapitProgressDialog(QDialog, FORM_CLASS):

    cancelled = False

    tickscolor = QColor('#000000')

    def __init__(self, titlebar=None, progress=0, caption="", tickmarks=None, parent=None):

        super(MapitProgressDialog, self).__init__(parent)

        self.setupUi(self)

        if titlebar is None:

            self.setWindowFlags(QtConstants.FramelessWindowHint)

        else:

            self.setWindowTitle(titlebar)

        self.setTickmarks(tickmarks)

        self.setProgress(progress, caption)

    def setupUi(self, window):

        super(MapitProgressDialog, self).setupUi(window)

        self.btnCancel.clicked.connect(self.onBtnCancel_clicked)

    def isCancelled(self):

        return self.cancelled

    def cancel(self):

        self.cancelled = True

        self.btnCancel.setEnabled(False)

        self.lblStatus.setText("Cancelling...")

        self.lblStatus.setEnabled(False)

        self.prgProgress.setEnabled(False)

    def setProgress(self, progress=None, caption=None):

        if self.isCancelled():

            raise StopIteration

        if progress is not None:

            if 0 <= progress <= 100:

                self.prgProgress.setValue(progress)

        if caption is not None:

            self.lblStatus.setText(caption)

        qApp.processEvents()

    def paintProgress(self, event):

        painter = QPainter(self.prgProgress)

        painter.setPen(self.tickscolor)

        QProgressBar.paintEvent(self.prgProgress, event)

        painter.drawLine(0, 0, self.prgProgress.width(), self.prgProgress.height())

    def setTickmarks(self, groups=None, color=None):

        if color is not None:

            self.tickscolor = color

        if groups is not None:

            # TODO: intercept the progressbar widget's paint event, blah blah blah

            #self.prgProgress.paintEvent = self.paintProgress

            pass

    def bringToFront(self):

        winstate = (self.windowState() & ~QtConstants.WindowMinimized)

        winstate |= QtConstants.WindowActive

        self.setWindowState(winstate)

        self.activateWindow()

    def show(self):

        super(MapitProgressDialog, self).show()

        self.bringToFront()

    def onBtnCancel_clicked(self):

        self.cancel()


class MapitProgress(object):

    DEFAULTINITCAPTION = "Initializing..."

    DEFAULTPROGRESSCAPTION = "..."

    DEFAULTWEIGHT = 1

    def __init__(self, dlg=None, defaultcaption=None):

        self.steps = []

        self.weights = {}

        self.captions = {}

        self.total = 0

        if dlg is None:

            self.dlg = MapitProgressDialog()

        else:

            self.dlg = dlg

        if defaultcaption is None:

            defaultcaption = self.DEFAULTINITCAPTION

        self.dlg.show()

        self.dlg.setProgress(0, defaultcaption)

    def hasSteps(self, steptype):

        return len([s for s in self.steps if s == steptype])

    def addSteps(self, steptype, n=1, weight=None, caption=None):

        if steptype not in list(self.weights.keys()):

            if weight is None:

                self.weights[steptype] = self.DEFAULTWEIGHT

            else:

                self.weights[steptype] = weight

        if steptype not in list(self.captions.keys()):

            if caption is None:

                self.captions[steptype] = self.DEFAULTPROGRESSCAPTION

            else:

                self.captions[steptype] = caption

        self.steps = self.steps + ([steptype] * n)

        self.total += (self.weights[steptype] * n)

    def removeSteps(self, steptype, upto=None):

        n = self.hasSteps(steptype)

        if n > 0:

            if upto is not None:

                n = max(n, upto)

            weight = self.weights[steptype]

            while n:

                self.steps.remove(steptype)

                self.total -= weight

                n -= 1

    def getProgress(self):

        if (not hasattr(self, 'progress')) or len(self.progress) == 0:

            return 0.0

        stepweights = [self.weights[s] for s in self.steps]

        stepcompletion = float(sum([s * p for (s, p) in zip(stepweights, self.progress)]))

        return stepcompletion / self.total

    def getCaption(self):

        if (not hasattr(self, 'progress')) or len(self.progress) == 0:

            curstep = 0

        else:

            curstep = len(self.progress)

            if (self.progress[-1] == 1) or curstep == len(self.steps):

                curstep -= 1

        return self.captions[self.steps[curstep]]

    def ready(self):

        self.dlg.setTickmarks(self.stepGroups())

        self.progress = []

    def makeProgress(self, pct=1):

        # Will return False if StopIteration trapped on the dialog box,

        #  i.e.: if the user hit "cancel" since last update. This should

        #  be handled appropriately by caller.

        if len(self.progress) == 0:

            curstepprogress = 1

        else:

            curstepprogress = self.progress[-1]

        if curstepprogress == 1:

            self.progress.append(pct)

        else:

            self.progress[-1] = min(1, curstepprogress + pct)

        if self.progress[-1] >= 0.995:

            self.progress[-1] = 1

        try:

            self.dlg.setProgress(self.getProgress() * 100, self.getCaption())

        except StopIteration:

            self.dlg.hide()

            return False

        return True

    def rollback(self, pct=None):

        # FIXME: because makeProgress() is lossy, trying to roll

        #  back a fractional step gets hairy. I'm not convinced

        #  it's worth trying to properly implement that noise.

        if pct is None:

            self.progress = self.progress[:-1]

        else:

            self.progress[-1] = max(0, self.progress[-1] - pct)

    def stepGroups(self):

        if len(self.steps) == 0:

            return []

        sofar = 0

        groups = []

        count = 0

        prev = self.steps[0]

        for s in self.steps:

            if s == prev:

                count += 1

            else:

                sofar += (count * self.IMPORTWEIGHTS[prev])

                groups.append(float(sofar) / self.total)

                prev = s

                count = 0

        return groups

    def done(self):

        if hasattr(self, 'dlg'):

            self.dlg.hide()

            del self.dlg


class MapitImportProgress(MapitProgress):

    STEP_GENERATE = 0

    STEP_WMSIMPORT = 1

    STEP_WFSIMPORT = 2

    STEP_WFSSTYLE = 3

    STEP_DISPLAY = 2

    # The weights for the various steps are based on hand tuning,

    #  derived from trial-and-error. Could probably stand to be

    #  tuned more tightly... although the RoI might not be there.

    IMPORTWEIGHTS = {

        STEP_GENERATE: 1,

        STEP_WMSIMPORT: 3,

        STEP_WFSIMPORT: 3,

        STEP_WFSSTYLE: 8,

        STEP_DISPLAY: 2

    }

    IMPORTCAPTIONS = {

        STEP_GENERATE: "Generating view from parameters...",

        STEP_WMSIMPORT: "Importing raster images (WMS)...",

        STEP_WFSIMPORT: "Importing feature layers (WFS)...",

        STEP_WFSSTYLE: "Applying styles and charts to feature layer...",

        STEP_DISPLAY: "Displaying imported layers..."

    }

    def __init__(self, dlg=None):

        super(MapitImportProgress, self).__init__(dlg, "Collecting import specifications...")

        self.weights = self.IMPORTWEIGHTS

        self.captions = self.IMPORTCAPTIONS
