# -*- coding: utf-8 -*-

from html.parser import (
    HTMLParser,
)
from urllib.request import (
    urlopen,
)

PP_START = 0

PP_CHART_DIV = 1

PP_DESCRIPTION_BODY = 2

PP_DESCRIPTION_HEAD_P = 3

PP_DESCRIPTION_HEAD_TXT = 4

PP_DESCRIPTION_CONTENT_P = 5

PP_FINISHED = 6


class MapitSrcParser(HTMLParser):

    def __init__(self, url=None):

        HTMLParser.__init__(self)

        self.all_data = []

        self.description = ""

        self.parsepoint = PP_START

        if url is not None:

            self.feedFile(url)

    def feedFile(self, url):

        f = urlopen(url)

        self.feed(f.read())

    def handle_starttag(self, tag, attrs):

        if tag == 'div':

            if len(attrs) > 0:

                if attrs[0] == ('id', 'chart_div'):

                    self.parsepoint = PP_CHART_DIV

        elif tag == 'body':

            if self.parsepoint == PP_CHART_DIV:

                self.parsepoint = PP_DESCRIPTION_BODY

        elif tag == 'p':

            if self.parsepoint == PP_DESCRIPTION_BODY:

                self.parsepoint = PP_DESCRIPTION_HEAD_P

            elif self.parsepoint == PP_DESCRIPTION_HEAD_TXT:

                self.parsepoint = PP_DESCRIPTION_CONTENT_P

    def handle_data(self, data):

        if self.parsepoint == PP_DESCRIPTION_HEAD_P:

            if data.strip().lower() == 'description':

                self.parsepoint = PP_DESCRIPTION_HEAD_TXT

        elif self.parsepoint == PP_DESCRIPTION_CONTENT_P:

            self.description = data

            self.parsepoint = PP_FINISHED

    def getDescription(self):

        return self.description
