# -*- coding: utf-8 -*-

from collections import (
    namedtuple,
)
from os import \
    path as \
    ospath

from PyQt5 import (
    QtCore,
    uic,
)
from PyQt5.QtCore import (
    QSettings,
)
from PyQt5.QtCore import \
    Qt as \
    QtConstants
from PyQt5.QtWidgets import (
    QDialog,
    QDialogButtonBox,
)

from .mapit_config import (
    MapitConfig,
    showConsole,
    showConsoleF,
)

(FORM_CLASS, _) = uic.loadUiType(ospath.join(ospath.dirname(__file__), 'mapit_plugin_predef_map_dialog_base.ui'))


class MapitPredefMapDialog(QDialog, FORM_CLASS):

    InputParams = namedtuple('InputParams', ['caption', 'numscen', 'selyear', 'altname'])

    class MapitInputOutOfScopeError(Exception):

        pass

    class Inputs(object):

        basescen = ''

        altscen = ''

        endyear = 0

        importwms = []

        importwfs = []

        def __str__(self):

            return str(dict([(v, getattr(self, v)) for v in dir(self) if not v.startswith('_')]))

    _numscen = 1

    _selyear = False

    _altname = "Alternate"

    _caption = ""

    _inputs = Inputs()

    @property
    def base_scenario(self):

        return self._inputs.basescen

    @property
    def alt_scenario(self):

        if self._numscen == 1:

            raise self.MapitInputOutOfScopeError

        return self._inputs.altscen

    @property
    def end_year(self):

        if not self._selyear:

            raise self.MapitInputOutOfScopeError

        return self._inputs.endyear

    @property
    def service_types(self):

        return self._inputs.importwms + self._inputs.importwfs

    def __init__(self, parent=None, lookup=None, input_params=None):
        """Constructor."""

        super(MapitPredefMapDialog, self).__init__(parent)

        self.lookup = lookup

        self.settings = QSettings(MapitConfig.APP_ORG, MapitConfig.APP_NAME)

        if input_params:

            self._numscen = input_params.numscen

            self._selyear = input_params.selyear

            self._altname = input_params.altname

            self._caption = input_params.caption

        self.setupUi(self)

    def setupUi(self, window):

        super(MapitPredefMapDialog, self).setupUi(window)

        self.cboBaseScen.currentIndexChanged.connect(self.onCboBaseScen_currentIndexChanged)

        self.cboAltScen.currentIndexChanged.connect(self.onCboAltScen_currentIndexChanged)

        self.cboEndYear.currentIndexChanged.connect(self.onCboEndYear_currentIndexChanged)

        self.ckbImportWMS.stateChanged.connect(self.onCkbImportWMS_stateChanged)

        self.ckbImportWFS.stateChanged.connect(self.onCkbImportWFS_stateChanged)

        self.lblFormCaption.setText(self._caption)

        self.scenarios = self.lookup.getScenarioList()

        self.years = [2011]

        self.setWidgetList(self.cboBaseScen, self.scenarios, self.getQSetting('basescenario', self.scenarios[0]))

        show_altscen = (self._numscen > 1)

        self.lblAltScen.setVisible(show_altscen)

        self.cboAltScen.setVisible(show_altscen)

        if show_altscen:

            altname = self._altname.capitalize()

            scen_bytype = [scen for scen in self.scenarios if scen.startswith(altname[0])]

            selectedalt = scen_bytype[0] if scen_bytype else self.scenarios[0]

            self.lblAltScen.setText("{} Scenario:".format(altname))

            self.setWidgetList(self.cboAltScen, self.scenarios, selectedalt)

        show_endyear = (self._selyear)

        self.lblEndYear.setVisible(show_endyear)

        self.cboEndYear.setVisible(show_endyear)

        self.cboEndYear.enabled = False

        self.ckbImportWMS.setCheckState(self.getQSetting('importwms', 0))

        self.ckbImportWFS.setCheckState(self.getQSetting('importwfs', 0))

        self._setOkEnabled()

    def show(self, bring_to_front=True):

        super(MapitPredefMapDialog, self).show()

        if bring_to_front:

            self.bringToFront()

    def setWidgetList(self, widget, itemlist, selectitem=None):

        widget.clear()

        if len(itemlist) > 0:

            widget.addItems([str(item) for item in itemlist])

            if selectitem is not None:

                if itemlist:

                    try:

                        idx = itemlist.index(selectitem)

                    except ValueError:

                        idx = 0

                else:

                    idx = -1

                widget.setCurrentIndex(idx)

    def validate(self):

        # showConsole(self._inputs)

        valid = bool(self.base_scenario)

        try:

            valid = valid and bool(self.alt_scenario)

        except self.MapitInputOutOfScopeError:

            pass

        try:

            valid = valid and bool(self.end_year)

        except self.MapitInputOutOfScopeError:

            pass

        valid = valid and bool(self.service_types)

        return valid

    def getQSetting(self, key, default=None):

        return self.settings.value(('HBA_Mapit_predef_' + key), default)

    def setQSetting(self, key, value):

        self.settings.setValue(('HBA_Mapit_predef_' + key), value)

    def _setOkEnabled(self, enabled=None):

        if enabled is None:

            enabled = self.validate()

        self.bbxDialog.button(QDialogButtonBox.Ok).setEnabled(enabled)

    def _getYearList(self):

        years = self.lookup.getYearList(self.base_scenario)[:]

        if self._numscen > 1 and self.alt_scenario:

            years2 = self.lookup.getYearList(self.alt_scenario)[:]

            years = [yr for yr in years if yr in years2]

        return sorted(years)

    def _regenYearList(self):

        if self._selyear:

            self.years = self._getYearList()

            selectedyear = self._inputs.endyear

            if self.years and selectedyear not in self.years:

                if selectedyear < self.years[0]:

                    selectedyear = self.years[0]

                else:

                    selectedyear = max([yr for yr in self.years if yr < selectedyear])

            self.setWidgetList(self.cboEndYear, self.years, selectedyear)

            self.cboEndYear.setEnabled(True)

    def bringToFront(self):

        winstate = (self.windowState() & ~QtConstants.WindowMinimized)

        winstate |= QtConstants.WindowActive

        self.setWindowState(winstate)

        self.activateWindow()

    def onCboBaseScen_currentIndexChanged(self, index):

        self._inputs.basescen = self.scenarios[self.cboBaseScen.currentIndex()]

        self.setQSetting('basescenario', self._inputs.basescen)

        self._regenYearList()

        self._setOkEnabled()

    def onCboAltScen_currentIndexChanged(self, index):

        self._inputs.altscen = self.scenarios[self.cboAltScen.currentIndex()]

        self._regenYearList()

        self._setOkEnabled()

    def onCboEndYear_currentIndexChanged(self, index):

        try:

            self._inputs.endyear = self.years[self.cboEndYear.currentIndex()]

        except IndexError:

            self._inputs.endyear = 0

        self._setOkEnabled()

    def onCkbImportWMS_stateChanged(self, state):

        self._inputs.importwms = [MapitConfig.OPTION_ST_WMS] * bool(state)

        self.setQSetting('importwms', state)

        self._setOkEnabled()

    def onCkbImportWFS_stateChanged(self, state):

        self._inputs.importwfs = [MapitConfig.OPTION_ST_WFS] * bool(state)

        self.setQSetting('importwfs', state)

        self._setOkEnabled()
