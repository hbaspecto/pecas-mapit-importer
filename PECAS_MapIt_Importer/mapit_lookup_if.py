# -*- coding: utf-8 -*-

from json import \
    dumps as \
    dumpjson
from json import \
    loads as \
    loadjson
from urllib.error import (
    HTTPError,
)
from urllib.parse import (
    urlencode,
)
from urllib.request import (
    Request,
    urlopen,
)

from PyQt5.QtSql import (
    QSqlDatabase,
)

from .mapit_config import (
    MapitConfig,
    showConsole,
    showConsoleF,
)


class MapitLookup(object):

    def __init__(self):

        pass

    def getCachedAttr(self, attrname, callback_getValue):

        if not hasattr(self, attrname):

            setattr(self, attrname, callback_getValue())

        return getattr(self, attrname)

    def setProjectUrl(self, projecturl):

        self.projecturl = projecturl

    def getAllActivityList(self):

        return self.getAllAttribList('activity')

    def getActivityGroupList(self):

        return self.getAttribGroupList('activity')

    def getAllCommodityList(self):

        return self.getAllAttribList('commodity')

    def getCommodityGroupList(self):

        return self.getAttribGroupList('commodity')

    def getAllAttribList(self, atype, filter=None):

        def getVal(): return self.do_getAllAttribList(atype, filter)

        return self.getCachedAttr(atype + 'list', getVal)

    def getAttribGroupList(self, atype, filter=None):

        def getVal(): return self.do_getAttribGroupList(atype, filter)

        return self.getCachedAttr(atype + 'grouplist', getVal)

    def getGroupedActivityList(self, groupids):

        return self.getGroupedAttribList('activity', groupids)

    def getGroupedCommodityList(self, groupids):

        return self.getGroupedAttribList('commodity', groupids)

    def getScenarioList(self):

        return self.getCachedAttr('scenariolist', self.do_getScenarioList)

    def getYearList(self, scenario):

        def getVal(): return self.do_getYearList(scenario)

        return self.getCachedAttr('yearlist_' + scenario, getVal)

    def getMapViewFromURL(self, url):


        print("Getting map view from URL", url)

        if 'mapserv?' in url:

            print("Getting map view from mapfile directly")

            map_param = list([param for param in (url.split('?')[1]).split('&') if 'map=' in param])

            print ("Map_param is :", map_param)

            try:

                mapfile = map_param[0].split('=')[1].split('/')[-1]

                viewspec = self.getViewSpecFromMapFile(mapfile)

                return (viewspec, mapfile)

            except IndexError:

                return (None, None)

        elif '/mapAct' in url:

            return (url.split('/')[-1], None)

        elif '/mapYr' in url:

            (baseurl, params) = url.split('?')

            mapfile = baseurl.split('/')[-1]

            map_param = list([param for param in params[1].split('&') if 'viewName' in param])

            try:

                return (map_param[0].split('=')[1], mapfile)

            except IndexError:

                return (self.getViewSpecFromMapFile(mapfile), mapfile)

        # shrug

        return (None, None)

    def validateURL(self, url):

        # There's probably a better way to do this, but eh... it'll work for now.

        return bool(self.getMapViewFromURL(url)[0])

    def makeViewInfoFromDialog(self, dlg):

        attrtype = 'group' if dlg.getNewViewType() == dlg.OPTION_NVT_COMPARE else 'indiv'

        actorcom = dlg.schema.atype[:3]

        attrcond = dlg.getGroupsAttrsForSubmit()

        attrlist1 = dumpjson(attrcond)

        aggfunc = dlg.schema.aggfunc

        attribute = dlg.schema.attrib

        selectedfile = dlg.getSchemaFileID()

        mkuseorbuysell = 'B' if 'Consumption' in dlg.schema.id else 'S' if 'Production' in dlg.schema.id else ''

        scenario1 = dlg.getSelectedScenario()

        if dlg.getNumViews() > 1:

            numscenario = 'two'

            operator = '-'

            rulehalfqty = ''

            scenario2 = dlg.getSelectedScenario2()

        else:

            numscenario = 'one'

        # I don't think this ever gets used, but seems to be defined on the HTML
        # form end as sltDrop/sltList/sltTDM/sltNonSpatial

        sltmethod = 'sltList'

        # Finally ready to build

        creation_params = {

            'aggFunc': aggfunc,

            'attribute': attribute,

            'selected_file': selectedfile,

            'mkUseOrBuySell': mkuseorbuysell,

            'num_scen': numscenario,

            'pltActOrCom': actorcom,

            'scenario1': scenario1,

            'sltMethod': sltmethod

        }

        if actorcom == 'act':

            viewattrs = {

                'activityCondition': attrcond,

                'actList1': attrlist1,

                'actType': attrtype

            }

        else:

            viewattrs = {

                'commodityCondition': attrcond,

                'comList1': attrlist1,

                'comType': attrtype

            }

        creation_params.update(viewattrs)

        if numscenario == 'two':

            creation_params.update({

                'scenario2': scenario2,

                'quantityRofH': rulehalfqty,

                'operator': operator

            })

        return creation_params

    def makeActViewInfoFromDialog(self, dlg):

        ispositive = self.isPositive(dlg.getSelectedScenario(), dlg.getSelectedYear(), dlg.schema.atype,

                                     dlg.getNewViewType(), dlg.getSelectedGroupsAttrs())

        mkpie = 'Yes' if ispositive else 'No'

        yearrun = dlg.getSelectedYear()

        creation_params = self.makeViewInfoFromDialog(dlg)

        creation_params.update({

            'mkPie': mkpie,

            'year_run': yearrun

        })

        return creation_params

    def makeYrViewInfoFromDialog(self, dlg):

        startyear = dlg.getSelectedYear()

        endyear = dlg.getSelectedYear2()

        creation_params = self.makeViewInfoFromDialog(dlg)

        creation_params.update({

            'startyear': startyear,

            'endyear': endyear,

            'quantityRofH': 'quantity'

        })

        return creation_params

    #### STUBS THAT NEED TO BE IMPLEMENTED BY SUBCLASSES ####

    def getLookupSourceString(self):

        raise NotImplementedError

    def do_getAllAttribList(self, atype, filter):

        raise NotImplementedError

    def do_getAttribGroupList(self, atype, filter):

        raise NotImplementedError

    def do_getScenarioList(self):

        raise NotImplementedError

    def do_getYearList(self, scenario):

        raise NotImplementedError

    def do_getViewList(self, scenario, year, attrtype):

        raise NotImplementedError

    def getGroupedAttribList(self, atype, groupids=[]):

        raise NotImplementedError

    def isPositive(self, scenario, year, attrtype='activity', viewtype='individ', groupsattrs=[]):

        raise NotImplementedError

    def createView(self, mapit_vtype, creation_params):

        raise NotImplementedError

    def createViewFromDialog(self, dlg):

        raise NotImplementedError

    def getMapNameFromViewSpec(self, viewspec):

        raise NotImplementedError

    def getViewSpecFromMapFile(self, mapfile):

        raise NotImplementedError

    def getModelFileID(self, filename='ActivityLocations'):

        raise NotImplementedError

    def getViewList(self, scenario, year, attrtype='activity'):

        raise NotImplementedError


class MapitDBLookup(MapitLookup):

    def __init__(self):

        super(MapitDBLookup, self).__init__()

        self.mkDBConnection()

    def getLookupSourceString(self):

        return "database"

    def mkDBConnection(self):

        db = QSqlDatabase('QPSQL')

        if db.isValid():

            db.setHostName(MapitConfig.DB_HOSTNAME)

            db.setDatabaseName(MapitConfig.DB_DATABASE)

            db.setUserName(MapitConfig.DB_USERNAME)

            db.setPassword(MapitConfig.DB_PASSWORD)

            db.setPort(MapitConfig.DB_PORT)

            self.db = db

    def connectDB(self):

        if not hasattr(self, 'db'):

            self.mkDBConnection()

        if not self.db.isOpen():

            if not self.db.open():

                raise EnvironmentError

    def getDBList(self, sql, cols=1):

        self.connectDB()

        results = []

        qry = self.db.exec_(sql)

        while next(qry):

            if cols == 1:

                results.append(qry.record().value(0))

            else:

                results.append([qry.record().value(c) for c in range(0, cols)])

        return results

    def do_getAllAttribList(self, atype, filter):

        sql = "select $ATYPEnumber, $ATYPE, $ATYPE_type_id from output.$ATYPE_numbers order by $ATYPE;"

        return self.getDBList(sql.replace('$ATYPE', atype), 3)

    def do_getAttribGroupList(self, atype, filter):

        sql = "select $ATYPE_type_id, $ATYPE_type_name from output.$ATYPE_types order by $ATYPE_type_name;"

        return self.getDBList(sql.replace('$ATYPE', atype), 2)

    def do_getScenarioList(self):

        sql = "select distinct scen_name from output.loaded_scenarios order by scen_name;"

        return self.getDBList(sql)

    def do_getYearList(self, scenario):

        self.connectDB()

        sql = "select start_year, end_year from output.loaded_scenarios where scen_name = '{}';".format(scenario)

        qry = self.db.exec_(sql)

        next(qry)

        return list(range(qry.record().value(0), qry.record().value(1) + 1))

    def getViewList(self, scenario, year, atype='activity'):

        tablespec = '_'.join([scenario, str(year), atype, '%', 'with_geom'])

        sql = "select table_name from information_schema.views where table_name like '{}' order by table_name desc;".format(
            tablespec)

        return self.getDBList(sql)

    def getGroupedAttribList(self, atype, groupids=[]):

        idspec = ', '.join([str(gid) for gid in groupids])

        sql = "select $ATYPE from output.$ATYPE_numbers where $ATYPE_type_id in ({}) order by $ATYPEnumber;".format(
            idspec)

        return self.getDBList(sql.replace('$ATYPE', atype))

    def getModelFileID(self, filename='ActivityLocations'):

        sql = "select file_id from public.aa_output_files where csv_file_name = '{}.csv' and is_active = TRUE;".format(
            filename)

        return self.getDBList(sql)[0]

    def getMapNameFromViewSpec(self, viewspec):

        sql = "select map_file_name from public.view_list where view_name = '{}';".format(viewspec)

        return self.getDBList(sql)[0]

    def getViewSpecFromMapFile(self, mapfile):

        if mapfile.endswith('.map'):

            mapfile = mapfile[:-4]

        sql = "select view_name from public.view_list where map_file_name = '{}';".format(mapfile)

        print ("Getting DB list by querying database ")

        return self.getDBList(sql)[0]


class MapitAPILookup(MapitLookup):

    API_PATH = 'mapit/'

    HEADERS = {

        'User-Agent': 'PECAS-QGIS-Plugin/',

        'Accept': 'application/json'

    }

    def __init__(self, projecturl):

        super(MapitAPILookup, self).__init__()

        self.projecturl = projecturl

        if self.HEADERS['User-Agent'].endswith('/'):

            self.HEADERS['User-Agent'] += '{}.{}'.format(MapitConfig.MAJOR_VERSION, MapitConfig.MINOR_VERSION)

    def getLookupSourceString(self):

        return "server API"

    def callAPI(self, apiname, params=None):

        # Note for HTTPS the better way to do it is to use urllib2.HTTPBasicAuthHandler & HTTPPasswordMgr

        #  but for some reason that doesn't seem to work. Doing it the manual way, however, does the trick.

        url = self.projecturl + self.API_PATH + apiname + '/'

        if params is None:

            params = {}

        params['accept'] = 'application/json'

        params = urlencode(params, True)

        params = params.encode('utf-8')

        req = Request(url, params, self.HEADERS)

        showConsole("Calling API: {} with params...".format(apiname))

        showConsoleF(params, indent=4)

        ok = False

        retry = False

        while not ok:

            try:

                resp = urlopen(req).read()

                ok = True

            except HTTPError as e:

                if (getattr(e, 'code', None) == 401) and not retry:

                    authline = e.headers['www-authenticate']

                    if ':' in authline:

                        authline = authline.split(':')[1]

                    scheme = authline.split()[0]

                    if scheme.lower() != 'basic':

                        showConsole(

                            "API Lookup HTTPError:\n"

                            "    Trying to authenticate HTTPS but expecting {!r}.".format(authline)

                        )

                        raise

                    req.add_header("Authorization", MapitConfig.getHTTPSAuthHeader())

                    retry = True

                else:

                    # uhhh

                    showConsole("\n".join([

                        "API Lookup HTTPError {} ({}):".format(getattr(e, 'code', 0), getattr(e, 'msg', '')),

                        "    URL:\n        {}".format(req.get_full_url()),

                        "    Data:\n        {}".format(req.get_data()),

                        "    Headers:",

                        "\n".join(["        {}:{}".format(k, v) for (k, v) in req.header_items()])

                    ]))

                    raise

        return loadjson(resp)

    def callGuardedAPI(self, apiname, errormsg=None, params=None):

        json = self.callAPI(apiname, params)

        print("Called api and result is ", json)

        if 'success' in list(json.keys()):

            # showConsole("API succeeded with result:")

            # showConsoleF(json['success'], indent=4)

            return json['success']

        elif 'fail' in list(json.keys()):

            if errormsg is not None:

                showConsole(errormsg.format(json['fail']))

            raise ValueError

        else:

            showConsole(

                "Unable to understand response from Mapit Server "

                "(expecting either 'success' or 'fail' as a top-level JSON key):"

            )

            showConsoleF(json, indent=4)

            raise ValueError

    def getPluralATypes(self, atype):

        return 'activities' if atype == 'activity' else 'commodities'

    def getAttribsFromRawList(self, rawlist, groupstoinclude=None):

        attriblist = []

        if groupstoinclude is None:

            groupstoinclude = list(rawlist.keys())

        for grpname in groupstoinclude:

            for attrib in rawlist[grpname]['attribs']:

                attriblist.append(attrib + [rawlist[grpname]['groupid']])

        def head(lst): return lst[0]

        return sorted(attriblist, key=head)

    def do_getRawAttribList(self, atype):

        atypes = self.getPluralATypes(atype).title()

        errmsg = "Could not get {} list because {{!r}}.".format(atype)

        return self.callGuardedAPI('get' + atypes, errmsg)

    def getRawAttribList(self, atype):

        def getVal(): return self.do_getRawAttribList(atype)

        return self.getCachedAttr('raw' + atype + 'list', getVal)

    def do_getAllAttribList(self, atype, filter):

        return self.getAttribsFromRawList(self.getRawAttribList(atype))

    def do_getAttribGroupList(self, atype, filter):

        rawlist = self.getRawAttribList(atype)

        def last(lst): return lst[-1]

        return sorted([[rawlist[grpname]['groupid'], grpname] for grpname in list(rawlist.keys())], key=last)

    def do_getScenarioList(self):

        errmsg = "Could not get scenario list because {!r}."

        return self.callGuardedAPI('getScenarios', errmsg)

    def do_getYearList(self, scenario):

        errmsg = "Could not get list of years for scenario {!r} because {{!r}}.".format(scenario)

        return self.callGuardedAPI('getYears', errmsg, {'scenario': scenario})

    def getGroupedAttribList(self, atype, groupids=[]):

        if len(groupids) == 0:

            return []

        rawlist = self.getRawAttribList(atype)

        groupnames = [gn for gn in list(rawlist.keys()) if rawlist[gn]['groupid'] in groupids]

        return [attr[1] for attr in self.getAttribsFromRawList(rawlist, groupnames)]

    def isPositive(self, scenario, year, atype='activity', viewtype='individ', groupsattrs=[]):

        if viewtype == 'individ':

            paramname = self.getPluralATypes(atype)

        else:

            paramname = '{}groups'.format(atype)

        params = {

            'scenario': scenario,

            'year': year,

            paramname: groupsattrs

        }

        errmsg = "Could not determine whether specified view is all-positive because {!r}."

        return self.callGuardedAPI('isPositive', errmsg, params)

    def getMapNameFromViewSpec(self, viewspec):

        errmsg = "Could not get map name for {!r} because {{!r}}.".format(viewspec)

        return self.callGuardedAPI('getMapFileName', errmsg, {'viewspec': viewspec})

    def getViewSpecFromMapFile(self, url):

        errmsg = "Could not get view name from {!r} because {{!r}}.".format(url)

        return self.callGuardedAPI('getViewFromMapFile', errmsg, {'mapfile': url})

    def getModelFileID(self, filename='ActivityLocations'):

        if not filename.endswith('.csv'):

            filename += '.csv'

        errmsg = "Could not get model file ID for {!r} because {{!r}}.".format(filename)

        return self.callGuardedAPI('getModelFileID', errmsg, {'csvfile': filename})

    def getViewList(self, scenario, year, atype='activity'):

        params = {

            'scenario': scenario,

            'year': year,

            'attribtype': atype

        }

        errmsg = "Could not get list of existing {} views for {} / {} because {{!r}}.".format(atype, scenario, year)

        return self.callGuardedAPI('getViewList', errmsg, params)

    def getViewCategory(self, param):

        errmsg = "Could not get view category for {} {!r} because {{!r}}.".format(*list(param.items())[0])

        return self.callGuardedAPI('getViewCategory', errmsg, param)

    def getViewCategoryFromViewSpec(self, viewspec):

        return self.getViewCategory({'viewspec': viewspec})

    def getViewCategoryFromMapFile(self, url):

        return self.getViewCategory({'mapfile': url})

    def createView(self, mapit_vtype, creation_params):

        api = 'queryBuilder{}'.format(mapit_vtype)

        errmsg = "Could not create view because {!r}."

        return self.callGuardedAPI(api, errmsg, creation_params)

    def createViewFromDialog(self, dlg):

        viewTypes = {

            MapitConfig.SCM_MODE_TIMESERIES: 'Yr',

            MapitConfig.SCM_MODE_ONEYEAR: 'Act',

            MapitConfig.SCM_MODE_COMPARESCENARIO: 'Act'

        }

        vt = viewTypes[dlg.schema.mode]

        mkinfo = getattr(self, 'make{}ViewInfoFromDialog'.format(vt))

        return self.createView(vt, mkinfo(dlg))
