#
# pecas-visualizations/PECAS_MapIt_Importer/files-to-zip.txt ---
#
# This is process by "./bin/pmi-build-zip" to create the zip file.
# If The file should be in the zip file, add it here.
# (The "./PECAS_MapIt_Importer" prefix will be added when it is made.)
#
# find . -type f > files-to-zip.txt
#
#####

# put the first, so we can find it first.
./metadata.txt

# The rest.
./LICENSE.txt
./Makefile
./README.rst
./__init__.py
./hbaspecto_mapit_importer.py
./hbaspecto_mapit_importer_dialog.py
./hbaspecto_mapit_importer_dialog_base.py
./hbaspecto_mapit_importer_dialog_base.ui
./help/Makefile
./help/make.bat
./help/source/conf.py
./help/source/index.rst
./images/icon_basic.png
./images/icon_extra_hh-zoneconst.png
./images/icon_extra_util.png
./images/icon_hh.png
./images/icon_hh_ts.png
./images/icon_info.png
./images/icon_non-hh.png
./images/icon_non-hh_ts.png
./images/icon_predef_A.png
./images/icon_predef_B.png
./images/icon_predef_C.png
./images/icon_predef_D.png
./images/icon_predef_E.png
./images/icon_prices.png
./images/icon_put-cons.png
./images/icon_put-cons_ts.png
./images/icon_put-prod.png
./images/icon_put-prod_ts.png
./images/icon_timeser.png
./images/icon_url.png
./images/icon_util.png
./mapit_capabilities_parsers.py
./mapit_config.py
./mapit_https.py
./mapit_import_parameters.py
./mapit_layer_spec.py
./mapit_lookup_if.py
./mapit_plugin_predef_map_dialog.py
./mapit_plugin_predef_map_dialog_base.py
./mapit_plugin_predef_map_dialog_base.ui
./mapit_plugin_progressbar.py
./mapit_plugin_progressbar_base.py
./mapit_plugin_progressbar_base.ui
./mapit_plugin_urlimport_dialog_base.py
./mapit_plugin_urlimport_dialog_base.ui
./mapit_plugin_version.py
./mapit_src_parser.py
./mapit_styler.py
./mapit_utils.py
./pylintrc
./resources.py
./resources.qrc
