# -*- coding: utf-8 -*-

from collections import (
    namedtuple,
)
from math import (
    floor,
    log10,
)
from random import (
    random,
)

from PyQt5.QtCore import \
    QSizeF  # , QSize
from PyQt5.QtGui import \
    QColor  # , QFont
from qgis import \
    core as \
    QgsCore

from .mapit_config import (
    MapitConfig,
    showConsole,
)


def n_sig_figs(x, n=1):

    if x == 0:

        return 0

    rv = round(x, (-int(floor(log10(abs(x))))) - 1 + n)

    if floor(log10(abs(rv)) + 1) >= n:

        return int(rv)

    else:

        return rv


class MapitStyler():

    Palette = namedtuple('Palette', ['qualitative', 'sequential'])

    # Color palettes chosen by reference to colorbrewer 2.0 (http://colorbrewer2.org/)

    #  in an attempt to generate a set of colors that will best support high

    #  distinguishability and colorblind-safety, given the number of color classes

    #  needed.

    COLOR_PALETTE = Palette(

        # The predefined qualitative color scheme will work for up to 24 color classes.

        #  When n <= 24 color classes are needed, just start picking them starting at

        #  index 0 and work our way up to n.

        # If n > 24 are needed, there's no "good" scheme that will support all the above

        #  qualifications and the best fallback is probably to just generate one randomly.

        qualitative=[

            (177, 89, 40), (255, 255, 153), (106, 61, 154), (202, 178, 214),

            (255, 127, 0), (253, 191, 111), (227, 26, 28), (251, 154, 153),

            (51, 160, 44), (178, 223, 138), (31, 120, 180), (166, 206, 227),

            (141, 211, 199), (255, 255, 179), (190, 186, 218), (251, 128, 114),

            (128, 177, 211), (253, 180, 98), (179, 222, 105), (252, 205, 229),

            (217, 217, 217), (188, 128, 189), (204, 235, 197), (255, 237, 111)

        ],



        # The predefined sequential color scheme uses multiple hues in addition to

        #  varying in brightness. Best way to optimize distinguishability is to

        #  predefine the categories rather than interpolate mathematically from a

        #  color range, because human eyes are annoying and the mapping from distance

        #  within the palette to distance WRT distinguishability is non-uniform.

        # Only supports 3-9 classes. Doesn't really make sense to go outside that range.

        sequential={

            3: [(237, 248, 177), (127, 205, 187), (44, 127, 184)],

            4: [(255, 255, 204), (161, 218, 180), (65, 182, 196), (34, 94, 168)],

            5: [(255, 255, 204), (161, 218, 180), (65, 182, 196), (44, 127, 184),

                (37, 52, 148)],

            6: [(255, 255, 204), (199, 233, 180), (127, 205, 187), (65, 182, 196),

                (44, 127, 184), (37, 52, 148)],

            7: [(255, 255, 204), (199, 233, 180), (127, 205, 187), (65, 182, 196),

                (29, 145, 192), (34, 94, 168), (12, 44, 132)],

            8: [(255, 255, 217), (237, 248, 177), (199, 233, 180), (127, 205, 187),

                (65, 182, 196), (29, 145, 192), (34, 94, 168), (12, 44, 132)],

            9: [(255, 255, 217), (237, 248, 177), (199, 233, 180), (127, 205, 187),

                (65, 182, 196), (29, 145, 192), (34, 94, 168), (37, 52, 148),

                (8, 29, 88)]

        }

    )

    CHOROPLETH_CLASSES = 9

    # CHOROPLETH_MODE    = QgsCore.QgsGraduatedSymbolRendererV2.Quantile

    CHOROPLETH_MODE = QgsCore.QgsGraduatedSymbolRenderer.EqualInterval

    CHOROPLETH_FIELD = 'sum_all'

    CHART_TYPE_BAR = 1

    CHART_TYPE_PIE = 2

    ROAD_LINE_COLOR = '#000000'

    ROAD_LINE_WIDTH = 0.35

    @staticmethod
    def applyRoadStyle(qlayer):

        rend = qlayer.renderer()

        if isinstance(rend, QgsCore.QgsSingleSymbolRenderer):

            symbol = rend.symbols(QgsCore.QgsRenderContext())[0]

            if hasattr(symbol, 'width'):

                # If it doesn't have this attribute, it's not a linear feature

                #  (i.e.: it's point/polygon, or perhaps a raster layer or something),

                #  and so we can't set the style in this way (indeed it may not

                #  actually be a road layer at all?)

                symbol.setColor(QColor(MapitStyler.ROAD_LINE_COLOR))

                symbol.setWidth(MapitStyler.ROAD_LINE_WIDTH)

    def __init__(self, iface, charttype, qlayer=None):

        self.iface = iface

        self.charttype = charttype

        if qlayer is not None:

            if self.setLayer(qlayer):

                self.generateStyle()

                self.generateCharts()

    def makeChartColors(self, n):

        if n <= 24:

            def clr(i): return self.COLOR_PALETTE.qualitative[i]

        else:

            def clr(_): return (int(random() * 256), int(random() * 256), int(random() * 256))

        return [QColor(*clr(c)) for c in range(n)]

    def generateQuantiles(self, sorteddata, n):

        ntotal = len(sorteddata)

        showConsole("    (Generating {} quantiles for {} rows.)".format(n, ntotal))

        def quant(i):

            return (sorteddata[int((float(i) / n) * ntotal)],

                    sorteddata[int(((float(i) + 1) / n) * ntotal) - 1])

        return [(i, quant(i)) for i in range(n)]

    def generateIntervals(self, sorteddata, n):

        ntotal = len(sorteddata)

        showConsole("    (Generating {} equal intervals for {} rows.)".format(n, ntotal))

        data_min = min(sorteddata)

        data_max = max(sorteddata)

        rng = data_max - data_min

        span = rng / float(n)

        def interv(i):

            return (data_min + int(i) * span, data_min + (int(i) + 1) * span)

        return [(i, interv(i)) for i in range(n)]

    def _lastFieldContaining(self, words=None, fieldlist=None):

        if not words:

            return None

        if not fieldlist:

            fieldlist = [field.name() for field in self.qlayer.fields()]

        def containsword(fname): return any(word in fname for word in words)

        try:

            return list(filter(containsword, sorted(fieldlist)))[-1]

        except IndexError:

            return None

    def getFieldValues(self, fieldname=None, sortdata=True):

        showConsole("    Polling attribute table for field ranges. Enumerating fields...")

        fieldnames = [field.name() for field in self.qlayer.fields()]

        showConsole("    Determining field to use to generate choropleth.")

        if not fieldname:

            if self.CHOROPLETH_FIELD in fieldnames:

                fieldname = self.CHOROPLETH_FIELD

            else:

                # Use the last year's sum to generate the choropleth... unless we're doing a

                #  comparison, in which case use the delta. I think. This may need to be revisited.

                # choro_field_text = ['delta'] if self.isComparison() else ['sum', 'delta']

                # Ok, nope, new idea. Try delta. Fall back to the last sum if there is no delta.

                choro_search_words = ['delta', 'sum']

                showConsole((

                    "Could not find {0.CHOROPLETH_FIELD!r} in field names. "

                    "Looking for last field containing {1}"

                ).format(self, choro_search_words))

                for word in choro_search_words:

                    fieldname = self._lastFieldContaining([word], fieldnames)

                    if fieldname:

                        showConsole("Found field: {}".format(fieldname))

                        break

                if not fieldname:

                    showConsole(
                        "Could not find field containing any of {} in {}.".format(
                            choro_search_words, fieldnames))

                    return []

            self.choropleth_field = fieldname

        showConsole("    Enumerating data in {!r} field.".format(fieldname))

        showConsole("    Finding features in layer ({} expected).".format(self.qlayer.featureCount()), True)

        # This next statement SHOULD work... but for some reason it doesn't on OSX (it just hangs). However the

        #  following statement block (which should be functionally identical) works just fine. Lol I dunno.

        #       values = [ftr[fieldname] for ftr in self.qlayer.getFeatures()]

        values = []

        i = 0

        featurelist = self.qlayer.getFeatures()

        for ftr in featurelist:

            i += 1

            if (i % 100 == 0):

                print(("{} ".format(i)), end=' ')

            values.append(ftr[fieldname])

        showConsole(
            "\n        Found {} data points. Aggregating {!r} data from features.".format(
                len(values), fieldname))

        if not sortdata:

            return values

        showConsole("        Sorting data.")

        return sorted(values)

    def getFieldNames(self, includesums=False):

        if not hasattr(self, '_names'):

            showConsole("    Enumerating field names.")

            self._names = [field.name() for field in self.qlayer.fields()]

        names = self._names

        if not includesums:

            if not hasattr(self, '_last_sum_idx'):

                last_sum = self._lastFieldContaining(['sum_all', 'per'], names)

                if not last_sum:

                    # We... don't have any sums? If this happens, it probably bears further investigation.

                    raise Exception('No sum fields found.')

                self._last_sum_idx = names.index(last_sum) + 1

            names = names[self._last_sum_idx:]

        return names

    def isComparison(self):

        # So this is a hack. Got a better plan?

        return ('comparing' in self.qlayer.abstract().lower())

    def isPositive(self):

        fieldnames = self.getFieldNames()

        for ftr in self.qlayer.getFeatures():

            for fn in fieldnames:

                if ftr[fn] < 0:

                    return False

        return True

    def getMaxAttribValue(self):

        fieldnames = self.getFieldNames()

        return max([max([float(ftr[fn]) for fn in fieldnames]) for ftr in self.qlayer.getFeatures()])

    def getMinAttribValue(self):

        fieldnames = self.getFieldNames()

        return min([min([float(ftr[fn]) for fn in fieldnames]) for ftr in self.qlayer.getFeatures()])

    def generateStyle(self, fieldname=None, qlayer=None):

        showConsole("Generating WFS style...")

        if qlayer and not self.setLayer(qlayer):

            return

        n = self.CHOROPLETH_CLASSES

        generateSymbolCategory = {

            QgsCore.QgsGraduatedSymbolRenderer.Quantile: self.generateQuantiles,

            QgsCore.QgsGraduatedSymbolRenderer.EqualInterval: self.generateIntervals

        }[self.CHOROPLETH_MODE]

        if n not in list(self.COLOR_PALETTE.sequential.keys()):

            raise IndexError

        def mkRange(i, xxx_todo_changeme):

            (qmin, qmax) = xxx_todo_changeme
            symbol = QgsCore.QgsSymbol.defaultSymbol(self.qlayer.geometryType())

            symbol.setColor(QColor(*self.COLOR_PALETTE.sequential[n][i]))

            symbol.setOpacity(1)

            # "3 significant figures oughtta' be enough for anybody..."

            label = "{} - {}".format(str(n_sig_figs(qmin, 3)), str(n_sig_figs(qmax, 3)))

            return QgsCore.QgsRendererRange(qmin, qmax, symbol, label)

        dataset = self.getFieldValues(fieldname)

        showConsole("Enumerating value ranges from quantiles.")

        self.choroplethRanges = [mkRange(*symcat) for symcat in generateSymbolCategory(dataset, n)]

        showConsole("Creating QgsGraduatedSymbolRenderer")

        rend = QgsCore.QgsGraduatedSymbolRenderer(self.choropleth_field, self.choroplethRanges)

        rend.setMode(self.CHOROPLETH_MODE)

        self.styleRenderer = rend

    def chartType(self):

        if self.schema.mode == MapitConfig.SCM_MODE_TIMESERIES:

            return self.CHART_TYPE_BAR

        if self.schema.attrib in ['quantity']:

            if self.isPositive:

                return self.CHART_TYPE_PIE

        return self.CHART_TYPE_BAR

    def getChartScaling(self):

        mmPerInch = 25.4

        Scaling = namedtuple('Scaling', ['maxdiagsize', 'mindiagsize', 'barwidth'])

        canvasXmm = (self.iface.mapCanvas().width() * mmPerInch) / self.iface.mapCanvas().logicalDpiX()

        canvasYmm = (self.iface.mapCanvas().height() * mmPerInch) / self.iface.mapCanvas().logicalDpiY()

        if self.charttype == self.CHART_TYPE_PIE:

            maxsz = min(canvasXmm, canvasYmm) / 10

            return Scaling(

                maxdiagsize=maxsz,

                mindiagsize=maxsz / 8,

                barwidth=0.0

            )

        else:

            return Scaling(

                maxdiagsize=canvasYmm / 10,

                mindiagsize=0.0,

                barwidth=canvasXmm / (25 * len(self.getFieldNames()))

            )

    def generateCharts(self, qlayer=None):

        showConsole("Generating WFS charts...")

        if qlayer is not None:

            if not self.setLayer(qlayer):

                return

        showConsole("    Determining chart scaling")

        scaling = self.getChartScaling()

        if self.charttype == self.CHART_TYPE_PIE:

            diag = QgsCore.QgsPieDiagram()

        else:

            diag = QgsCore.QgsHistogramDiagram()

        fieldnames = self.getFieldNames()

        numfields = len(fieldnames)

        showConsole("    Applying QgsDiagramSettings")

        dset = QgsCore.QgsDiagramSettings()

        dset.categoryColors = self.makeChartColors(numfields)

        dset.categoryAttributes = ['"{}"'.format(fn) for fn in fieldnames]

        dset.categoryLabels = fieldnames

        dset.font = self.iface.defaultStyleSheetFont()

        dset.transparency = 0

        dset.size = QSizeF(scaling.maxdiagsize, scaling.maxdiagsize)

        dset.sizeType = 0

        dset.labelPlacementMethod = dset.XHeight

        dset.scaleByArea = True

        dset.scaleBasedVisibility = False

        # dset.minimumSize = scaling.mindiagsize

        dset.penWidth = 0.0

        dset.backgroundColor = QColor('#ffffff')

        dset.penColor = QColor(0, 0, 0, 0)

        dset.barWidth = scaling.barwidth

        dset.lineSizeUnit = QgsCore.QgsUnitTypes.RenderUnit.RenderPixels

        # By request: create & send diagrams to QGIS, but don't display them by default

        dset.enabled = False

        showConsole("    Creating QgsLinearlyInterpolatedDiagramRenderer")

        rend = QgsCore.QgsLinearlyInterpolatedDiagramRenderer()

        rend.setUpperValue(self.getMaxAttribValue())

        rend.setUpperSize(QSizeF(scaling.maxdiagsize, scaling.maxdiagsize))

        rend.setLowerValue(0)

        rend.setLowerSize(QSizeF(0, 0))

        rend.setDiagram(diag)

        rend.setDiagramSettings(dset)

        rend.setClassificationAttributeExpression('max("' + ('", "'.join(fieldnames)) + '")')

        rend.setClassificationAttributeIsExpression(True)

        showConsole("    Applying QgsDiagramLayerSettings")

        dls = QgsCore.QgsDiagramLayerSettings()

        dls.dist = 0.0

        dls.priority = 0

        dls.xPosColumn = -1

        dls.yPosColumn = -1

        dls.placement = QgsCore.QgsDiagramLayerSettings.OverPoint

        self.diagramRenderer = rend

        self.dls = dls

    def setLayer(self, qlayer, overwrite=False):

        if qlayer.title() not in [zlyr + '_data' for zlyr in MapitConfig.MAP_LAYERS_ZONES]:

            return False

        if overwrite or (not hasattr(self, 'qlayer')):

            self.qlayer = qlayer

            return True

    def getLayer(self):

        return self.qlayer

    def apply(self):

        showConsole("Rendering layer style to QGIS window")

        if hasattr(self, 'qlayer'):

            if hasattr(self, 'styleRenderer'):

                self.qlayer.setRenderer(self.styleRenderer)

            if hasattr(self, 'diagramRenderer'):

                self.qlayer.setDiagramRenderer(self.diagramRenderer)

                self.qlayer.setDiagramLayerSettings(self.dls)

                self.qlayer.triggerRepaint()
