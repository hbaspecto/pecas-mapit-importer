# coding=utf-8

__author__ = 'seanni@trichotomy.ca'
__date__ = '2017-10-30'
__copyright__ = 'Copyright 2017, Sean Nichols, HBA Specto'

import unittest

from PyQt4.QtGui import QIcon



class MapitImportPluginDialogTest(unittest.TestCase):
    """Test rerources work."""

    def setUp(self):
        """Runs before each test."""
        pass

    def tearDown(self):
        """Runs after each test."""
        pass

    def test_icon_png(self):
        """Test we can click OK."""
        path = ':/plugins/MapitImportPlugin/icon.png'
        icon = QIcon(path)
        self.assertFalse(icon.isNull())

if __name__ == "__main__":
    suite = unittest.makeSuite(MapitImportPluginResourcesTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)
