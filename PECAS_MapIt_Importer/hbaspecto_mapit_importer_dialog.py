# -*- coding: utf-8 -*-

from os import \
    path as \
    ospath

from PyQt5 import (
    QtCore,
    uic,
)
from PyQt5.QtCore import (
    QObject,
    QSettings,
    QSize,
)
from PyQt5.QtCore import \
    Qt as \
    QtConstants
from PyQt5.QtCore import (
    pyqtSignal,
)
from PyQt5.QtWidgets import (
    QDialog,
    QDialogButtonBox,
    QMessageBox,
)

from .mapit_capabilities_parsers import (
    MapitWFSCapabilitiesParser,
    MapitWMSCapabilitiesParser,
)
from .mapit_config import (
    MapitConfig,
    showConsole,
)

# from pprint import pprint

# from PyQt5.QtGui import QDialogButtonBox, QMessageBox #, qApp

(FORM_CLASS, _) = uic.loadUiType(ospath.join(ospath.dirname(__file__), 'hbaspecto_mapit_importer_dialog_base.ui'))


GA_NUMBER = 0

GA_TEXT = 1

GA_GROUP = 2


class MapitImportPluginDialog(QDialog, FORM_CLASS):

    MIN_WIDTH = 850

    MIN_HEIGHT = 350

    OPTION_NVT_COMPARE = 'comp'

    OPTION_NVT_ENTIRE = 'entire'

    OPTION_NVT_INDIVIDUAL = 'individ'

    OPTION_EVT_VIEWSPEC = 'vspec'

    OPTION_EVT_URLENTRY = 'url'

    OPTION_TAB_NEWVIEW = 'new'

    OPTION_TAB_EXISTINGVIEW = 'existing'

    OPTION_WFS_ALLFEATURES = 'allfeatures'

    OPTION_WFS_ONLYZONE = 'onlyzonefeatures'

    trigger = pyqtSignal()

    def __init__(self, parent=None):
        """Constructor."""

        super(MapitImportPluginDialog, self).__init__(parent)

        # Set up the user interface from Designer.

        # After setupUI you can access any designer object by doing

        # self.<objectname>, and you can use autoconnect slots - see

        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html

        # #widgets-and-dialogs-with-auto-connect

        self.settings = QSettings(MapitConfig.APP_ORG, MapitConfig.APP_NAME)

        self.setupUi(self)

    def setupUi(self, window):

        super(MapitImportPluginDialog, self).setupUi(window)

        minwidth = self.MIN_WIDTH

        minheight = self.MIN_HEIGHT

        minwidth *= max(1, self.logicalDpiX() / MapitConfig.UI_NATIVE_DPI)

        minheight *= max(1, self.logicalDpiY() / MapitConfig.UI_NATIVE_DPI)

        self.setMinimumSize(QSize(int(minwidth), int(minheight)))

        #self.trigger.connect(self.ledProjectURL, QtCore.SIGNAL('textChanged(QString)'), self.onLedProjectURL_textChanged)

        self.ledProjectURL.textChanged.connect(self.onLedProjectURL_textChanged)

        self.cboViewSpec.currentIndexChanged.connect(self.onCboViewSpec_currentIndexChanged)

        self.ledViewURL.textChanged.connect(self.onLedViewURL_textChanged)

        self.btnCheckURL.clicked.connect(self.onBtnCheckURL_clicked)

        self.cboScenario.currentIndexChanged.connect(self.onCboScenario_currentIndexChanged)

        self.cboScenario2.currentIndexChanged.connect(self.onCboScenario2_currentIndexChanged)

        self.cboYear.currentIndexChanged.connect(self.onCboYear_currentIndexChanged)

        self.cboYear2.currentIndexChanged.connect(self.onCboYear2_currentIndexChanged)

        self.lsbGroupsAttribs.itemSelectionChanged.connect(self.onLsbGroupsAttribs_itemSelectionChanged)

        self.tbwSpecification.currentChanged.connect(self.onTbwSpecification_currentChanged)

        self.radNewVSCompare.toggled.connect(self.onRadNewVS_toggled)

        self.radNewVSEntire.toggled.connect(self.onRadNewVS_toggled)

        self.radNewVSIndividual.toggled.connect(self.onRadNewVS_toggled)

        self.radExistingVSList.toggled.connect(self.onRadExistingVS_toggled)

        self.radExistingVSURLEntry.toggled.connect(self.onRadExistingVS_toggled)

        self.ckbWMS.stateChanged.connect(self.onCkbWMS_stateChanged)

        self.ckbWFS.stateChanged.connect(self.onCkbWFS_stateChanged)

        self.ckbWMSContext.stateChanged.connect(self.onCkbWMSContext_stateChanged)

        self.ckbWMSZoneBoundaries.stateChanged.connect(self.onCkbWMSZoneBoundaries_stateChanged)

        self.radWFSAllClasses.toggled.connect(self.onRadWFSFeatures_toggled)

        self.radWFSOnlyLastClass.toggled.connect(self.onRadWFSFeatures_toggled)

        # This is about the only widget we explicitly set in this method, because various things will depend on this

        # value, before we actually ever show the dialog. Note it should be set
        # *after* we connect the QtCore.Signal, above.

        if len(self.ledProjectURL.text().strip()) == 0:

            self.ledProjectURL.setText(self.getQSetting('projecturl', MapitConfig.MAPIT_URL_HOST))

    def resetUI(self, schema, lookup, projecturl=None, plugin=None):

        self.resetting = True

        self.schema = schema

        self.lookup = lookup

        self.plugin = plugin

        self.fcnames = []

        if projecturl is None:

            projecturl = self.getQSetting('projecturl', None)

        if projecturl is not None:

            self.setProjectUrl(projecturl)

        self.viewspec = ''

        self.mapfile = None

        # this needs to come first, before setting some of the later UI stuff

        self.setScenarioList(self.lookup.getScenarioList())

        self.setScenario2List(self.lookup.getScenarioList())

        scencomp = (schema.mode == MapitConfig.SCM_MODE_COMPARESCENARIO)

        timeseries = (schema.mode == MapitConfig.SCM_MODE_TIMESERIES)

        altscentext = "Alt " if (scencomp or timeseries) else ""

        altyeartext = "Alt " if scencomp else "Start " if timeseries else ""

        baseyeartext = "Base " if scencomp else "End " if timeseries else ""

        self.hltScenarioComp.setVisible(scencomp or timeseries)

        self.lblScenario.setText("{}Scenario:".format(altscentext))

        self.lblYear.setText("{}Year:".format(altyeartext))

        self.lblYear2.setText("{}Year:".format(baseyeartext))

        if len(self.views) > 0:

            self.ledLayerName.setText(self.views[0])

        else:

            self.ledLayerName.clear()

        # viewtype = self.getQSetting('viewtype', self.OPTION_TAB_NEWVIEW)

        newviewtype = self.getQSetting('newviewtype', self.OPTION_NVT_COMPARE)

        if newviewtype == self.OPTION_NVT_COMPARE:

            self.radNewVSCompare.setChecked(True)

        elif newviewtype == self.OPTION_NVT_ENTIRE:

            self.radNewVSEntire.setChecked(True)

        else:

            self.radNewVSIndividual.setChecked(True)

        self.setNewViewType(newviewtype, True)

        existingviewtype = self.getQSetting('existingviewtype', self.OPTION_EVT_VIEWSPEC)

        if existingviewtype == self.OPTION_EVT_VIEWSPEC:

            self.radExistingVSList.setChecked(True)

        else:

            self.radExistingVSURLEntry.setChecked(True)

        self.setExistingViewType(existingviewtype, True)

        self.ledViewURL.clear()

        self.viewurl_ok = False

        importwms = bool(self.getQSetting('importwms', True))

        importwfs = bool(self.getQSetting('importwfs', False))

        st = ([MapitConfig.OPTION_ST_WMS] if importwms else []) + ([MapitConfig.OPTION_ST_WFS] if importwfs else [])

        self.setServiceTypes(st, True)

        self.ckbWMS.setChecked(importwms)

        self.ckbWFS.setChecked(importwfs)

        self.ckbWMSContext.setChecked(bool(self.getQSetting('showwmscontext', False)))

        self.ckbWMSZoneBoundaries.setChecked(bool(self.getQSetting('showwmszones', False)))

        self.featureclass = self.getQSetting('wfsfeatures', self.OPTION_WFS_ONLYZONE)

        if self.featureclass == self.OPTION_WFS_ONLYZONE:

            self.radWFSOnlyLastClass.setChecked(True)

        else:

            self.radWFSAllClasses.setChecked(True)

        attrcaption = "Select &Individual {}:".format(MapitConfig.getReadableATypes(schema.atype)[1].title())

        self.radNewVSIndividual.setText(attrcaption)

        self.resetting = False

    def getQSetting(self, key, default=None):

        return self.settings.value(('HBA_Mapit_' + key), default)

    def setQSetting(self, key, value):

        self.settings.setValue(('HBA_Mapit_' + key), value)

    def show(self, bringtofront=False):

        self.setWindowTitle("PECAS [{}]".format(self.schema.id))

        super(MapitImportPluginDialog, self).show()

        if bringtofront:

            self.bringToFront()

    def bringToFront(self):

        winstate = (self.windowState() & ~QtConstants.WindowMinimized)

        winstate |= QtConstants.WindowActive

        self.setWindowState(winstate)

        self.activateWindow()

    def setWidgetList(self, widget, itemlist, selectitem=None):

        widget.clear()

        if len(itemlist) > 0:

            widget.addItems([str(item) for item in itemlist])

            if selectitem is not None:

                try:

                    idx = itemlist.index(selectitem)

                except ValueError:

                    idx = 0

                widget.setCurrentIndex(idx)

    def setViewList(self, views):

        self.views = views

        self.setWidgetList(self.cboViewSpec, views, None)

    def setScenarioList(self, scenarios):

        self.scenarios = list(scenarios)

        self.setWidgetList(self.cboScenario, scenarios, self.getQSetting('scenario', scenarios[0]))

    def setScenario2List(self, scenarios):

        if hasattr(self, 'schema') and self.schema.mode == MapitConfig.SCM_MODE_TIMESERIES:

            scenarios.insert(0, '(none)')

        self.scenarios2 = list(scenarios)

        self.setWidgetList(self.cboScenario2, scenarios, self.getQSetting('scenario2', scenarios[0]))

    def setYearList(self, years):

        self.years = years

        savedyear = self.getQSetting('year', years[0])

        if savedyear not in years:

            savedyear = years[0]

        self.setWidgetList(self.cboYear, years, savedyear)

    def setYearList2(self, years):

        self.years2 = years

        savedyear2 = self.getQSetting('year2', years[0])

        if savedyear2 not in years:

            savedyear2 = years[0]

        self.setWidgetList(self.cboYear2, years, savedyear2)

    def setGroupsAttrsList(self, groupsattrs):

        self.groupsattrs = groupsattrs

        self.setWidgetList(self.lsbGroupsAttribs, [ga[GA_TEXT] for ga in groupsattrs], None)

        self.setLayerName()

    def regenViewList(self):

        scenario = self.getSelectedScenario()

        year = self.getSelectedYear()

        views = self.lookup.getViewList(scenario, year, self.schema.atype)

        self.setViewList(views)

    def regenYearList(self):

        scenario = self.getSelectedScenario()

        years = self.lookup.getYearList(scenario)

        self.setYearList(years)

        self.setLayerName()

    def regenYearList2(self):

        scenario = self.getSelectedScenario2()

        if scenario == '(none)':

            scenario = self.getSelectedScenario()

        years = self.lookup.getYearList(scenario)

        self.setYearList2(years)

    def regenGroupsAttrsList(self):

        if self.getNewViewType() == self.OPTION_NVT_INDIVIDUAL:

            self.setGroupsAttrsList(self.lookup.getAllAttribList(self.schema.atype, self.schema.filter))

        else:

            grouplist = self.lookup.getAttribGroupList(self.schema.atype, self.schema.filter)

            # A Quick hack because Importers & Exporters are external and so not yet supported

            # TODO: once we've added LUZ groups to Mapit, then remove this filter

            self.setGroupsAttrsList([g for g in grouplist if g not in ("Importers", "Exporters")])

    def getSelectedScenario(self):

        return self.scenarios[self.cboScenario.currentIndex()]

    def getSelectedScenario2(self):

        return self.scenarios2[self.cboScenario2.currentIndex()]

    def getSelectedYear(self):

        return self.years[self.cboYear.currentIndex()]

    def getSelectedYear2(self):

        return self.years2[self.cboYear2.currentIndex()]

    def getSelectedGroupsAttrs(self):

        return [itm.text() for itm in self.lsbGroupsAttribs.selectedItems()]

    def getSelectedGroups(self):

        if self.getNewViewType() == self.OPTION_NVT_INDIVIDUAL:

            groupids = [attr[GA_GROUP] for attr in self.groupsattrs if attr[GA_TEXT] in self.getSelectedGroupsAttrs()]

            return [grp[GA_TEXT] for grp in self.lookup.getAttribGroupList(
                self.schema.atype) if grp[GA_NUMBER] in groupids]

        else:

            return self.getSelectedGroupsAttrs()

    def getGroupsAttrsForSubmit(self):

        if self.getNewViewType() == self.OPTION_NVT_ENTIRE:

            groupids = [attr[GA_NUMBER] for attr in self.groupsattrs if attr[GA_TEXT] in self.getSelectedGroupsAttrs()]

            return self.lookup.getGroupedAttribList(self.schema.atype, groupids)

        else:

            return self.getSelectedGroupsAttrs()

    def getSelectedViewSpec(self):

        try:

            return self.views[self.cboViewSpec.currentIndex()]

        except AttributeError:

            return None

    def getViewSpec(self):

        return self.viewspec

    def setFeatureClasses(self, mapurl=None):

        try:

            wfscapabil = MapitWFSCapabilitiesParser(mapurl or self.getMapUrl(), MapitConfig.DEFAULT_WFS_VERSION)

            self.fcnames = wfscapabil.getFeatureClassNames()

            lastFC = "Only {} layer.".format(wfscapabil.getTitle(self.fcnames[-1]))

            self.radWFSOnlyLastClass.setText(lastFC)

        except TypeError as e:

            showConsole("Unable to get WFS capabilities for '{}' because {}".format(mapurl, e))

    def setWLayers(self, mapurl=None):

        try:

            wmscapabil = MapitWMSCapabilitiesParser(mapurl or self.getMapUrl(), MapitConfig.DEFAULT_WMS_VERSION)

            self.wlayernames = wmscapabil.getLayerNames()

            self.wparentlayer = wmscapabil.getLayerName()

        except TypeError as e:

            showConsole("Unable to get WMS capabilities for '{}' because {}".format(mapurl, e))

    def setViewSpec(self, viewspec, mapfile=None):

        if (not hasattr(self, 'viewspec')) or (self.viewspec != viewspec):

            self.viewspec = viewspec

            if mapfile or not hasattr(self, 'mapfile'):

                self.mapfile = mapfile

            mapurl = self.getMapUrl()

            self.setFeatureClasses()

            self.setWLayers()

    def getMapUrl(self, viewspec=None):

        proj = self.getProjectUrl()

        # If we already know what the mapfile name is, that's a much better plan than

        #  trying to regenerate it from the viewspec (which is... fragile).

        if self.mapfile:

            showConsole("I can haz mapfile! {!r}".format(self.mapfile))

            return MapitConfig.getMapPath(self.mapfile, proj)

        showConsole("Nope. I can't haz mapfile. Trying the API lookup.")

        vs = viewspec or self.viewspec

        if isinstance(vs, list):

            vs = vs[0]

        mapfile = self.lookup.getMapNameFromViewSpec(vs)

        if mapfile:

            showConsole("Yay! They finded mah mapfile! {!r}".format(mapfile))

            return MapitConfig.getMapPath(mapfile, proj)

        showConsole(

            "Mapfile still lost. I hopes they findz my mapfile."

        )

        if vs.startswith(self.getSelectedScenario2() + '_'):

            scen = 2

        else:

            scen = 1

        return MapitConfig.getMapUrl(vs, proj, scen=scen)

    def getProjectUrl(self):

        return self.ledProjectURL.text().strip()

    def setProjectUrl(self, url):

        self.ledProjectURL.setText(url)

    def getLayerName(self):

        n = self.ledLayerName.text().strip()

        if len(n) == 0:

            n = self.getSelectedViewSpec()

        return n

    def setLayerName(self, name=None):

        if name is None:

            name = self.generateLayerName()

        if name is not None:

            if len(name) == 0:

                self.ledLayerName.clear()

            else:

                self.ledLayerName.setText(name)

    def setServiceTypes(self, st, forceuireset=False):

        if forceuireset or (st != getattr(self, 'servicetypes', None)):

            self.servicetypes = st

            self.fraWMSOptions.setVisible(MapitConfig.OPTION_ST_WMS in st)

            self.fraWFSOptions.setVisible(MapitConfig.OPTION_ST_WFS in st)

            self.setOkEnabled()

    def hasSelectedGroupsAttrs(self, forsubmit=False):

        if forsubmit:

            groupsattrslist = self.getGroupsAttrsForSubmit()

        else:

            groupsattrslist = self.getSelectedGroupsAttrs()

        return (len(groupsattrslist) > 0)

    def validate(self, quickvalidate=False):

        # If not quickvalidate, then will make API/DB/other lookup calls to verify

        #  not just that options have been selected, but that those selections can be

        #  successfully sent in. May be a slow process; probably don't want to do it

        #  in every widget's event handler.

        msg = []

        if self.getViewTab() == self.OPTION_TAB_NEWVIEW:

            if self.schema.mode == MapitConfig.SCM_MODE_COMPARESCENARIO:

                if self.getSelectedScenario() == self.getSelectedScenario2():

                    msg.append("Comparing scenario against itself. Please select different scenarios.")

            atype = MapitConfig.getReadableATypes(self.schema.atype)

            if quickvalidate:

                if not self.hasSelectedGroupsAttrs():

                    msg.append("No group or {} selected. Please select at least one.".format(atype[0]))

            else:

                if not self.hasSelectedGroupsAttrs(forsubmit=True):

                    if self.hasSelectedGroupsAttrs():

                        msg.append("Selected group contains no {}.".format(atype[1]))

                    else:

                        msg.append("No group or {} selected. Please select at least one.".format(atype[0]))

        else:

            if self.getExistingViewType() == self.OPTION_EVT_VIEWSPEC:

                if (not hasattr(self, 'viewspec')) or len(self.getViewSpec()) == 0:

                    msg.append("No existing view selected. Please select one.")

            else:

                if len(self.ledViewURL.text().strip()) == 0:

                    msg.append("No URL for existing view entered. Please enter a URL.")

                else:

                    if not self.viewurl_ok:

                        msg.append("No map verified at specified URL. Please check URL.")

        if (not hasattr(self, 'servicetypes')) or len(self.servicetypes) == 0:

            msg.append("Neither WMS nor WFS import selected. Please select at least one.")

        return None if len(msg) == 0 else msg

    def setOkEnabled(self, enabled=None, reason=None):

        if not isinstance(enabled, bool):

            val = self.validate(quickvalidate=True)

            enabled = (val is None)

            reason = None if val is None else "\n".join([" - {}".format(msg) for msg in val])

        self.bbxDialog.button(QDialogButtonBox.Ok).setEnabled(enabled)

        if not enabled and reason is not None:

            tooltip = "Cannot import because:\n" + reason

        else:

            tooltip = ""

        self.bbxDialog.button(QDialogButtonBox.Ok).setToolTip(tooltip)

    def getServiceTypes(self):

        st = []

        if self.ckbWMS.isChecked():

            st.append(MapitConfig.OPTION_ST_WMS)

        if self.ckbWFS.isChecked():

            st.append(MapitConfig.OPTION_ST_WFS)

        return st

    def getViewTab(self):

        return self.OPTION_TAB_NEWVIEW if self.tbwSpecification.currentIndex() == 0 else self.OPTION_TAB_EXISTINGVIEW

    def setNewViewType(self, nvt, forceuireset=False):

        if forceuireset or (not hasattr(self, 'newviewtype')) or (nvt != self.newviewtype):

            self.newviewtype = nvt

            self.regenGroupsAttrsList()

            self.setOkEnabled()

    def getNewViewType(self):

        return self.OPTION_NVT_COMPARE if self.radNewVSCompare.isChecked(
        ) else self.OPTION_NVT_ENTIRE if self.radNewVSEntire.isChecked() else self.OPTION_NVT_INDIVIDUAL

    def setExistingViewType(self, evt, forceuireset=False):

        if forceuireset or (not hasattr(self, 'existingviewtype')) or (evt != self.existingviewtype):

            self.existingviewtype = evt

            evt_isList = (evt == self.OPTION_EVT_VIEWSPEC)

            self.cboViewSpec.setEnabled(evt_isList)

            self.ledViewURL.setEnabled(not evt_isList)

            self.setLayerName()

            self.setOkEnabled()

    def getExistingViewType(self):

        return self.OPTION_EVT_VIEWSPEC if self.radExistingVSList.isChecked() else self.OPTION_EVT_URLENTRY

    def getNumViews(self):

        nv = MapitConfig.getNumViews(self.schema)

        if self.schema.mode == MapitConfig.SCM_MODE_TIMESERIES:

            scen1 = self.getSelectedScenario()

            scen2 = self.getSelectedScenario2()

            if scen2 in (scen1, '(none)'):

                nv = 1

        return nv

    def getFeatureClassNames(self):

        if self.featureclass == self.OPTION_WFS_ALLFEATURES:

            # The LUZ/TAZ layer is last in GetCapabilities, but we want to promote it by one (so towns --

            #  which are second-to-last -- don't show up over top of it and block the zones)

            return self.fcnames[:-2] + self.fcnames[:-3:-1]

        else:

            return self.fcnames[-1:]

    def getWMSLayers(self):

        # The order here is important: topmost layer first; bottom layer last.

        #  (Note a QGIS bug will *only* include the legend for the bottom-most layer.

        #   Still looking for a solution for this.)

        if self.ckbWMSZoneBoundaries.isChecked() and self.ckbWMSContext.isChecked():

            # If we're bringing in everything, better just to supply the parent ("overall") layer.

            #  Unfortunately, for schemas with multiple views, that needs to be different for each

            #  view - so will leave it blank for now. That will trigger a GetCapabilities

            #  layer name check on import (which ensures that it will be after the mapfiles have

            #  actually been generated, which may or may not be true right now), so that the

            #  correct layer names are inserted at that time.

            layers = []

        else:

            chart_opts = MapitConfig.MAP_LAYERS_CHART

            zones_opts = MapitConfig.MAP_LAYERS_ZONES

            wlayernames = getattr(self, 'wlayernames', [])

            chartlayer = [lname for lname in wlayernames if lname in chart_opts]

            zoneslayer = [lname for lname in wlayernames if lname in zones_opts]

            contextlayers = [lname for lname in wlayernames if lname not in (chart_opts + zones_opts)]

            layers = chartlayer

            if self.ckbWMSZoneBoundaries.isChecked():

                layers += zoneslayer

            if self.ckbWMSContext.isChecked():

                layers += contextlayers

        return layers

    def getSchemaFileID(self):

        return self.lookup.getModelFileID(self.schema.file)

    def generateLayerName(self):

        try:

            if self.getViewTab() == self.OPTION_TAB_NEWVIEW:

                parts = [self.schema.menu + ":", self.getSelectedScenario(), str(self.getSelectedYear())]

                if self.schema.mode == MapitConfig.SCM_MODE_COMPARESCENARIO:

                    parts.extend(['vs', str(self.getSelectedYear2())])

                elif self.schema.mode == MapitConfig.SCM_MODE_TIMESERIES:

                    parts.extend(['-', str(self.getSelectedYear2()),

                                  'vs', self.getSelectedScenario2()])

                parts.append("+".join(self.getSelectedGroups()))

                return " ".join(parts)

            else:

                if self.getExistingViewType() == self.OPTION_EVT_VIEWSPEC:

                    return self.getSelectedViewSpec()

                else:

                    # Note that None causes setLayerName() to do nothing, whereas

                    #  '' causes it to clear out any existing name...

                    if hasattr(self, 'viewurl_ok') and self.viewurl_ok:

                        return None

                    else:

                        return ''

        except IndexError:

            return None


###############################################################################################################

#   Event / QtCore.Signal handlers

###############################################################################################################

    def onLedProjectURL_textChanged(self, text):

        self.setQSetting('projecturl', text)

        if hasattr(self, 'lookup') and self.lookup is not None:

            self.lookup.setProjectUrl(text)

    def onCboViewSpec_currentIndexChanged(self, index):

        if index > -1:

            viewspec = self.getSelectedViewSpec()

            self.setLayerName()

            self.setViewSpec(viewspec)

            self.setOkEnabled()

    def onLedViewURL_textChanged(self, text):

        self.viewurl_ok = False

        self.setLayerName()

        self.setOkEnabled()

    def onBtnCheckURL_clicked(self, checked):

        class StatusErr(Exception):

            pass

        try:

            url = self.ledViewURL.text()

            if url:

                (viewspec, mapfile) = self.lookup.getMapViewFromURL(url)

                if mapfile and (not mapfile.endswith('.map')):

                    mapfile += '.map'

            else:

                raise StatusErr("No URL for existing view entered. Please enter a URL.")

            if viewspec:

                self.viewurl_ok = True

                self.setLayerName(viewspec)

                self.setViewSpec(viewspec, mapfile=mapfile)

            else:

                raise StatusErr("No map found at specified URL. Please verify and enter a new URL.")

        except StatusErr as e:

            msg = e.args[0]

            if self.plugin:

                self.plugin.msgBox(msg, QMessageBox.Warning)

            else:

                showConsole(msg)

        self.setOkEnabled()

    def onCboScenario_currentIndexChanged(self, index):

        if index > -1:

            self.setQSetting('scenario', self.getSelectedScenario())

            self.regenYearList()

    def onCboYear_currentIndexChanged(self, index):

        if index > -1:

            if not self.resetting:

                self.setQSetting('year', self.getSelectedYear())

            self.regenViewList()

    def onCboScenario2_currentIndexChanged(self, index):

        if index > -1:

            scen = self.getSelectedScenario2()

            if scen != '(none)':

                self.setQSetting('scenario2', scen)

            self.regenYearList2()

    def onCboYear2_currentIndexChanged(self, index):

        if index > -1:

            if not self.resetting:

                self.setQSetting('year2', self.getSelectedYear2())

    def onTbwSpecification_currentChanged(self, index):

        if index == 0:

            self.lsbGroupsAttribs.clearSelection()

        else:

            self.cboViewSpec.setCurrentIndex(0)

        self.setQSetting('viewtype', self.getViewTab())

        self.setLayerName()

        self.setOkEnabled()

    def onCkbWMS_stateChanged(self, state):

        self.setServiceTypes(self.getServiceTypes())

        self.setQSetting('importwms', int(MapitConfig.OPTION_ST_WMS in self.servicetypes))

    def onCkbWFS_stateChanged(self, state):

        self.setServiceTypes(self.getServiceTypes())

        self.setQSetting('importwfs', int(MapitConfig.OPTION_ST_WFS in self.servicetypes))

    def onRadNewVS_toggled(self, checked):

        self.setNewViewType(self.getNewViewType())

        self.setQSetting('newviewtype', self.newviewtype)

        self.setLayerName()

    def onRadExistingVS_toggled(self, checked):

        self.setExistingViewType(self.getExistingViewType())

        self.setQSetting('existingviewtype', self.existingviewtype)

        self.ledViewURL.clear()

        self.setLayerName()

    def onLsbGroupsAttribs_itemSelectionChanged(self):

        self.setLayerName()

        self.setOkEnabled()

    def onCkbWMSContext_stateChanged(self, state):

        self.setQSetting('showwmscontext', int(self.ckbWMSContext.isChecked()))

    def onCkbWMSZoneBoundaries_stateChanged(self, state):

        self.setQSetting('showwmszones', int(self.ckbWMSZoneBoundaries.isChecked()))

    def onRadWFSFeatures_toggled(self, checked):

        self.featureclass = self.OPTION_WFS_ALLFEATURES if self.radWFSAllClasses.isChecked() else self.OPTION_WFS_ONLYZONE

        self.setQSetting('wfsfeatures', self.featureclass)

    def resizeEvent(self, *args):

        try:

            QDialog.resizeEvent(self, *args)

        except AttributeError:

            # This will sometimes throw a "NoneType has no attribute resizeEvent"

            #  AttributeError if the QGIS window is minimized with the plugin still

            #  running and (self) still instantiated. Nothing to worry about here,

            #  just don't bother with the rest of the event handler.

            return

        self.setQSetting('windowsize', self.size())

        dimensions = (

            self.width(), self.height(),

            self.fltMaster.width(), self.fltMaster.height(),

            self.bbxDialog.width(), self.bbxDialog.height()

        )

        showConsole("dlg=({}, {}) :: flt=({}, {})/bbx=({}, {})".format(*dimensions))
