# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mapit_plugin_progressbar_base.ui'
#
# Created: Fri Nov 01 16:03:25 2019
#      by: PyQt4 UI code generator 4.10.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import (
    QtCore,
    QtGui,
)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MapitPluginProgressbarBase(object):
    def setupUi(self, MapitPluginProgressbarBase):
        MapitPluginProgressbarBase.setObjectName(_fromUtf8("MapitPluginProgressbarBase"))
        MapitPluginProgressbarBase.resize(542, 106)
        MapitPluginProgressbarBase.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.vltProgress = QtGui.QVBoxLayout(MapitPluginProgressbarBase)
        self.vltProgress.setObjectName(_fromUtf8("vltProgress"))
        self.prgProgress = QtGui.QProgressBar(MapitPluginProgressbarBase)
        self.prgProgress.setProperty("value", 0)
        self.prgProgress.setObjectName(_fromUtf8("prgProgress"))
        self.vltProgress.addWidget(self.prgProgress)
        self.lblStatus = QtGui.QLabel(MapitPluginProgressbarBase)
        self.lblStatus.setObjectName(_fromUtf8("lblStatus"))
        self.vltProgress.addWidget(self.lblStatus)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.vltProgress.addItem(spacerItem)
        self.hltCancel = QtGui.QHBoxLayout()
        self.hltCancel.setObjectName(_fromUtf8("hltCancel"))
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.hltCancel.addItem(spacerItem1)
        self.btnCancel = QtGui.QPushButton(MapitPluginProgressbarBase)
        self.btnCancel.setObjectName(_fromUtf8("btnCancel"))
        self.hltCancel.addWidget(self.btnCancel)
        self.vltProgress.addLayout(self.hltCancel)

        self.retranslateUi(MapitPluginProgressbarBase)
        QtCore.QMetaObject.connectSlotsByName(MapitPluginProgressbarBase)

    def retranslateUi(self, MapitPluginProgressbarBase):
        MapitPluginProgressbarBase.setWindowTitle(_translate("MapitPluginProgressbarBase", "Progress", None))
        self.lblStatus.setText(_translate("MapitPluginProgressbarBase", "Doing the thing...", None))
        self.btnCancel.setText(_translate("MapitPluginProgressbarBase", "&Cancel", None))
