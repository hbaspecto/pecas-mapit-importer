pecas-visualizations/END_USER_INSTALL
==================================================

There are two ways to install the plugin: 

- from a zip file
- from the plugin repo.


Installing from a ZIP file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Download the ZIP file from
  
  http://files.hbaspecto.com/qgis-plugins/

- Import them into QGIS. (???)
  (Need to expand this)



Installing from the plugin repo
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. todo: put this in a seperate doc

To install the plugin into QGIS, the plugin repo needs to be
added, then the plugin installed.

First, add the HBA QGIS plugin repo.
(This only needs to be done once for each QGIS app.)

- Start QGIS.

- From the menu bar, pick ``'Plugins'``

- In the plugin window, open ``'Settings'`` on the side.

- Add the HBA plugin repo with ``'Add...'``.

- Enter ``http://qgis-plugins.hbaspecto.com/plugins/plugins.xml``
  You will also need to enter a user and password for the HBA plugin site:

::

    User:     qgis-user 
    Password: ToffeeBlizzard

- Enable ``'Show experimental'``.
  (Otherwise the plugin wont show up.)


Once the HBA plugin repo has been added, find and install the plugin:

- Pick the ``'All'`` tab.

- Enter ``'PECAS MapIt Importer'`` in the search bar.

- Select the plugin.

- Click ``'Install Plugin'``
