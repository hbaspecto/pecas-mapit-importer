Admin Setup
--------------------------------------------------

For deployment

- Deploy the `qgis-plugins-hbaspecto-com-play.yml <https://bitbucket.org/hbaspecto/systems-hbaspecto-com/src/master/ansible/plays/qgis-plugins-hbaspecto-com-play.yml>`_ site.

- Login to ``fw-1`` and create a password:

::

    sudo htpasswd -b -c /etc/nginx/auth/qgis-plugins.htpasswd qgis-user ToffeeBlizzard

- Test it by visiting the site in the browser:
  `http://qgis-plugins.hbaspecto.com/ <http://qgis-plugins.hbaspecto.com/>`_

- Test it with QGIS plugin manager.


We also want to create a user for the plugin to talk to the mapit server.

::

    sudo htpasswd -b /etc/nginx/auth/mrsgui.htpasswd plugin-user-1 c36c8d1f33edc0430784d749bcc96e96

- Use this username and password for the plugin.
  (Put it into the code.)

- Test it by visiting: `https://mapit-aset-1-s.hbaspecto.com/mapit <https://mapit-aset-1-s.hbaspecto.com/mapit>`_
