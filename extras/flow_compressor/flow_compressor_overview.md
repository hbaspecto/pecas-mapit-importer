Title: Flow Compression for Visualization
Author: John E. Abraham
Date: June 2018

# [%Title]

A "Desire Line" map shows all of the zone-to-zone economic relationships or trips in 
a model with the thickness of the line proportional to the amount of flow, and
sometimes with an arrowhead indicating direction. With 523 zones
this is 273,000 lines on a map, and with the infinite
tail in the probability curve of the logit function, all of the lines would be drawn. Such
a map is not readable.  

![Desire lines example showing illegibility](full_desire_line_example.png). 

Many models have much more zones than 523, making the problem worse.

A number of possible solutions are available including:

1. making small lines partially or fully transparent

![Example color ramp](transparent_colour_ramp.png)
![Line thickness function](line_thickness_style.png)

2. aggregating zones together

3. assigning the flows to a minimal subset network

4. clustering arrows together using k-means

## Clustering

The clustering algorithm uses k-means clustering in 4 dimensions.  The dimensions (i.e.
the attributes to be considered in the "distance measure" in k-means are:

* x-coordinate of the origin
* y-coordinate of the origin
* x-coordinate of the destination
* y-coordinate of the destination.

We use a weighted k-means, and weight is the quantity of flow.  The coordinates should be
in a projection based on meters or feet, so that equal differences in the x and y 
coordinates are equally distant.  The centroid calculation for the cluster is a weighted average in four
dimensions of the coordinate:

	@Override
	public FourCoordinatesI centroidOf(Collection<FourCoordinatesI> p) {

		FourCoordinates fc = new FourCoordinates();
		fc.weight = 0;

		for (FourCoordinatesI obj : p) {
			double newWeight = fc.weight+obj.getWeight();
			fc.dest_x = (fc.dest_x*fc.weight + obj.getDestX()*obj.getWeight())/newWeight;
			fc.dest_y = (fc.dest_y*fc.weight + obj.getDestY()*obj.getWeight())/newWeight;
			fc.orig_x = (fc.orig_x*fc.weight + obj.getOrigX()*obj.getWeight())/newWeight;
			fc.orig_y = (fc.orig_y*fc.weight + obj.getOrigY()*obj.getWeight())/newWeight;
			fc.weight = newWeight;
		}
		return fc;
	}

The distance between two clusters is the cartesian distance (sqrt of sum of squares differences)
between their 4-dimensional centroids.

	@Override
	public double distanceFrom(FourCoordinatesI o) {
		return Math.sqrt(
				(getDestX()-o.getDestX())*(getDestX()-o.getDestX()) +
				(getDestY()-o.getDestY())*(getDestY()-o.getDestY()) +					
				(getOrigX()-o.getOrigX())*(getOrigX()-o.getOrigX()) +		
				(getOrigY()-o.getOrigY())*(getOrigY()-o.getOrigY()));	

	}


## PECAS implementation

The PECAS flow clusterer can read in the .zipmatrix files for economic flows produced 
by PECAS, together with a ZoneCoordinates.csv file.  It is a Java program which

* reads the .zipmatrix file in which has the origin and destination zone and the flow volume.

* reads the zonescoordinates.csv file which, has the centroid coordinates of all the zones,
and creates flow objects by attaching the coordinates to the flow volumes.

* calls the [k-means clustering algorithm](http://commons.apache.org/proper/commons-math/apidocs/org/apache/commons/math4/ml/clustering/KMeansPlusPlusClusterer.html)
from apache.commons-math 
with a target number of iterations and a number
of desired clusters

* opens a .gml file and writes both
	* the full set of flows to the .gml file
	* the compressed set of flows to the .gml file

Each of these have attributes for both the percent of the total flow and the absolute flow. 
	
Once the Java program has produced the .gml file, the file is imported into a GIS program
such as QGIS.  This takes sometime as the file has much text in it. Then, QGIS styles
are applied to:

1) Make the thickness of the line proportional to the aggregate volume of the cluster as a percentage of the total, and
2) Add arrowheads to the line proportional in size to the aggregate volume of the cluster as a percentage of the total.

See the QGIS style files `Flow Quantity Percent.qml` which we use for clustered flows, 
and `FullFlowMatrixDesireLines.qml` which we use for the full matrix.

Generally we use the percent flow, as this gives a reliable visualization. The absolute flow amounts are quite different between
different commmodities ("Puts"), so using absolute flow may produce very thick arrows covering the whole map
or very thin arrows that are not visible.  Adjusting the arrow thickness in QGIS is a trial-and-error
process that is time consuming because the arrow thickness is specified quite deep in the dialog boxes for style editing.

The resulting maps look like this: 

![Example of compressed flow map][ab_flows]

[ab_flows]: Compressed_flow_AB_example.png "Compressed Flow Visual from QGIS and PECAS"

## Existing problem or concerns

The problems with this implementation are:

* There is no way for the user to group different "Puts" or commodities together before 
clustering.

* PECAS currently writes .zipmatrix format for flows, which needs to be converted to .csv 
files or database tables to be imported into most other software.

* There is no way to interactively specify of vary the number of clusters.  Too few clusters
abstracts the flows too much, which can be misleading.  However too many clusters produces
maps with too much detail to read.  We use between 100 and 5000 clusters depending on the context 
zoom level.  The user should be able to adjust the number of clusters interactively.  Clustering 
use less clusters is much faster than using more clusters, interactively the user may wish to 
explore a small number of clusters quite quickly (e.g. 100), and then dial up the number of clusters
for presentation graphics.

* The k-means algorithm is iterative.  More iterations gives a better set of clusters, but of
course takes more time.  It would be very nice to be able to see the earlier results from
the first iterations visually and interactively, and have them update in front of the user
so that the visualization can be used while it is also being improved.  We typically use 30 iterations
but sometimes use 100 iterations, and it takes over 20 minutes to produce a set of clusters, and
then the user still has to perform the subsequent steps in QGIS before they can see the 
results.  Thus, this is not currently a very interactive process.

* K-means can be parallelized and run using GPU resources and run on the user's machine.  Our current
implementation has been running on the server machine, thus tying up server resources.

* The PECAS implementation stores the full flows in the same .gml file as the partial flows,
so this file is quite large due to the inefficiencies in the .gml standard.  A more compact storage
mechanism would be better, and storing the full matrix separate from the compressed matrix would also
be nice.  In fact in a QGIS implementation, we would not have to write out the results at all, the
user could choose to save te results in any format they wanted using QGIS export functionality.

# Proposed implementation

The proposal is to have a system running on the user's computer that:

1. (Optional) Connects to the PECAS server to download the flow matrix and the zone coordinates,
and to aggregate different flow matrices together on either the server or client. (For example, 
look at all labour flows at once.)

2. (Optional) Converts the projection into the local UTM projection

3. Interactively prompts (using a slider, ideally) the user for the number of clusters

4. Starts clustering

5. After each iteration of the clustering, it produces a map of arrows similar to [the alberta flow map][ab_flows]

There are two proposed implementations:

1) Using a QGIS plugin, so that the map can be drawn in QGIS, and the software can be written in python.

2) Using a Javascript implementation, so that the map can be draw in a browser, and the software can be written in Javascript.

We would like to use the GPU resources, as k-means is a simple algorithm suitable for a large
degree of parallelism. The choice between python (QGIS) or javascript (browser) may depend
on the availability and suitability of libraries, skills, and computing resources (e.g. GPUs available).

Some libraries have been identified: 

* Nathan Epstein [Clusters](https://github.com/NathanEpstein/clusters)
library for clustering in Javascript.

* The [gpu.js](http://gpu.rocks) library for using the GPU in javascript

* The [kmcuda](https://github.com/src-d/kmcuda) library in python which uses the 
CUDA library on NVidia processes.  It claims "300K samples are grouped into 5000 clusters in 4½ minutes on NVIDIA Titan X (15 iterations)"
which is a problem of a similar scale to what we have in the Alberta PECAS Model, and would show the user
another iteration every 18 seconds.

* The [parallel implementation of k-means](https://github.com/ishanhan/parallel-implementation-of-kmeans) which uses
MPI and PyCUDA

There are [many other possibilites](https://www.google.com/search?client=safari&rls=en&q=python+parallel+k-means&ie=UTF-8&oe=UTF-8) for Python.  

However for real speed in a python program, the best option is often to call a 
[C binary library](https://www.google.com/search?client=safari&rls=en&ei=hdYrW5jJJ9qN0PEP6oy92Ag&q=parallel+k-means+c+library&oq=parallel+k-means+c+library)
that has been packaged as a python module.
This would give us the full availability of parallel processing and the performance of C.  However the ability
to report results each iteration may be more difficult to program.


### Example java main program
	public static void main(String[] args) {
		// Test method to build a compressed matrix
		String matrixFileName = args[0];
		String centroidCoordinatesFile = args[1];
		String outputFileName = args[2];
		Path p = Paths.get(outputFileName);
		String flowName = p.getFileName().toString();
		if(flowName.contains(".")) flowName = flowName.substring(0, flowName.lastIndexOf('.'));
		int numFlows = Integer.valueOf(args[3]);
		int iterations = 100;
		if (args.length > 4) {
			iterations = Integer.valueOf(args[4]);
		}
		
		FlowMatrixCompressor me = new FlowMatrixCompressor();

		Matrix b = null;
		TableDataSet coords=null;
		try {
			b = MatrixReader.readMatrix(new File(matrixFileName), null);
			TableDataFileReader myReader = new NEW_CSVFileReader();
			coords = myReader.readFile(new File(centroidCoordinatesFile));
		} catch (IOException e) {
			logger.error("Problem reading or writing data in test routine");
			e.printStackTrace();
		}
		
		coords.buildIndex(1);
		ArrayList<Collection<FourCoordinatesI>> flows = new ArrayList<Collection<FourCoordinatesI>>();
		flows.add(me.buildFlowArray(b,coords));
		int[] numFlowArray = new int[2];
		numFlowArray[0] = b.getRowCount()*b.getRowCount();
		Collection<FourCoordinatesI> results = me.compress(flows.get(0),numFlows,iterations);
		flows.add(results);
		numFlowArray[1] = numFlows;
		//if (outputFileName.endsWith("gml")) {
			int srid_epsg = 3857;
			try {
				srid_epsg = Integer.valueOf(args[5]);
			} catch (Exception e) {	
				// just use 3857
			}
			writeGMLFlows(outputFileName, flowName, flows, srid_epsg, outputFileName, numFlowArray);
		//} else {
			//writeCSVCompressedFlows(outputFileName, results[1]);
		//}

	}







