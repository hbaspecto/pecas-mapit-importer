### Installing/Running Flow Compressor Explorer extension in Windows

1. Ensure you have a Java JRE installed. _(Question for John: is there a minimum Java version prereq?)_
2. Find a useful folder and copy `StandAloneFlowCompressor.jar` & `log4j.xml` to it
3. Find a (possibly different) useful folder and copy `run_flow_compressor.bat` & `getinput.vbs` to it
4. Edit `run_flow_compressor.bat` and modify the `!!! CHANGE ME BELOW !!!` lines as appropriate, using correct location to scripts from step 2 above, and providing useful default values for `numclusters` and `numiterations`.
5. Find a scenario somewhere with `*.zipMatrix` files in it, and choose one to open.  
**This file/scenario _must_ be on a drive-letter-mapped path** (e.g.: `W:\pecas\C70\2011`) **and _not_ on a UNC path** (e.g.: `\\oyen\pecas\C70\2011`) **!**  
The extension will not work on .zipMatrix files on a UNC path.
6. Right-click on the `*.zipMatrix` file, select `Open With...` > `Choose another app`
7. Scroll to the bottom > _More Apps_ > scroll (again) to the bottom > _Look for another app on this PC_. (Make sure to check _Always use this app to open .zipMatrix files_.)
8. Navigate to `run_flow_compressor.bat` (wherever you saved it in step 3) and click "Open."
9. The first time you run it, you may get a warning/confirmation about opening `getinput.vbs` - this file is safe to open. (And it may be more convenient to uncheck the "_Always ask before opening this file_" box. If you are uncomfortable doing so but find the repeated warning cumbersome, then editing `run_flow_compressor.bat` and setting **`set userverify=No`** will skip this call, at the cost of not being able to confirm # of iterations or clusters at run-time.)
10. If you press the _Cancel_ button at either of the # of iteration/cluster confirmation boxes, the entire process will abort. Otherwise a DOS window will pop up showing that compression has started. The flow compressor will take several minutes to run. Closing this window will abort the process.
11. Once finished, the status will update with `Done!` and the appropriate gml file will appear in the scenario directory, ready to open in QGIS.

---

1. To run the flow compressor on another zipMatrix file, just double-click the file in Windows Explorer.

