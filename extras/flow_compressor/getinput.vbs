Option Explicit

Dim args, answer, retval, question

Set args = WScript.Arguments
question = args(0)
retval = args(1)

answer = InputBox( question, "Flow Compressor", retval )
On Error resume next
retval = CInt( answer )

WScript.Quit retval
