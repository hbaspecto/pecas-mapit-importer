@echo off
setlocal enabledelayedexpansion

REM this just gets the path to the "Documents" folder - assuming that the java stuff
REM  is getting saved in there somewhere; if not then you may not need this bit
set regkey="HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders"
for /f "tokens=3 delims= " %%v in ('reg query %regkey% /v "Personal"') do (
    set documents=%%v
)


REM ==================== !!! CHANGE ME BELOW !!! ====================

REM The path to the java stuff (specifically the StandAloneFlowCompressor.jar and log4j.xml
REM  files). May want to just hard-code this if it's not in your Documents folder...
set jarpath=%documents%\src\java

REM this is where the JRE executable is stored on your computer
REM  if you don't have a JRE installed, get one at https://java.com/en/
set java_exec="C:\Program Files\Java\jre1.8.0_161\bin\java.exe"

REM default parameters for FlowCompressor
set numclusters=5000
set numiterations=30
set projection=3401

REM Get the user to verify the default values? (Yes)
REM Or just use them as-is?                    (No)
set userverify=Yes

REM ==================== !!! CHANGE ME ABOVE !!! ====================


set getinput="%~dp0\getinput.vbs"
if "%userverify%" == "No" (
    REM Counter-intuitive, but don't just put this next bit inside a big if ( ... )
    REM  block. It'll mess up the variable assignment (because of how DOS parses
    REM  variable expansion inside the block). Just stick with the "goto" approach.
    goto endverify
)

%getinput% "Number of clusters:" %numclusters%
set numclusters=!errorlevel!    
if "%numclusters%" == "0" (
    echo Canceled by user.
    goto end
)

%getinput% "Number of iterations:" %numiterations%
set numiterations=%errorlevel%
if "%numiterations%" == "0" (
    echo Canceled by user.
    goto end
)

:endverify

for %%v in (%*) do (
    REM Recall: to get parts of the variable %%v, use syntax %%~[dpnx]v where
    REM  d - Drive
    REM  p - Path
    REM  n - (base) fileName
    REM  x - eXtension
    
    set zipmatrixfile="%%~dpnxv"
    set zonecoords="%%~dpv..\AllYears\Inputs\ZonesCoordinates.csv"
    set outputfile="%%~dpnv.gml"
    
    echo Running Flow Compressor on: %%~nxv
    echo               in directory: %%~dpv
    echo          with num clusters: %numclusters%
    echo         and num iterations: %numiterations%...
    
    %java_exec% ^
        -Dlog4j.configuration=file:%jarpath%\log4j.xml ^
        -jar %jarpath%\StandAloneFlowCompressor.jar ^
        !zipmatrixfile! ^
        !zonecoords! ^
        !outputfile! ^
        %numclusters% ^
        %numiterations% ^
        %projection%
)

:end
endlocal
echo Done!
pause