<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Flow Compression for Visualization</title>
	<meta name="author" content="John E. Abraham"/>
	<meta name="date" content="June 2018"/>
</head>
<body>

<h1 id="title">Flow Compression for Visualization</h1>

<p>A &#8220;Desire Line&#8221; map shows all of the zone-to-zone economic relationships or trips in
a model with the thickness of the line proportional to the amount of flow, and
sometimes with an arrowhead indicating direction. With 523 zones
this is 273,000 lines on a map, and with the infinite
tail in the probability curve of the logit function, all of the lines would be drawn. Such
a map is not readable. </p>

<p><img src="full_desire_line_example.png" alt="Desire lines example showing illegibility" />. </p>

<p>Many models have much more zones than 523, making the problem worse.</p>

<p>A number of possible solutions are available including:</p>

<ol>
<li>making small lines partially or fully transparent</li>
</ol>

<p><img src="transparent_colour_ramp.png" alt="Example color ramp" />
<img src="line_thickness_style.png" alt="Line thickness function" /></p>

<ol>
<li><p>aggregating zones together</p></li>
<li><p>assigning the flows to a minimal subset network</p></li>
<li><p>clustering arrows together using k-means</p></li>
</ol>

<h2 id="clustering">Clustering</h2>

<p>The clustering algorithm uses k-means clustering in 4 dimensions. The dimensions (i.e.
the attributes to be considered in the &#8220;distance measure&#8221; in k-means are:</p>

<ul>
<li>x-coordinate of the origin</li>
<li>y-coordinate of the origin</li>
<li>x-coordinate of the destination</li>
<li>y-coordinate of the destination.</li>
</ul>

<p>We use a weighted k-means, and weight is the quantity of flow. The coordinates should be
in a projection based on meters or feet, so that equal differences in the x and y
coordinates are equally distant. The centroid calculation for the cluster is a weighted average in four
dimensions of the coordinate:</p>

<pre><code>@Override
public FourCoordinatesI centroidOf(Collection&lt;FourCoordinatesI&gt; p) {

    FourCoordinates fc = new FourCoordinates();
    fc.weight = 0;

    for (FourCoordinatesI obj : p) {
        double newWeight = fc.weight+obj.getWeight();
        fc.dest_x = (fc.dest_x*fc.weight + obj.getDestX()*obj.getWeight())/newWeight;
        fc.dest_y = (fc.dest_y*fc.weight + obj.getDestY()*obj.getWeight())/newWeight;
        fc.orig_x = (fc.orig_x*fc.weight + obj.getOrigX()*obj.getWeight())/newWeight;
        fc.orig_y = (fc.orig_y*fc.weight + obj.getOrigY()*obj.getWeight())/newWeight;
        fc.weight = newWeight;
    }
    return fc;
}
</code></pre>

<p>The distance between two clusters is the cartesian distance (sqrt of sum of squares differences)
between their 4-dimensional centroids.</p>

<pre><code>@Override
public double distanceFrom(FourCoordinatesI o) {
    return Math.sqrt(
            (getDestX()-o.getDestX())*(getDestX()-o.getDestX()) +
            (getDestY()-o.getDestY())*(getDestY()-o.getDestY()) +                   
            (getOrigX()-o.getOrigX())*(getOrigX()-o.getOrigX()) +       
            (getOrigY()-o.getOrigY())*(getOrigY()-o.getOrigY()));   

}
</code></pre>

<h2 id="pecasimplementation">PECAS implementation</h2>

<p>The PECAS flow clusterer can read in the .zipmatrix files for economic flows produced
by PECAS, together with a ZoneCoordinates.csv file. It is a Java program which</p>

<ul>
<li><p>reads the .zipmatrix file in which has the origin and destination zone and the flow volume.</p></li>
<li><p>reads the zonescoordinates.csv file which, has the centroid coordinates of all the zones,
and creates flow objects by attaching the coordinates to the flow volumes.</p></li>
<li><p>calls the <a href="http://commons.apache.org/proper/commons-math/apidocs/org/apache/commons/math4/ml/clustering/KMeansPlusPlusClusterer.html">k-means clustering algorithm</a>
from apache.commons-math
with a target number of iterations and a number
of desired clusters</p></li>
<li>opens a .gml file and writes both

<ul>
<li>the full set of flows to the .gml file</li>
<li>the compressed set of flows to the .gml file</li>
</ul></li>
</ul>

<p>Each of these have attributes for both the percent of the total flow and the absolute flow. </p>

<p>Once the Java program has produced the .gml file, the file is imported into a GIS program
such as QGIS. This takes sometime as the file has much text in it. Then, QGIS styles
are applied to:</p>

<p>1) Make the thickness of the line proportional to the aggregate volume of the cluster as a percentage of the total, and
2) Add arrowheads to the line proportional in size to the aggregate volume of the cluster as a percentage of the total.</p>

<p>See the QGIS style files <code>Flow Quantity Percent.qml</code> which we use for clustered flows,
and <code>FullFlowMatrixDesireLines.qml</code> which we use for the full matrix.</p>

<p>Generally we use the percent flow, as this gives a reliable visualization. The absolute flow amounts are quite different between
different commmodities (&#8220;Puts&#8221;), so using absolute flow may produce very thick arrows covering the whole map
or very thin arrows that are not visible. Adjusting the arrow thickness in QGIS is a trial-and-error
process that is time consuming because the arrow thickness is specified quite deep in the dialog boxes for style editing.</p>

<p>The resulting maps look like this: </p>

<figure>
<img src="Compressed_flow_AB_example.png" alt="Example of compressed flow map" id="ab_flows" title="Compressed Flow Visual from QGIS and PECAS" />
<figcaption>Example of compressed flow map</figcaption>
</figure>

<h2 id="existingproblemorconcerns">Existing problem or concerns</h2>

<p>The problems with this implementation are:</p>

<ul>
<li><p>There is no way for the user to group different &#8220;Puts&#8221; or commodities together before
clustering.</p></li>
<li><p>PECAS currently writes .zipmatrix format for flows, which needs to be converted to .csv
files or database tables to be imported into most other software.</p></li>
<li><p>There is no way to interactively specify of vary the number of clusters. Too few clusters
abstracts the flows too much, which can be misleading. However too many clusters produces
maps with too much detail to read. We use between 100 and 5000 clusters depending on the context
zoom level. The user should be able to adjust the number of clusters interactively. Clustering
use less clusters is much faster than using more clusters, interactively the user may wish to
explore a small number of clusters quite quickly (e.g. 100), and then dial up the number of clusters
for presentation graphics.</p></li>
<li><p>The k-means algorithm is iterative. More iterations gives a better set of clusters, but of
course takes more time. It would be very nice to be able to see the earlier results from
the first iterations visually and interactively, and have them update in front of the user
so that the visualization can be used while it is also being improved. We typically use 30 iterations
but sometimes use 100 iterations, and it takes over 20 minutes to produce a set of clusters, and
then the user still has to perform the subsequent steps in QGIS before they can see the
results. Thus, this is not currently a very interactive process.</p></li>
<li><p>K-means can be parallelized and run using GPU resources and run on the user&#8217;s machine. Our current
implementation has been running on the server machine, thus tying up server resources.</p></li>
<li><p>The PECAS implementation stores the full flows in the same .gml file as the partial flows,
so this file is quite large due to the inefficiencies in the .gml standard. A more compact storage
mechanism would be better, and storing the full matrix separate from the compressed matrix would also
be nice. In fact in a QGIS implementation, we would not have to write out the results at all, the
user could choose to save te results in any format they wanted using QGIS export functionality.</p></li>
</ul>

<h1 id="proposedimplementation">Proposed implementation</h1>

<p>The proposal is to have a system running on the user&#8217;s computer that:</p>

<ol>
<li><p>(Optional) Connects to the PECAS server to download the flow matrix and the zone coordinates,
and to aggregate different flow matrices together on either the server or client. (For example,
look at all labour flows at once.)</p></li>
<li><p>(Optional) Converts the projection into the local UTM projection</p></li>
<li><p>Interactively prompts (using a slider, ideally) the user for the number of clusters</p></li>
<li><p>Starts clustering</p></li>
<li><p>After each iteration of the clustering, it produces a map of arrows similar to <a href="Compressed_flow_AB_example.png" title="Compressed Flow Visual from QGIS and PECAS">the alberta flow map</a></p></li>
</ol>

<p>There are two proposed implementations:</p>

<p>1) Using a QGIS plugin, so that the map can be drawn in QGIS, and the software can be written in python.</p>

<p>2) Using a Javascript implementation, so that the map can be draw in a browser, and the software can be written in Javascript.</p>

<p>We would like to use the GPU resources, as k-means is a simple algorithm suitable for a large
degree of parallelism. The choice between python (QGIS) or javascript (browser) may depend
on the availability and suitability of libraries, skills, and computing resources (e.g. GPUs available).</p>

<p>Some libraries have been identified: </p>

<ul>
<li><p>Nathan Epstein <a href="https://github.com/NathanEpstein/clusters">Clusters</a>
library for clustering in Javascript.</p></li>
<li><p>The <a href="http://gpu.rocks">gpu.js</a> library for using the GPU in javascript</p></li>
<li><p>The <a href="https://github.com/src-d/kmcuda">kmcuda</a> library in python which uses the
CUDA library on NVidia processes. It claims &#8220;300K samples are grouped into 5000 clusters in 4½ minutes on NVIDIA Titan X (15 iterations)&#8221;
which is a problem of a similar scale to what we have in the Alberta PECAS Model, and would show the user
another iteration every 18 seconds.</p></li>
<li><p>The <a href="https://github.com/ishanhan/parallel-implementation-of-kmeans">parallel implementation of k-means</a> which uses
MPI and PyCUDA</p></li>
</ul>

<p>There are <a href="https://www.google.com/search?client=safari&amp;rls=en&amp;q=python+parallel+k-means&amp;ie=UTF-8&amp;oe=UTF-8">many other possibilites</a> for Python. </p>

<p>However for real speed in a python program, the best option is often to call a
<a href="https://www.google.com/search?client=safari&amp;rls=en&amp;ei=hdYrW5jJJ9qN0PEP6oy92Ag&amp;q=parallel+k-means+c+library&amp;oq=parallel+k-means+c+library">C binary library</a>
that has been packaged as a python module.
This would give us the full availability of parallel processing and the performance of C. However the ability
to report results each iteration may be more difficult to program.</p>

<h3 id="examplejavamainprogram">Example java main program</h3>

<pre><code>public static void main(String[] args) {
    // Test method to build a compressed matrix
    String matrixFileName = args[0];
    String centroidCoordinatesFile = args[1];
    String outputFileName = args[2];
    Path p = Paths.get(outputFileName);
    String flowName = p.getFileName().toString();
    if(flowName.contains(&quot;.&quot;)) flowName = flowName.substring(0, flowName.lastIndexOf('.'));
    int numFlows = Integer.valueOf(args[3]);
    int iterations = 100;
    if (args.length &gt; 4) {
        iterations = Integer.valueOf(args[4]);
    }

    FlowMatrixCompressor me = new FlowMatrixCompressor();

    Matrix b = null;
    TableDataSet coords=null;
    try {
        b = MatrixReader.readMatrix(new File(matrixFileName), null);
        TableDataFileReader myReader = new NEW_CSVFileReader();
        coords = myReader.readFile(new File(centroidCoordinatesFile));
    } catch (IOException e) {
        logger.error(&quot;Problem reading or writing data in test routine&quot;);
        e.printStackTrace();
    }

    coords.buildIndex(1);
    ArrayList&lt;Collection&lt;FourCoordinatesI&gt;&gt; flows = new ArrayList&lt;Collection&lt;FourCoordinatesI&gt;&gt;();
    flows.add(me.buildFlowArray(b,coords));
    int[] numFlowArray = new int[2];
    numFlowArray[0] = b.getRowCount()*b.getRowCount();
    Collection&lt;FourCoordinatesI&gt; results = me.compress(flows.get(0),numFlows,iterations);
    flows.add(results);
    numFlowArray[1] = numFlows;
    //if (outputFileName.endsWith(&quot;gml&quot;)) {
        int srid_epsg = 3857;
        try {
            srid_epsg = Integer.valueOf(args[5]);
        } catch (Exception e) { 
            // just use 3857
        }
        writeGMLFlows(outputFileName, flowName, flows, srid_epsg, outputFileName, numFlowArray);
    //} else {
        //writeCSVCompressedFlows(outputFileName, results[1]);
    //}

}
</code></pre>

</body>
</html>
