# Flow Compressor from Finder (to be ported to Windows)

This seems to work in OSX as an automator workflow, although it does not work if I echo the java command and cut and paste it (file names need to be quoted inside of terminal, but not inside of bash???)


```
#!/bin/bash
echo "$@"
for fullpath in "$@"
do
	filename="${fullpath##*/}"                      # Strip longest match of */ from start
    dir="${fullpath:0:${#fullpath} - ${#filename}}" # Substring from 0 thru pos of filename
    base="${filename%.[^.]*}"                       # Strip shortest match of . plus at least one non-dot char from end
    ext="${filename:${#base} + 1}"                  # Substring from len of base thru end
    if [[ -z "$base" && -n "$ext" ]]; then          # If we have an extension and no base, it's really the base
        base=".$ext"
        ext=""
    fi
	echo Dir is "$dir"
	java -Dlog4j.configuration=file:/Users/jea/Applications/log4j.xml -jar ~/Applications/StandAloneFlowCompressor.jar "$fullpath" "$dir../AllYears/Inputs/ZonesCoordinates.csv" "$dir$base.gml" 5000 30 3401 &
done
wait
```

Here is the automator screenshot. ![automator][]

[automator]: automator_compressor.png "The screenshot within OSX Automator"