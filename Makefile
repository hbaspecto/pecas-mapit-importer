#
# pecas-visualizations/Makefile ---
#
# Qs: drop support for < QGIS 3.16
#     what is the oldest?

_default: _test

include ./Makefile.inc

#####

# system install (ubuntu)
_apt_get_install:
	sudo apt-get install \
	  isort \
	  python3-autopep8

_brew_install:
	brew install pyqt

#####

_ve_build:
	mkdir -p "$(dir ${PMI_VE_DIR})"
#
	${sys_python3_exe} -m venv "${PMI_VE_DIR}"
#
	${pip3_cmd} install --upgrade pip wheel
	${pip3_cmd} install autopep8 isort
#
	${pip3_cmd} install pyyaml 'jinja2-cli[yaml]'

_ve_rm:
	-rm -rf "${PMI_VE_DIR}"

_ve_rebuild: _ve_rm _ve_build

${PMI_VE_DIR}:
	make _ve_rebuild

_ve_exists: ${PMI_VE_DIR}

#####

autopep8_files=$(wildcard ./PECAS_MapIt_Importer/*.py)

_isort:
	${isort_cmd} \
	  ${autopep8_files}

_autopep8:
	${autopep8_cmd} \
	  ${autopep8_files}

_dos2unix:
	find -E \
	  ./PECAS_MapIt_Importer \
	  -type f \
	  -regex '.*\.(py|rst|txt)' \
	  -exec dos2unix "{}" ";"

#####

#
_build_LICENSE.txt:
	cp ./LICENSE.txt \
	  ${PMI_PLUGIN_SRC}/LICENSE.txt

_build_metadata.txt:
#
	rm -f ${PMI_PLUGIN_SRC}/metadata.txt
#
	./bin/pmi-jinja2 \
	  ${PMI_PLUGIN_SRC}/metadata.txt.j2 \
	  > ${PMI_PLUGIN_SRC}/metadata.txt.tmp
#
	mv ${PMI_PLUGIN_SRC}/metadata.txt.tmp ${PMI_PLUGIN_SRC}/metadata.txt
# debug
	head ${PMI_PLUGIN_SRC}/metadata.txt

_build_mapit_https.py:
#
	rm -f ${PMI_PLUGIN_SRC}/mapit_https.py
#
	./bin/pmi-jinja2 \
	  ${PMI_PLUGIN_SRC}/mapit_https.py.j2 \
	  > ${PMI_PLUGIN_SRC}/mapit_https.py.tmp
#
	mv ${PMI_PLUGIN_SRC}/mapit_https.py.tmp ${PMI_PLUGIN_SRC}/mapit_https.py
# debug
	head ${PMI_PLUGIN_SRC}/mapit_https.py

_build_zip:
	./bin/pmi-build-zip

# Dont need to do this all the time
# _build_all+=_build_LICENSE.txt

# Build it all
_build_all+=_build_metadata.txt
_build_all+=_build_mapit_https.py
_build_all+=_build_zip

_build_all: ${_build_all}

_precommit+=_ve_rebuild
_precommit+=_isort
_precommit+=_autopep8
_precommit+=_build_all

# Dont push to prd unless we mean to.
ifeq (${PMI_DEPLOY},test)
_precommit+=_deploy_to_qgis_plugins
endif

_precommit: ${_precommit}

#####

_deploy_to_qgis_plugins:
# copy this zip
	scp "./zips/${PMI_ZIPFILE_NAME}" \
	  "${QPHC_HOST}:${QPHC_WWW_PLUGIN_DIR}"
# rebuild the "plugins.xml" files
	ssh "${QPHC_HOST}" 'cd /nfs/www/qgis-plugins-hbaspecto-com && source ./hba-setup.env && ./jenkins/jenkins-rebuild-plugins-xml'

#####

# Makes a link, so we can edit the source.
# That way we dont have to zip/install/remove for each change.
_qgis3_link_create:
	ln -sf "${PMI_PLUGIN_SRC}" "${QGIS3_PLUGINS_DIR}"

_qgis3_link_rm:
	rm -f "${QGIS3_PLUGINS_DIR}/PECAS_MapIt_Importer"

#####

# To download the offical "plugins.xml" file, you need to give the QGIS user agent.
# Otherwise it says it doesnt exist.
./plugins.qgis.org-plugins.xml:
	wget \
	  -O ${@} \
	  --user-agent "Mozilla/5.0 (Macintosh; Intel Mac OS X) AppleWebKit/602.1 (KHTML, like Gecko) QGIS3/3.16.5 () Version/10.0 Safari/602.1" \
	  'https://plugins.qgis.org/plugins/plugins.xml?qgis=3.16'
#
# grep -e pyqgis_plugin -e download_url ./plugins.qgis.org-plugins.xml

#####

_test:
	echo "@TODO: put tests here"

####################
####################


#####
#
# _metadata_util_test_get_version:
# 	./bin/metadata-util \
# 	  --mdf ${MAPIT_PLUGIN_METADATA_TXT} \
# 	  --get-version
#
# _metadata_util_test_set_version:
# #
# 	cp ${MAPIT_PLUGIN_METADATA_TXT} ./metadata.tmp
# 	grep "version=" ./metadata.tmp
# #
# 	./bin/metadata-util \
# 	  --mdf ./metadata.tmp \
# 	  --set-version ${MAPIT_PLUGIN_VER}
# #
# 	grep "version=" ./metadata.tmp
#
# metadata_util_test+=_metadata_util_test_get_version
# metadata_util_test+=_metadata_util_test_set_version
#
# _metadata_util_test: ${metadata_util_test}
#
# #####
#
# # Will have ".SEC" appended.
# MAPIT_PLUGIN_NAME:=PECAS_MapIt_Importer
# # Users will set the major version
# MAPIT_PLUGIN_VER_MAJOR:=0.7
# # use the date for the minor part (like: 201911070913)
# MAPIT_PLUGIN_VER_MINOR:=$(shell date +%Y%m%d%H%M)
# #
# MAPIT_PLUGIN_VER=${MAPIT_PLUGIN_VER_MAJOR}.${MAPIT_PLUGIN_VER_MINOR}
#
# #
# MAPIT_PLUGIN_METADATA_TXT:=./PECAS_MapIt_Importer/metadata.txt
# # use "=" to delay expansion.
# MAPIT_PLUGIN_VER_FROM_FILE=$(shell ./bin/metadata-util \
# 	--mdf ${MAPIT_PLUGIN_METADATA_TXT} \
# 	--get-version)
# MAPIT_PLUGIN_ZIP=${MAPIT_PLUGIN_NAME}.${MAPIT_PLUGIN_VER_FROM_FILE}.zip
#
# mapit_plugin_glob=/nfs/www/qgis-plugins-hbaspecto-com/hba/plugins/packages/${MAPIT_PLUGIN_NAME}.${MAPIT_PLUGIN_VER_MAJOR}.*.zip
#
# _mapit_plugin_clean_zips:
# 	-rm -f ${MAPIT_PLUGIN_NAME}.*.zip
#
# _mapit_plugin_update_version:
# 	./bin/metadata-util \
# 	  --mdf ${MAPIT_PLUGIN_METADATA_TXT} \
# 	  --set-version ${MAPIT_PLUGIN_VER}
# #
# 	./bin/metadata-util \
# 	  --mdf ${MAPIT_PLUGIN_METADATA_TXT} \
# 	  --get-version
#
# _mapit_plugin_rm_old:
# 	-rm ${mapit_plugin_glob}
#
# _mapit_plugin_zip:
# 	-rm ./${MAPIT_PLUGIN_ZIP}
# #
# 	zip \
# 	  -r ./${MAPIT_PLUGIN_ZIP} \
# 	  ${MAPIT_PLUGIN_NAME}
#
# _mapit_plugin_update:
# # TODO: too much of a big hammer
# 	-rm /nfs/www/qgis-plugins-hbaspecto-com/hba/plugins/plugins.xml
# #
# 	source /nfs/home/repos/qgis-plugins-xml/hba-setup.env && \
# 	hba-qgis-plugins-add \
# 	  ${MAPIT_PLUGIN_NAME} \
# 	  ${MAPIT_PLUGIN_ZIP}
# #
# 	echo "### now test it with QGIS."
# 	echo "### ${QPHC_TEST_WWW_URL}"
#
# _mapit_plugin_ls:
# 	-ls -l ${mapit_plugin_glob}
#
# _mapit_plugin: \
# 	_mapit_plugin_clean_zips \
# 	_mapit_plugin_rm_old \
# 	_mapit_plugin_update_version \
# 	_mapit_plugin_zip \
# 	_mapit_plugin_update \
# 	_mapit_plugin_ls
#
# #####
#
# _test+=_metadata_util_test
#
# _test: ${_test}
#
# #####
#
# # http://jenkins-2.office.hbaspecto.com/job/qgis-plugins/job/PECAS_MapIt_Importer--build
# _jenkins_mapit_plugin: _mapit_plugin
#
