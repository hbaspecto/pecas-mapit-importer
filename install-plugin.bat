@echo off

if exist "%programfiles%\QGIS 2.16\apps\Python27" (
    set PYTHONHOME=%programfiles%\QGIS 2.16\apps\Python27
    set PYTHONPATH=%programfiles%\QGIS 2.16\apps\Python27\Lib
    "%programfiles%\QGIS 2.16\bin\python.exe" installer.py %*
    exit /b
)

if exist "%programfiles%\QGIS 2.18\apps\Python27" (
    set PYTHONHOME=%programfiles%\QGIS 2.18\apps\Python27
    set PYTHONPATH=%programfiles%\QGIS 2.18\apps\Python27\Lib
    "%programfiles%\QGIS 2.18\bin\python.exe" installer.py %*
    exit /b
)

echo QGIS with Python not found in "%programfiles%\QGIS 2.16" or "%programfiles%\QGIS 2.18"
:getpath
set /p qgispath="Please enter the path to your QGIS installation (2.16 or 2.18 required): "
if exist "%qgispath%\apps\Python27" (
    set PYTHONHOME=%qgispath%\apps\Python27
    set PYTHONPATH=%qgispath%\apps\Python27\Lib
    start /b "QQGIS Installer" "%qgispath%\bin\python.exe" installer.py %*
    exit /b
)
echo No QGIS with Python installation found at "%qgispath%"
goto getpath
