pecas-mapit-importer/README
==================================================

Links:

- Repo:    https://bitbucket.org/hbaspecto/pecas-mapit-importer
- Docs:    http://docs.office.hbaspecto.com/pecas-mapit-importer
- Jenkins: http://jenkins-2.office.hbaspecto.com/job/qgis-plugins-hbaspecto-com/
- License: http://www.hbaspecto.com/products/pecas/software

qgis-plugins.hbaspecto.com:

- Web:     http://qgis-plugins.hbaspecto.com
- Jenkins: http://jenkins-2.office.hbaspecto.com/job/qgis-plugins-hbaspecto-com

Docs:

- https://docs.qgis.org/3.16/en/docs/pyqgis_developer_cookbook/plugins/index.html
- https://www.qgistutorials.com/en/docs/3/building_a_python_plugin.html

Add: sha1 of zip


Quickstart
--------------------------------------------------

Install ``QGIS-3.16``, the long term release, as that is
what we recommend to users.

::

    git clone git@bitbucket.org:hbaspecto/pecas-mapit-importer.git
    cd pecas-mapit-importer

    # This defaults to "hba-dev"; If you want "hba-prd", then
    # source ./configs/hba-prd/setup.env
    source ./hba-setup.env
    make _test

    # Build the local VE if you want to do local dev (not used by QGIS, but
    # required to build and deploy the zip
    make _ve_rebuild

    # To use your version with your QGIS locally
    make _qgis3_link_create
	# You may have to edit the value of ${QGIS3_PLUGINS_DIR} to point
    # to the python/plugins directory in your profile folder.
    # QGIS has a menu item to show you your profile folder, it's
    # Settings -> User Profiles -> Open Active Profile Folder

    # After you've reviewed your changes locally, you can build the release
    make _build_all
    # which makes a .zip file in the ./bin directory
    # But if you push your changes to master you can ask Jenkins to deploy it
    # to a particular client and deploy configuration.


.. Also sphinx/source/END_USER_INSTALL.rst

.. `End user installation instructions <http://docs.office.hbaspecto.com/pecas-visualizations/END_USER_INSTALL.html>`_

Dev Notes
--------------------------------------------------

- Look in ``${QGIS3_PLUGINS_DIR}`` for the plugins.

- Use ``_qgis3_link_create`` to make a simlink to your local QGIS installation

- This used to be called ``pecas-visualizations``.



Branches
--------------------------------------------------

There are a couple branches in this repo.  We use them to
track what we are shipping to our users.

Normally, we should work on ``master``.  Once we are happy
with things, it is merged onto the branch which is shipped
to them.


PECAS_MapIt_Importer Releases
--------------------------------------------------

- The major part of the version number is in the makefile.

- Commits to the ``master`` branch no longer cause the jenkins job
  to build automatically, please use jenkins to build and deploy.


